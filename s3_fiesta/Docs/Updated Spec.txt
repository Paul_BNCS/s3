1. Make buffersize configurable, default = 16K
2. Add buffer full event

3. Synchronise logging
4. Support for external debug view
5. Debug the protocol parser in a test app using yesterday's logs
6. ignore value changes until all units/parameters are populated
7. essentially redesign the initialisation routine and persist getunit calls until all units/parameters are gathered