﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BNCS;
using Bncs.Controls;

namespace s3_fiesta
{
    public enum WizardPosition { Start, Middle, End, Lost };

    /// <summary>
    /// Interaction logic for InstanceEditor.xaml
    /// </summary>
    public partial class InstanceEditor : Window
    {
        private List<BNCSDeviceInstance> _data = new List<BNCSDeviceInstance>();
        private bool _ok = false;
        private int _index = -1;

        public InstanceEditor(List<string> deviceNames_, int infodriverNumber_)
        {
            InitializeComponent();

            foreach (string deviceName in deviceNames_)
                _data.Add(new BNCSDeviceInstance(deviceName, infodriverNumber_));
            NextClick(null, null);
        }

        private WizardPosition DisplayDevice()
        {
            if (_index > -1 && _index < _data.Count)
            {
                SetEventHandlers(false);
                tbDeviceType.Text = _data[_index].DeviceTypeName;
                nsIDNumber.Value = _data[_index].Reference.DeviceNumber;
                nsOffset.Value = _data[_index].Reference.SlotOffset;
                tbAltID.Text = _data[_index].AltID;
                tbID.Text = _data[_index].Id;
                tbLocale.Text = _data[_index].Location;
                cbComposite.IsChecked = null == _data[_index].Composite ? false : _data[_index].Composite;
                Title = "Instances Editor - " + tbDeviceType.Text;
                SetEventHandlers(true);

                if (0 == _index)
                    return WizardPosition.Start;
                else if (_data.Count - 1 == _index)
                    return WizardPosition.End;
                return WizardPosition.Middle;
            }
            Title = "Instances Editor";
            return WizardPosition.Lost;
        }

        private bool OKButtonEnablement()
        {
            bool result = true;
            foreach (BNCSDeviceInstance instance in _data)
            {
                result = instance.DeviceTypeName.Length > 0 && instance.Id.Length > 0;
                if (!result)
                    break;
            }
            return result;
        }

        private void ExecuteButtonClick()
        {
            switch (DisplayDevice())
            {
                case WizardPosition.Start:
                    bnPrevious.IsEnabled = false;
                    bnNext.Content = _index == _data.Count - 1 ? "OK" : "Next";
                    if (bnNext.Content.ToString().Equals("OK"))
                        bnNext.IsEnabled = OKButtonEnablement();
                    else
                        bnNext.IsEnabled = true;
                    break;
                case WizardPosition.Middle:
                    bnPrevious.IsEnabled = true;
                    bnNext.IsEnabled = true;
                    bnNext.Content = "Next";
                    break;
                case WizardPosition.End:
                    bnPrevious.IsEnabled = true;
                    bnNext.Content = "OK";
                    bnNext.IsEnabled = OKButtonEnablement();
                    break;
                default:
                    bnNext.IsEnabled = false;
                    bnPrevious.IsEnabled = false;
                    break;
            }
        }

        private void PreviousClick(object sender, RoutedEventArgs e)
        {
            --_index;
            if (_index < _data.Count)
                ExecuteButtonClick();
            else
                ++_index;
        }

        private void NextClick(object sender, RoutedEventArgs e)
        {
            if (bnNext.Content.ToString().Equals("Next"))
            {
                ++_index;
                if (_index < _data.Count)
                    ExecuteButtonClick();
                else
                    --_index;
            }
            else
            {
                _ok = true;
                Close();
            }
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public List<BNCSDeviceInstance> Data
        {
            get { return _ok ? _data : null; }
        }

        private void TextEdit(object sender, TextChangedEventArgs e)
        {
            switch(((TextBox)sender).Tag.ToString())
            {
                case "locale":
                    _data[_index].Location = ((TextBox)sender).Text;
                    break;
                case "altid":
                    _data[_index].AltID = ((TextBox)sender).Text;
                    break;
                case "id":
                    _data[_index].Id = ((TextBox)sender).Text;
                    if (bnNext.Content.ToString().Equals("OK"))
                        bnNext.IsEnabled = OKButtonEnablement();
                    break;
                case "devicetype":
                    _data[_index].DeviceTypeName = ((TextBox)sender).Text;
                    if (bnNext.Content.ToString().Equals("OK"))
                        bnNext.IsEnabled = OKButtonEnablement();
                    break;
            }
        }

        private void NumericEdit(object sender, OnBeforeNumericSelectorValueChangeEventArgs e, ref bool accept_)
        {
            if (((NumericSelector)sender).Tag.ToString().Equals("offset"))
                _data[_index].Reference = new BNCSInstanceReference(_data[_index].Reference.DeviceNumber, e.NewValue);
            else
                _data[_index].Reference = new BNCSInstanceReference(e.NewValue, _data[_index].Reference.SlotOffset);
        }

        private void CheckEdit(object sender, RoutedEventArgs e)
        {
            _data[_index].Composite = true == cbComposite.IsChecked;
        }

        private void SetEventHandlers(bool on_)
        {
            if (on_)
            {
                tbAltID.TextChanged += TextEdit;
                tbDeviceType.TextChanged += TextEdit;
                tbID.TextChanged += TextEdit;
                tbLocale.TextChanged += TextEdit;
                nsIDNumber.OnBeforeChange += NumericEdit;
                nsOffset.OnBeforeChange += NumericEdit;
                cbComposite.Checked += CheckEdit;
                cbComposite.Unchecked += CheckEdit;
            }
            else
            {
                tbAltID.TextChanged -= TextEdit;
                tbDeviceType.TextChanged -= TextEdit;
                tbID.TextChanged -= TextEdit;
                tbLocale.TextChanged -= TextEdit;
                nsIDNumber.OnBeforeChange -= NumericEdit;
                nsOffset.OnBeforeChange -= NumericEdit;
                cbComposite.Checked -= CheckEdit;
                cbComposite.Unchecked -= CheckEdit;
            }
        }
    }
}
