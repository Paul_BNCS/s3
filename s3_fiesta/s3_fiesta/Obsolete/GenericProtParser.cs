﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Bncs.SysUtils;

namespace Fiesta
{
    #region GenericProtocol event type
    // Enumerated member that betrays the parse status
    public enum ParseInfo { ValidDatagram, IncompleteDatagram, InvalidDatagram, Ack, Nack, Csi };
    // Enumerated member that allows a subscriber to clear the buffer. The value out is always Wait, a subscriber will change this to Clear, typically after receiving a 
    // ParseInfo.InvalidDatagram
    public enum PostParseAction { Wait, Clear };
    public class OnParseResultEventArgs : EventArgs
    {
        public ParseInfo Info;
        public byte[] Data;
        public PostParseAction Action = PostParseAction.Wait;
    }
    public delegate void OnParseResultEventHandler(object sender, ref OnParseResultEventArgs e);
    #endregion

    public enum AckState { NotFound, AckAtStart, NackAtStart };

    public abstract class GenericProtParser
    {
        #region Private members
        private ByteQueue _buffer = new ByteQueue();
        private bool _isMidMessage = false;
        private List<DataGramTemplate> _templates = new List<DataGramTemplate>();
        private UserControl _controlPanel = null;
        private event OnParseResultEventHandler _onParse = null;
        #endregion

        #region Protected members
        protected string _displayName = null;
        protected byte[] _ACK = { 0 };
        protected byte[] _NACK = { 0 };
        protected string _deviceResponseMessage = string.Empty;
        #endregion

        #region Protected constants
        protected const int _PARSE_INVALID = -1;
        protected const int _PARSE_INCOMPLETE = 0;
        #endregion

        #region Private method
        private AckState SeekAckOrNack()
        {
            if (null != _ACK || null != _NACK)
            {
                if (null != _ACK)
                {
                    if (_buffer.StartsWith(_ACK))
                        return AckState.AckAtStart;
                }

                if (null != _NACK)
                {
                    if (_buffer.StartsWith(_NACK))
                        return AckState.NackAtStart;
                }
            }
            return AckState.NotFound;
        }
        #endregion

        #region Protected abstract methods
        /// <summary>
        /// Parses the buffer on a derived class, derived classes MUST implement this method
        /// </summary>
        /// <param name="buffer_">Array of bytes currently in the buffer</param>
        /// <returns> A DatagramPosition or alternatively derived classes are free to return the DatagramPosition derivitive DatagramTemplatePosition if dealing with a 
        /// DatagramTemplate. 
        /// 
        /// When returning a DatagramPosition the length member should be set as follows by the derived class: 
        /// _PARSE_INVALID (-1): the data represents an invalid datagram 
        /// _PARSE_INCOMPLETE (0): the data represents a partialy valid datagram
        /// > 0: the data represents a valid datagram up to and including the returned element index
        /// 
        /// When returning a DatagramTemplatePosition the Length and StartIndex members must be set accordingly so that the data may be extracted accurately. Typically this
        /// return will a direct return from DatagramTemplate.IndexOf()</returns>
        protected abstract DatagramPosition ParseData(byte[] buffer_);

        /// <summary>
        /// Ensures derived classes are notified to clear 
        /// </summary>
        protected abstract void Clear(); 
        #endregion

        #region Protected method
        protected bool AddTemplate(DataGramTemplate template_)
        {
            foreach (DataGramTemplate template in _templates)
            {
                if (template.Name.ToLower().Equals(template_.Name.ToLower()))
                    return false;
            }

            _templates.Add(template_);
            return true;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Override of ToString()
        /// </summary>
        /// <returns>Enables a class instanciation to be represented textually by it's display name</returns>
        public override string ToString()
        {
            // This to encourage developers, ie child class authors, to set the display name in their derived constructor 
            if (null == _displayName || 0 == _displayName.Trim().Length)
                throw new Exception("Classes derived from GenericProtocol should assign a value to the \"_displayName\" variable in their constructor.");
            return _displayName;
        }

        /// <summary>
        /// Adds a data buffer to the end of the existing buffer and fires off a child classes parsing routine until we have finished processing all valid datagrams therein
        /// </summary>
        /// <param name="buffer_">Data to add</param>
        /// <returns>True if the data is not null and has a length greater than zero</returns>
        public bool AppendData(byte[] buffer_)
        {
            if (null != buffer_ && buffer_.Length > 0)
            {
                // Append incoming data to the existing buffer
                _buffer.Enqueue(buffer_);

                // Seek ACK or NACK if we're not already mid message
                if (!_isMidMessage)
                {
                    AckState ackState = SeekAckOrNack();
                    while (ackState > AckState.NotFound)
                    {
                        int size = ackState == AckState.AckAtStart ? _ACK.Length : _NACK.Length;
                        _buffer.Dequeue(size);
                        if (null != _onParse)
                        {
                            // Create the event arguments
                            OnParseResultEventArgs args = new OnParseResultEventArgs();
                            // Set the info to ack or nack
                            args.Info = ackState == AckState.AckAtStart ? ParseInfo.Ack : ParseInfo.Nack;
                            // Copy the buffer data to the event argument data buffer
                            args.Data = ackState == AckState.AckAtStart ? _ACK : _NACK;
                            // Fire the event handler
                            _onParse(this, ref args);
                            if (PostParseAction.Clear == args.Action)
                                _buffer.Clear();
                        }
                        // Keep checking in case we have consecutive ack/nacks
                        ackState = SeekAckOrNack();
                    }
                }
                // Call the parser from a derived class, potentially more than once, so create and set true a do parse flag
                bool doParse = true;
                // Loop while we need to parse
                while (doParse)
                {
                    // In most cases we don't need to parse any more so assuming this by setting the flag to false to escape this loop when done
                    doParse = false;
                    // Call a derived classes ParseData method passing it the buffer
                    DatagramPosition position = ParseData(_buffer.Peek(_buffer.Count, true));
                    if (null == position)
                    {
                        if (null != _onParse)
                        {
                            // Create the event arguments
                            OnParseResultEventArgs args = new OnParseResultEventArgs();
                            // Set the info to valid
                            args.Info = _isMidMessage ? ParseInfo.IncompleteDatagram : ParseInfo.InvalidDatagram;
                            // Fire the event handler
                            _onParse(this, ref args);
                            if (PostParseAction.Clear == args.Action)
                                _buffer.Clear();
                        }
                        return false;
                    }

                    if (position is DatagramTemplatePosition)
                    {
                        if (null != _onParse)
                        {
                            DatagramTemplatePosition templatePosition = (DatagramTemplatePosition)position;
                            // Create the event arguments
                            OnParseResultEventArgs args = new OnParseResultEventArgs();
                            // Set the info to valid
                            args.Info = templatePosition.Length > -1 ? ParseInfo.ValidDatagram : ParseInfo.IncompleteDatagram;
                            // Copy the valid datagram bytes to the event argument data buffer
                            args.Data = args.Info == ParseInfo.ValidDatagram ? _buffer.Dequeue(templatePosition) : null;
                            // Fire the event handler
                            _onParse(this, ref args);
                            if (PostParseAction.Clear == args.Action)
                            {
                                Clear();
                                _buffer.Clear();
                            }
                        }
                    }
                    else
                    {
                        // Act according to the result from the derived class
                        switch (position.Length)
                        {
                            case _PARSE_INCOMPLETE: // data is valid from the start but incomplete, so do nothing
                                _isMidMessage = true;
                                break;
                            case _PARSE_INVALID: // data is invalid and needs to be thrown away
                                _isMidMessage = false;
                                byte[] buffer = _buffer.Dequeue(_buffer.Count);
                                if (null != _onParse) // OnParse is subscribed to so fire this event 
                                {
                                    // Create the event arguments
                                    OnParseResultEventArgs args = new OnParseResultEventArgs();
                                    // Set the info to invalid
                                    args.Info = ParseInfo.InvalidDatagram;
                                    // Copy the buffer data to the event argument data buffer
                                    args.Data = buffer;
                                    // Fire the event handler
                                    _onParse(this, ref args);
                                }
                                break;
                            default: // data is valid and represents a complete datagram packet up to and including byte index # parseDataResult
                                _isMidMessage = false;
                                if (null != _onParse) // OnParse is subscribed to so fire this event 
                                {
                                    // Create the event arguments
                                    OnParseResultEventArgs args = new OnParseResultEventArgs();
                                    // Set the info to valid
                                    args.Info = ParseInfo.ValidDatagram;
                                    // Copy the valid datagram bytes to the event argument data buffer
                                    args.Data = _buffer.Dequeue(position.Length);
                                    // Fire the event handler
                                    _onParse(this, ref args);
                                    if (PostParseAction.Clear == args.Action)
                                        _buffer.Clear();
                                }

                                // Reset the parse flag
                                doParse = _buffer.Count > 0;
                                break;
                        }
                    }
                }
                return true;

            }
            return false;
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Gets the current contents of the buffer validated or not
        /// </summary>
        public byte[] Buffer
        {
            get { return _buffer.Peek(_buffer.Count); }
        }

        /// <summary>
        /// Gets the array of bytes that a derived protocol class defines as it's acknowledgement message
        /// </summary>
        public byte[] ACK
        {
            get { return _ACK; }
        }

        /// <summary>
        /// Gets the array of bytes that a derived protocol class defines as it's negative acknowledgement message
        /// </summary>
        public byte[] NACK
        {
            get { return _NACK; }
        }

        public List<DataGramTemplate> Templates
        {
            get { return _templates; }
        }

        public string DeviceResponseMessage
        {
            get 
            { 
                string result = _deviceResponseMessage;
                _deviceResponseMessage = string.Empty;
                return result;
            }
        }

        /// <summary>
        /// User control to tie to this protocol
        /// </summary>
        public UserControl ControlPanel
        {
            get { return _controlPanel; }
            set { _controlPanel = value; }
        }
        #endregion

        #region Public event handlers
        /// <summary>
        /// OnParse event handler, fires when passing concludes be it success or failure
        /// </summary>
        public event OnParseResultEventHandler OnParse
        {
            add { _onParse += value; }
            remove { _onParse -= value; }
        }
        #endregion
    }

}