﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Windows.Threading;
using Bncs.XML;
using Bncs.SysUtils;
using BNCS;

namespace Fiesta
{
    public enum ProcessUnitAlarmResult { NoXmlValue, NoUSSValue, NoAlarmType, NoAlarmTypeData, ZeroLengthAlarmType, UnknownAlarmType, NoUnitValue, InvalidUnitValue, FailedToWriteToInfoDriver, Normal, Primary, Secondary, Comms }; // Bug #2825
    public enum ProcessDataResult { Failed, GotUnitList, GotParameterValue, SetParameterValue, SetNotifyOnValue, GotUnit, LoggedIn, Error, GotSummaryStatus, GotUnregisterAll, GotUnitAlarms, GotAllUnits };
    public enum NotificationCriteria { All, OutOfRange, NoLogging, Nothing };
    public enum XMLErrorSource { TranslateUnitParameters, GetUnit, GetParameterValue, SetNotifyOnValueChanged, GetAllUnitValues, GetUnitList };

    public class FiestaProtocol : GenericProtParser
    {
        #region Private members
        private List<DataGramTemplate> _datagramTemplates = new List<DataGramTemplate>();
        private ObservableCollection<DeviceUnit> _units = new ObservableCollection<DeviceUnit>();
        private ObservableCollection<DeviceParameter> _parameters = new ObservableCollection<DeviceParameter>();
        private string _status = string.Empty;
        private string _lastErrorType = string.Empty;
        private string _lastErrorMessage = string.Empty;
        private DispatcherTimer _dispatcherTimer = new DispatcherTimer();
        private event OnGetParameterValueEvent _onGetParameter = null;
        private event OnSetNotifyOnValueChangedResponseEvent _onSetNotifyResponse = null;
        private event OnErrorEvent _onError = null;
        private event OnXmlErrorEventHandler _onXmlError = null;
        private Queue<UnitParameter> _setOperations = new Queue<UnitParameter>();
        private int _lastXmlErrorUnitNumber = -1;
        #endregion
        
        #region Constructor
        public FiestaProtocol()
        {
            string[] responseElements = new string[] {"logUserIn", "getUnitList", "getUnit", "SetNotifyOnValueChanged", "setParameterValue", "UnregisterAllNotifies", "getParameterValue", "getAllStatus", "getAllUnits", 
                "setUnitFit", "setUnitLock", "setUnitMaintenance", "error", "getSummaryStatus", "GetNotifyOnValueChanged", "getUnitAlarms"};

            foreach (string responseElement in responseElements)
                _datagramTemplates.Add(new DataGramTemplate(responseElement + " Reply", new object[] { "<" + responseElement + ">", DataGramTemplate.GetParam(ParameterTypeFlag.ptVariableLengthString), "</" + responseElement + ">" }));

            _dispatcherTimer.Tick += OnTimer;
        }
        #endregion

        #region Private methods
        private void ExceptionHandler(Exception exception_, XMLErrorSource source_)
        {
            OnXmlErrorEventArgs args = new OnXmlErrorEventArgs();
            args.Source = source_;
            args.ExceptionType = exception_.GetType().ToString();
            args.Message = Xception.Traverse(exception_, ',', true);
            string message = args.Message.ToLower();
            int startIndex = message.IndexOf("<un>");
            int endIndex = message.IndexOf("</un>");
            if (startIndex > 0 && endIndex > startIndex)
            {
                string number = message.Substring(startIndex + 4);
                number = number.Substring(0, number.IndexOf("</un>"));
                int test;
                if (Int32.TryParse(number, out test))
                    args.UnitNumber = test;
            }

            startIndex = message.IndexOf("<pn>");
            endIndex = message.IndexOf("</pn>");
            if (startIndex > 0 && endIndex > startIndex)
            {
                string number = message.Substring(startIndex + 4);
                number = number.Substring(0, number.IndexOf("</pn>"));
                int test;
                if (Int32.TryParse(number, out test))
                    args.ParameterNumber = test;
            }

            startIndex = message.IndexOf(". Line "); //, position 13
            endIndex = message.IndexOf(", position "); //, position 13
            if (startIndex > 0 && endIndex > startIndex)
            {
                string number = message.Substring(startIndex + 7);
                number = number.Substring(0, number.IndexOf(','));
                int test;
                if (Int32.TryParse(number, out test))
                    args.XmlLine = test;

                number = message.Substring(endIndex + 11);
                if (number.EndsWith("."))
                    number = number.Substring(0, number.Length - 1);
                if (Int32.TryParse(number, out test))
                    args.XmlPosition = test;
            }

            if (null != _onXmlError)
                _onXmlError(this, args);
            _lastErrorMessage = args.ToString();
        }

        private string CleanXML(string unclean_)
        {
            return unclean_.Replace("&", "&amp;");
        }

        private void GetUnitList(string getUnitList_)
        {
            try
            {
                var units = XElement.Parse(getUnitList_);
                var unitList = units.Elements("UL");
                _units.Clear();
                foreach (var unit in unitList)
                {
                    var unitNumber = unit.Element("uN");
                    var unitName = unit.Element("n");
                    DeviceUnit fu = new DeviceUnit();
                    fu.Number = Int32.Parse(unitNumber.Value);
                    fu.Name = unitName.Value;
                    _units.Add(fu);
                }
            }
            catch (Exception exception)
            {
                ExceptionHandler(exception, XMLErrorSource.GetUnitList);
            }
        }

        private void GetAllUnitValues(string getAllUnitValues_)
        {
            try
            {
                var all = XElement.Parse(getAllUnitValues_);
                var units = all.Elements("uVals");
                foreach (var unit in units)
                {
                    var xUnitNumber = unit.Element("uN");
                    DeviceUnit deviceUnit = GetDeviceByNumber((int)xUnitNumber);
                    if (null != deviceUnit)
                    {
                        var values = unit.Elements("pV");
                        foreach (var parameter in values)
                        {
                            var xNumber = parameter.Element("pN");
                            int number = ((int)xNumber) - 1;
                            if (number > -1 && number < deviceUnit.Parameters.Count)
                            {
                                var xValue = parameter.Element("v");
                                deviceUnit.Parameters[number].Value = (string)xValue;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionHandler(exception, XMLErrorSource.GetAllUnitValues);
            }

        }

        private void OnTimer(object sender, EventArgs e)
        {
            _dispatcherTimer.Stop();
            if (null != _onGetParameter && _setOperations.Count > 0)
            {
                OnGetParameterValueEventArgs args = new OnGetParameterValueEventArgs();
                UnitParameter unitParameter = _setOperations.Dequeue();
                args.Unit = unitParameter.Unit;
                args.Parameter = unitParameter.Parameter;
                _onGetParameter(this, args);
            }
        }

        // pfd
        private void SetNotifyOnValueChanged(string setNotifyOnValueChanged_)
        {
            try
            {
                var parameterValue = XElement.Parse(setNotifyOnValueChanged_);
                var unitNumber = parameterValue.Element("uN");
                var paramNumber = parameterValue.Element("pN");
                var aS = parameterValue.Element("AS");
                var min = parameterValue.Element("Min");
                var max = parameterValue.Element("Max");
                var value = parameterValue.Element("value");

                if (null != _onSetNotifyResponse)
                {
                    OnSetNotifyOnValueChangedResponseEventArgs args = new OnSetNotifyOnValueChangedResponseEventArgs();
                    args.Unit = (int)unitNumber;
                    args.Parameter = (int)paramNumber;
                    args.TriggerReason = (string)aS;
                    string minimum = (string)min;
                    string maximum = (string)max;
                    double test;
                    args.MinimumValue = Double.TryParse(minimum, out test) ? test : 0;
                    args.MaximumValue = Double.TryParse(maximum, out test) ? test : 0;
                    args.Value = (string)value;
                    _onSetNotifyResponse(this, args);
                }
            }
            catch (Exception exception)
            {
                int errorUnit = 0;
                int errorParameter = 0;
                string setNotifyOnValueChanged = setNotifyOnValueChanged_.ToLower();
                int index = setNotifyOnValueChanged.IndexOf("<un>");
                if (index > -1)
                {
                    int endIndex = setNotifyOnValueChanged.IndexOf("</un>");
                    if (endIndex > index)
                    {
                        string number = setNotifyOnValueChanged.Substring(index + 4, endIndex - (index + 4));
                        if (Int32.TryParse(number, out errorUnit))
                        {
                            index = setNotifyOnValueChanged.IndexOf("<pn>");
                            if (index > -1)
                            {
                                endIndex = setNotifyOnValueChanged.IndexOf("</pn>");
                                if (endIndex > index)
                                {
                                    number = setNotifyOnValueChanged.Substring(index + 4, endIndex - (index + 4));
                                    if (!Int32.TryParse(number, out errorParameter))
                                        errorParameter = 0;
                                }
                            }
                        }
                    }
                }

                string errorSource = string.Empty;
                if (errorUnit > 0 && errorParameter > 0)
                    errorSource = "Unit #" + errorUnit.ToString() + " Parameter #" + errorParameter.ToString() + ": ";
                
                if (exception.GetType().ToString().Equals("System.Xml.XmlException"))
                {
                    string error = errorSource;
                    while (null != exception)
                    {
                        error += exception.Message;
                        exception = exception.InnerException;
                    }
                    error += ": \"" + setNotifyOnValueChanged_ + "\"";
                    if (null != _onError)
                        _onError(this, error);
                }
                else
                {
                    string error = errorSource;
                    while (null != exception)
                    {
                        error += exception.Message;
                        exception = exception.InnerException;
                    }
                    error += ": \"" + setNotifyOnValueChanged_ + "\"";
                    if (null != _onError)
                        _onError(this, error);
                }
            }
        }

        /*
        private void SetParameterValue(string setParameterValue_)
        {
            var parameterValue = XElement.Parse(setParameterValue_);
            var unitNumber = parameterValue.Element("uN");
            var paramNumber = parameterValue.Element("pN");
            var paramValue = parameterValue.Element("value");


            //Start poll timer
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 10, 750);
            _dispatcherTimer.Start();
        }
         */

        private string GetParameterValue(string getParameterValue_)
        {
            try
            {
                var parameterValue = XElement.Parse(getParameterValue_);
                var unitNumber = parameterValue.Element("uN");
                var paramNumber = parameterValue.Element("pN");
                var paramValue = parameterValue.Element("value");

                int unit = -1;
                int parameter = -1;
                if (Int32.TryParse(unitNumber.Value.ToString(), out unit) && Int32.TryParse(paramNumber.Value, out parameter) && null != _onGetParameter && unit > 0 && parameter > 0)
                {
                    OnGetParameterValueEventArgs args = new OnGetParameterValueEventArgs();
                    args.Unit = unit;
                    args.Parameter = parameter;
                    args.Value = null == paramValue ? string.Empty : paramValue.Value;
                    _onGetParameter(this, args);
                }

                return string.Format("Status: Value={0}", paramValue.Value);
            }
            catch (Exception exception) 
            {
                ExceptionHandler(exception, XMLErrorSource.GetParameterValue);
            }

            return string.Empty;
        }

        private bool ContainsParameter(int parameterNumber_)
        {
            if (0 == _parameters.Count)
                return false;

            foreach (DeviceParameter parameter in _parameters)
            {
                if (parameter.Number == parameterNumber_)
                    return true;
            }

            return false;
        }
        private void GetUnit(string getUnit_)
        {
            try
            {
                getUnit_ = CleanXML(getUnit_);
                _parameters.Clear();
                var unit = XElement.Parse(getUnit_);
                var driverName = unit.Element("dN");
                var paramList = unit.Elements("Param");
                foreach (var param in paramList)
                {
                    var paramEnum = param.Elements("e");
                    var paramNumber = param.Element("pN");
                    var paramName = param.Element("n");
                    var paramType = param.Element("t");
                    var paramMin = param.Element("m");
                    var paramMax = param.Element("x");
                    var paramStep = param.Element("s");
                    var paramValue = param.Element("v");
                    var paramUnits = param.Element("u");
                    var paramPrivilege = param.Element("r");

                    string enumList = string.Empty;
                    if (paramEnum.Count() > 0)
                    {
                        foreach (var enumItem in paramEnum)
                            enumList += enumItem.Value + ",";
                        enumList = enumList.TrimEnd(',');
                    }

                    //Parameter type
                    // 		"B"  = Boolean
                    // 		"C"  = Char (signed 8 bit)
                    // 		"UC" = Unsigned Char(unsigned 8 bit)
                    // 		"S"  = Short (signed 16 bit)
                    // 		"US" = Unsigned Short (unsigned 16 bit)
                    // 		"L"  = Long (signed 32 bit)
                    // 		"UL" = Unsigned Long (unsigned 32 bit)
                    // 		"D"  = Double (double)
                    // 		"B32"= Bit32 (32 bit, bitfield)
                    // 		"ST"  = String (String - variable length)
                    // 		"pST"= pointer to SNMP Table (only here for completeness)
                    string strTypeName = paramType.Value;
                    if (strTypeName == "B")
                        strTypeName += " - Boolean";
                    else if (strTypeName == "C")
                        strTypeName += " - Char";
                    else if (strTypeName == "UC")
                        strTypeName += " - UChar";
                    else if (strTypeName == "S")
                        strTypeName += " - Short";
                    else if (strTypeName == "US")
                        strTypeName += " - UShort";
                    else if (strTypeName == "L")
                        strTypeName += " - Long";
                    else if (strTypeName == "UL")
                        strTypeName += " - ULong";
                    else if (strTypeName == "D")
                        strTypeName += " - Double";
                    else if (strTypeName == "B32")
                        strTypeName += " - Bit32";
                    else if (strTypeName == "ST")
                        strTypeName += " - String";
                    else if (strTypeName == "pST")
                        strTypeName += " - SNMP Table";

                    DeviceParameter fiestaParam = new DeviceParameter();
                    fiestaParam.EnumList = enumList;

                    double min = 0;
                    if (Double.TryParse(paramMin.Value, out min))
                        fiestaParam.Minimum = min;

                    double max = 0;
                    if (Double.TryParse(paramMax.Value, out max))
                        fiestaParam.Maximum = max;

                    double step = 0;
                    if (Double.TryParse(paramStep.Value, out step))
                        fiestaParam.Step = step;

                    fiestaParam.Number = Int32.Parse(paramNumber.Value);
                    // DEBUG START
                    if (fiestaParam.Number == 94 || fiestaParam.Number == 618)
                    {
                        string xstr = fiestaParam.Number.ToString();
                    }
                    // DEBUG END
                    if (!ContainsParameter(fiestaParam.Number))
                    {
                        fiestaParam.Name = paramName.Value;
                        fiestaParam.Type = paramType.Value;
                        fiestaParam.TypeName = strTypeName;
                        fiestaParam.Value = paramValue.Value;
                        fiestaParam.Units = paramUnits.Value;
                        fiestaParam.Privilege = paramPrivilege.Value;
                        _parameters.Add(fiestaParam);
                    }
                }
            }
            catch (Exception exception)
            {
                int startIndex = getUnit_.IndexOf("<uN>");
                if (startIndex > -1)
                {
                    int endIndex = getUnit_.IndexOf("</uN>");
                    if (endIndex > startIndex)
                    {
                        string unitNumber = getUnit_.Substring(0, endIndex);
                        unitNumber = unitNumber.Substring(startIndex + 4);
                        int test;
                        if (Int32.TryParse(unitNumber, out test))
                            _lastXmlErrorUnitNumber = test;
                    }

                }
                ExceptionHandler(exception, XMLErrorSource.GetUnit);
            }

        }
        #endregion

        #region Protected overridden methods
        protected override void Clear()
        {

        }

        protected override DatagramPosition ParseData(byte[] buffer_)
        {
            Databuffer buffer = new Databuffer(buffer_);
            foreach (DataGramTemplate template in _datagramTemplates)
            {
                DatagramTemplatePosition position = buffer.IndexOf(template);
                if (null != position)
                    return position;
            }
            return null;
        }
        #endregion

        #region Public command methods

        public byte[] GetSummaryStatusCommand()
        {
            return new Databuffer("<getSummaryStatus></getSummaryStatus>\n").Buffer;
        }

        public byte[] GetUnitListCommand()
        {
            return new Databuffer("<getUnitList></getUnitList>\n").Buffer;
        }

        public byte[] GetAllUnitsCommand()
        {
            return new Databuffer("<getAllUnits></getAllUnits>\n").Buffer;
        }

        public byte[] GetParameterValueCommand(int unit_, int parameter_)
        {
            return new Databuffer(String.Format("<getParameterValue><uN>{0}</uN><pN>{1}</pN></getParameterValue>\n", unit_, parameter_)).Buffer;
        }

        public byte[] SetParameterValueCommand(int unit_, int parameter_, string value_)
        {
            return new Databuffer(String.Format("<setParameterValue><uN>{0}</uN><pN>{1}</pN><value>{2}</value></setParameterValue>\n", unit_, parameter_, value_)).Buffer;
        }

        // pfd
        public byte[] SetNotifyOnValueChangedCommand(int unit_, int parameter_, NotificationCriteria criteria_)
        {
            // min or max is missing or is out of range
            //return new DataBuffer(String.Format("<SetNotifyOnValueChanged><uN>{0}</uN><pN>{1}</pN><AS>{2}</AS><Min></Min><Max></Max></SetNotifyOnValueChanged>\n", unit_, parameter_, criteria_.ToString().Substring(0, 1))).Buffer;
            return new Databuffer(String.Format("<SetNotifyOnValueChanged><uN>{0}</uN><pN>{1}</pN><AS>{2}</AS></SetNotifyOnValueChanged>\n", unit_, parameter_, criteria_.ToString().Substring(0, 1))).Buffer;
        }

        public byte[] SetNotifyOnValueChangedCommand(int unit_, int parameter_, NotificationCriteria criteria_, int minValue_, int maxValue_)
        {
            /*
                TX:<SetNotifyOnValueChanged><uN>20</uN><pN>1</pN><AS>A</AS><Min></Min><Max></Max></SetNotifyOnValueChanged>
                RX:<SetNotifyOnValueChanged><uN>20</uN><pN>1</pN><AS>A</AS><Min>13,750.000</Min><Max>14,500.000</Max></SetNotifyOnValueChanged>         
             */
            return new Databuffer(String.Format("<SetNotifyOnValueChanged><uN>{0}</uN><pN>{1}</pN><AS>{2}</AS><Min>{3}</Min><Max>{4}</Max></SetNotifyOnValueChanged>\n", unit_, parameter_, criteria_.ToString().Substring(0, 1), minValue_, maxValue_)).Buffer;
        }

        public byte[] GetUnitAlarms(int unit_)
        {
            return new Databuffer(String.Format("<getUnitAlarms><uN>{0}</uN></getUnitAlarms>\n", unit_)).Buffer;
        }

        // pfd
        public byte[] GetNotifyOnValueChangedCommand(int unit_, int parameter_)
        {
            // min or max is missing or is out of range
            return new Databuffer(String.Format("<GetNotifyOnValueChanged><uN>{0}</uN><pN>{1}</pN></GetNotifyOnValueChanged>\n", unit_, parameter_)).Buffer;
        }

        public byte[] LogUserInCommand(string userName_, string password_)
        {
            return new Databuffer(String.Format("<logUserIn><user>{0}</user><password>{1}</password></logUserIn>\n", userName_, password_)).Buffer;
        }

        public byte[] GetUnitCommand(int unit_)
        {
            return new Databuffer(String.Format("<getUnit><uN>{0}</uN></getUnit>\n", unit_)).Buffer;
        }

        public byte[] GetUnregisterAllNotifyCommand()
        {
            return new Databuffer("<UnregisterAllNotifies></UnregisterAllNotifies>\n").Buffer;
        }
        #endregion

        #region Public methods
        public ProcessDataResult ProcessData(byte[] data_)
        {
            string response = new Databuffer(data_).ToString();

            if (response.StartsWith("<getUnitList>"))
            {
                GetUnitList(CleanXML(response));
                return ProcessDataResult.GotUnitList;
            }
            else if (response.StartsWith("<getParameterValue>"))
            {
                GetParameterValue(CleanXML(response));
                return ProcessDataResult.GotParameterValue;
            }
            else if (response.StartsWith("<setParameterValue>"))
            {
                return ProcessDataResult.SetParameterValue;
            }
            else if (response.StartsWith("<SetNotifyOnValueChanged>")) // pfd
            {
                SetNotifyOnValueChanged(CleanXML(response));
                return ProcessDataResult.SetNotifyOnValue;
            }
            else if (response.StartsWith("<getUnit>"))
            {
                GetUnit(CleanXML(response));
                return ProcessDataResult.GotUnit;
            }
            else if (response.StartsWith("<logUserIn>"))
            {
                _status = "Logged In";
                return ProcessDataResult.LoggedIn;
            }
            else if (response.StartsWith("<getSummaryStatus>"))
            {
                return ProcessDataResult.GotSummaryStatus;
            }
            else if (response.StartsWith("<UnregisterAllNotifies>"))
            {
                return ProcessDataResult.GotUnregisterAll;
            }
            else if (response.StartsWith("<getAllUnits>"))
            {
                GetAllUnitValues(CleanXML(response));
                return ProcessDataResult.GotAllUnits;
            }
            else if (response.StartsWith("<getUnitAlarms>"))
            {
                return ProcessDataResult.GotUnitAlarms;
            }
            else if (response.StartsWith("<error>") && response.EndsWith("</error>"))
            {
                int index = response.IndexOf("<errorType>");
                if (index > -1)
                {
                    response = response.Substring(index + 11);
                    index = response.IndexOf("</errorType>");
                    if (index > -1)
                    {
                        _lastErrorType = response.Substring(0, index);
                        response = response.Substring(index + 12);
                        index = response.IndexOf("<errorMessage>");
                        if (index > -1)
                        {
                            response = response.Substring(index + 14);
                            index = response.IndexOf("</errorMessage>");
                            if (index > -1)
                                _lastErrorMessage = response.Substring(0, index);
                        }
                    }
                }

                return ProcessDataResult.Error;
            }

            return ProcessDataResult.Failed;
        }

        public DeviceUnit GetDeviceByNumber(int deviceNumber_)
        {
            foreach (DeviceUnit unit in _units)
            {
                if (unit.Number == deviceNumber_)
                    return unit;
            }
            return null;
        }

        public void SetNextSetParameter(UnitParameter unitParameter_)
        {
            _setOperations.Enqueue(unitParameter_);
        }

        public ObservableCollection<DeviceParameter> TranslateUnitParameters(string getUnit_)
        {
            try
            {
                ObservableCollection<DeviceParameter> parameters = new ObservableCollection<DeviceParameter>();
                List<int> checkList = new List<int>();
                getUnit_ = CleanXML(getUnit_);

                var unit = XElement.Parse(getUnit_);
                var driverName = unit.Element("dN");
                var paramList = unit.Elements("Param");
                foreach (var param in paramList)
                {
                    var paramEnum = param.Elements("e");
                    var paramNumber = param.Element("pN");
                    var paramName = param.Element("n");
                    var paramType = param.Element("t");
                    var paramMin = param.Element("m");
                    var paramMax = param.Element("x");
                    var paramStep = param.Element("s");
                    var paramValue = param.Element("v");
                    var paramUnits = param.Element("u");
                    var paramPrivilege = param.Element("r");

                    string enumList = string.Empty;
                    if (paramEnum.Count() > 0)
                    {
                        foreach (var enumItem in paramEnum)
                            enumList += enumItem.Value + ",";
                        enumList = enumList.TrimEnd(',');
                    }

                    //Parameter type
                    // 		"B"  = Boolean
                    // 		"C"  = Char (signed 8 bit)
                    // 		"UC" = Unsigned Char(unsigned 8 bit)
                    // 		"S"  = Short (signed 16 bit)
                    // 		"US" = Unsigned Short (unsigned 16 bit)
                    // 		"L"  = Long (signed 32 bit)
                    // 		"UL" = Unsigned Long (unsigned 32 bit)
                    // 		"D"  = Double (double)
                    // 		"B32"= Bit32 (32 bit, bitfield)
                    // 		"ST"  = String (String - variable length)
                    // 		"pST"= pointer to SNMP Table (only here for completeness)
                    string strTypeName = paramType.Value;
                    if (strTypeName == "B")
                        strTypeName += " - Boolean";
                    else if (strTypeName == "C")
                        strTypeName += " - Char";
                    else if (strTypeName == "UC")
                        strTypeName += " - UChar";
                    else if (strTypeName == "S")
                        strTypeName += " - Short";
                    else if (strTypeName == "US")
                        strTypeName += " - UShort";
                    else if (strTypeName == "L")
                        strTypeName += " - Long";
                    else if (strTypeName == "UL")
                        strTypeName += " - ULong";
                    else if (strTypeName == "D")
                        strTypeName += " - Double";
                    else if (strTypeName == "B32")
                        strTypeName += " - Bit32";
                    else if (strTypeName == "ST")
                        strTypeName += " - String";
                    else if (strTypeName == "pST")
                        strTypeName += " - SNMP Table";

                    DeviceParameter fiestaParam = new DeviceParameter();
                    fiestaParam.EnumList = enumList;

                    double min = 0;
                    if (Double.TryParse(paramMin.Value, out min))
                        fiestaParam.Minimum = min;

                    double max = 0;
                    if (Double.TryParse(paramMax.Value, out max))
                        fiestaParam.Maximum = max;

                    double step = 0;
                    if (Double.TryParse(paramStep.Value, out step))
                        fiestaParam.Step = step;

                    fiestaParam.Number = Int32.Parse(paramNumber.Value);
                    if (!checkList.Contains(fiestaParam.Number))
                    {
                        fiestaParam.Name = paramName.Value;
                        fiestaParam.Type = paramType.Value;
                        fiestaParam.TypeName = strTypeName;
                        fiestaParam.Value = paramValue.Value;
                        fiestaParam.Units = paramUnits.Value;
                        fiestaParam.Privilege = paramPrivilege.Value;
                        parameters.Add(fiestaParam);
                        checkList.Add(fiestaParam.Number);
                    }
                }
                return parameters;
            }
            catch (Exception exception)
            {
                ExceptionHandler(exception, XMLErrorSource.TranslateUnitParameters);
            }
            return null;
        }
        #endregion

        #region Public r/o properties
        public ObservableCollection<DeviceUnit> Units
        {
            get { return _units; }
        }

        public ObservableCollection<DeviceParameter> Parameters
        {
            get { return _parameters; }
        }

        public string Status
        {
            get { return _status; }
        }

        public string LastErrorMessage
        {
            get
            {
                string result = _lastErrorMessage;
                _lastErrorMessage = string.Empty;
                return result;
            }
        }

        public string LastErrorType
        {
            get
            {
                string result = _lastErrorType;
                _lastErrorType = string.Empty;
                return result;
            }
        }

        public int LastXmlErrorUnitNumber
        {
            get 
            {
                int result = _lastXmlErrorUnitNumber;
                _lastXmlErrorUnitNumber = -1;
                return result; 
            }
        }
        #endregion

        #region Event handlers
        public event OnGetParameterValueEvent OnGetParameter
        {
            add { _onGetParameter += value; }
            remove { _onGetParameter -= value; }
        }

        public event OnSetNotifyOnValueChangedResponseEvent OnSetNotifyResponse
        {
            add { _onSetNotifyResponse += value; }
            remove { _onSetNotifyResponse -= value; }
        }

        public event OnErrorEvent OnError
        {
            add { _onError += value; }
            remove { _onError -= value; }
        }

        public event OnXmlErrorEventHandler OnXmlError
        {
            add { _onXmlError += value; }
            remove { _onXmlError -= value; }
        }
        #endregion
    }

    public class UnitParameter
    {
        #region Private members
        private int _unit = -1;
        private int _parameter = -1;
        #endregion

        #region Constructor
        public UnitParameter(int unit_, int parameter_)
        {
            _unit = unit_;
            _parameter = parameter_;
        }
        #endregion

        #region Public properties
        public int Unit
        {
            get { return _unit; }
        }

        public int Parameter
        {
            get { return _parameter; }
        }
        #endregion
    }

    public class OnSetNotifyOnValueChangedResponseEventArgs : EventArgs
    {
        #region Public members
        public int Unit;
        public int Parameter;
        public string TriggerReason;
        public double MinimumValue;
        public double MaximumValue;
        public string Value;
        #endregion
    }
    public delegate void OnSetNotifyOnValueChangedResponseEvent(object sender, OnSetNotifyOnValueChangedResponseEventArgs e);

    public delegate void OnErrorEvent(object sender, string e);

    public class OnXmlErrorEventArgs : EventArgs
    {
        public override string ToString()
        {
            string result = "XML Parse Error in " + Source.ToString() + "(): " + Message;
            if (UnitNumber > 0)
                result += " Unit#" + UnitNumber.ToString();
            if (ParameterNumber > 0)
                result += " Parameter#" + ParameterNumber.ToString();
            return result;
        }

        public string Message = string.Empty;
        public string ExceptionType = string.Empty;
        public XMLErrorSource Source;
        public int UnitNumber = 0;
        public int ParameterNumber = 0;
        public int XmlLine = -1;
        public int XmlPosition = -1;
    }
    public delegate void OnXmlErrorEventHandler(object sender, OnXmlErrorEventArgs e);
}
