using System;
using System.Collections.Generic;

namespace Fiesta
{
    public class ByteQueue
    {
        #region Private member
        private List<byte> _list = new List<byte>();
        #endregion

        #region Public methods
        /// <summary>
        /// Removes all bytes from the queue
        /// </summary>
        public void Clear()
        {
            _list.Clear();
        }

        /// <summary>
        /// Adds an array of bytes to the end of the queue
        /// </summary>
        /// <param name="buffer_">Bytes to add</param>
        public void Enqueue(byte[] buffer_)
        {
            foreach (byte abyte in buffer_)
                _list.Add(abyte);
        }

        /// <summary>
        /// Removes the first byte from the start of the queue and returns it 
        /// </summary>
        /// <returns>The first byte or null if the queue is empty</returns>
        public byte? Dequeue()
        {
            if (_list.Count > 0)
            {
                byte result = _list[0];
                _list.RemoveAt(0);
                return result;
            }
            return null;
        }

        /// <summary>
        /// Removes the first n bytes from the start of the queue and returns them
        /// </summary>
        /// <param name="numberOfBytes_">Number of bytes to dequeue</param>
        /// <returns>An array of n bytes or null if the queue is empty or hasn't sufficient bytes to satisfy the operation</returns>
        public byte[] Dequeue(int numberOfBytes_)
        {
            if (_list.Count > 0 && numberOfBytes_ <= _list.Count)
            {
                byte[] result = new byte[numberOfBytes_];
                for (int loop = 0; loop < numberOfBytes_; ++loop)
                    result[loop] = _list[loop];
                for (int loop = 0; loop < numberOfBytes_; ++loop)
                    _list.RemoveAt(0);
                return result;
            }
            return null;
        }

        /// <summary>
        /// Removes the first n bytes from the start of the queue and returns them
        /// </summary>
        /// <param name="position_">Number of bytes to dequeue</param>
        /// <returns>An array of n bytes or null if the queue is empty or hasn't sufficient bytes to satisfy the operation</returns>
        public byte[] Dequeue(DatagramTemplatePosition position_)
        {
            if (_list.Count > 0 && position_.Length > -1 && position_.Length <= _list.Count)
            {
                for (int loop = 0; loop < position_.StartIndex; ++loop)
                    _list.RemoveAt(0);
                byte[] result = new byte[position_.Length];
                for (int loop = 0; loop < position_.Length; ++loop)
                    result[loop] = _list[loop];
                for (int loop = 0; loop < position_.Length; ++loop)
                    _list.RemoveAt(0);
                return result;
            }
            return null;
        }

        /// <summary>
        /// Returns the first byte without removing it from the queue
        /// </summary>
        /// <returns>The first byte or null if the queue is empty</returns>
        public byte? Peek()
        {
            if (_list.Count > 0)
                return _list[0];
            return null;
        }

        /// <summary>
        /// Returns up to the first n bytes in an array without removing them from the queue
        /// </summary>
        /// <returns>An array of bytes or null if the queue is empty</returns>
        public byte[] Peek(int numberOfBytes_)
        {
            return Peek(numberOfBytes_, false);
        }

        /// <summary>
        /// Returns the first n bytes in an array without removing them from the queue
        /// </summary>
        /// <returns>An array of bytes or null if the queue is empty</returns>
        public byte[] Peek(int numberOfBytes_, bool onlyReturnRequestedAmount_)
        {
            if (_list.Count > 0 && numberOfBytes_ > 0)
            {
                if (numberOfBytes_ > _list.Count && !onlyReturnRequestedAmount_)
                    numberOfBytes_ = _list.Count;

                if (numberOfBytes_ <= _list.Count)
                {
                    byte[] result = new byte[numberOfBytes_];
                    for (int loop = 0; loop < numberOfBytes_; ++loop)
                        result[loop] = _list[loop];
                    return result;
                }
            }
            return null;
        }

        public bool StartsWith(byte[] buffer_)
        {
            if (null != buffer_ && _list.Count >= buffer_.Length)
            {
                int index = -1;
                foreach (byte abyte in buffer_)
                {
                    if (!abyte.Equals(_list[++index]))
                        return false;
                }
                return true;
            }
            return false;
        }
        #endregion

        #region Public r/o property
        /// <summary>
        /// Gets the amount of bytes on the queue
        /// </summary>
        public int Count
        {
            get { return _list.Count; }
        }
        #endregion
    }

    public enum ParameterTypeFlag : short { ptByte = -1, ptUShort = -2, ptUInt = -3, ptULong = -4, ptChar = -5, ptVariableLengthString = -6, ptFixedLengthString = -7 };
    public class DataGramTemplate
    {
        #region Private members
        private string _name = string.Empty;
        private short[] _template = null;
        private int _parameterCount = 0;
        #endregion

        #region Constructors
        public DataGramTemplate(string name_, short[] template_)
        {
            if (null == template_)
                throw new ArgumentNullException("The template_ argument to the constructor must not be null.");
            if (name_.Length > 0)
            {
                _name = name_;
                _template = template_;
                foreach (short abyte in _template)
                {
                    if (abyte > byte.MaxValue)
                        throw new ArgumentOutOfRangeException("Argument out of range. The short values that define a template mask bytes, therefore the value may not exceed 255. However negative values are permitted as argument insertion type flags, use -1 for byte, -2 for short, -3 for int, -4 for long, -5 for char, -6 for variable length string or less than -6 for fixed length strings.");
                    if (abyte < 0)
                        ++_parameterCount;
                }
            }
            else
                throw new ArgumentException("The name_ argument passed to the constructor must consist of at least one character.");
        }

        public DataGramTemplate(string name_, params object[] parameters_)
        {
            if (null == parameters_)
                throw new ArgumentNullException("The parameters_ argument to the constructor must not be null.");
            if (0 == parameters_.Length)
                throw new ArgumentException("The parameters_ argument to the constructor must consist of at least one parameter.");
            if (name_.Length > 0)
            {
                _name = name_;
                ReadParameters(parameters_);
                foreach (short abyte in _template)
                {
                    if (abyte > byte.MaxValue)
                        throw new ArgumentOutOfRangeException("Argument out of range. The short values that define a template mask bytes, therefore the value may not exceed 255. However negative values are permitted as argument insertion type flags, use -1 for byte, -2 for short, -3 for int, -4 for long, -5 for char, -6 for variable length string or less than -6 for fixed length strings.");
                    if (abyte < 0)
                        ++_parameterCount;
                }
            }
            else
                throw new ArgumentException("The name_ argument passed to the constructor must consist of at least one character.");
        }
        #endregion

        #region Private methods
        private void ReadParameters(params object[] parameters_)
        {
            foreach (object parameter in parameters_)
            {
                if (parameter is short[] || parameter is byte[] || parameter is string || parameter is Int16)
                {
                    short[] argument = null;
                    if (parameter is short[])
                        AppendToTemplate((short[])parameter);
                    else if (parameter is byte[])
                    {
                        byte[] arg = (byte[])parameter;
                        argument = new short[arg.Length];
                        int index = -1;
                        foreach (byte abyte in arg)
                            argument[++index] = (short)abyte;
                    }
                    else if (parameter is string)
                    {
                        string arg = (string)parameter;
                        argument = new short[arg.Length];
                        int index = -1;
                        foreach (char character in arg)
                            argument[++index] = (short)character;
                    }
                    else
                    {
                        argument = new short[1];
                        argument[0] = (short)parameter;
                    }

                    if (null != argument)
                        AppendToTemplate(argument);
                }
                else
                    ThrowUnacceptableTypeException(parameter);
            }

            //_template = template_;
        }

        private void AppendToTemplate(short[] argument_)
        {
            int index = -1;
            if (null == _template)
                _template = new short[argument_.Length];
            else
            {
                short[] temp = new short[_template.Length];
                foreach (short value in _template)
                    temp[++index] = value;
                _template = new short[temp.Length + argument_.Length];
                index = -1;
                foreach (short value in temp)
                    _template[++index] = value;
            }
            foreach (short value in argument_)
                _template[++index] = value;
        }

        private void ThrowUnacceptableTypeException(object parameter_)
        {
            throw new ArgumentException("Parameter of type " + parameter_.GetType().ToString() + " detected. Acceptable types are: short[], byte[], string, byte, short.");
        }

        private void ThrowParameterTypeMismatchException(int argumentNumber_, object parameter_, ParameterTypeFlag flag_)
        {
            ++argumentNumber_;
            throw new ArgumentException("GetDatagram() argument #" + argumentNumber_.ToString() + " is of type " + parameter_.GetType().ToString() + ", type " + flag_.ToString().Substring(2) + " expected.");
        }

        private void ThrowParameterValueOutOfRangeException(int argumentNumber_, ParameterTypeFlag flag_)
        {
            ++argumentNumber_;
            throw new ArgumentOutOfRangeException("GetDatagram() argument #" + argumentNumber_.ToString() + " is beyond the permissable range of type " + flag_.ToString().Substring(2) + ".");
        }

        private byte[] ShortToBytes(ushort value_)
        {
            byte[] result = new byte[2];
            result[0] = (byte)value_;
            result[1] = (byte)(((uint)value_ >> 8) & 0xFF);
            return result;
        }

        private byte[] IntToBytes(uint value_)
        {
            byte[] result = new byte[4];
            result[0] = (byte)value_;
            result[1] = (byte)(((uint)value_ >> 8) & 0xFF);
            result[2] = (byte)(((uint)value_ >> 16) & 0xFF);
            result[3] = (byte)(((uint)value_ >> 24) & 0xFF);
            return result;
        }

        private byte[] LongToBytes(ulong value_)
        {
            byte[] result = new byte[8];
            result[0] = (byte)value_;
            result[1] = (byte)(((ulong)value_ >> 8) & 0xFF);
            result[2] = (byte)(((ulong)value_ >> 16) & 0xFF);
            result[3] = (byte)(((ulong)value_ >> 24) & 0xFF);
            result[4] = (byte)(((ulong)value_ >> 32) & 0xFF);
            result[5] = (byte)(((ulong)value_ >> 40) & 0xFF);
            result[6] = (byte)(((ulong)value_ >> 48) & 0xFF);
            result[7] = (byte)(((ulong)value_ >> 56) & 0xFF);
            return result;
        }
        #endregion

        #region Public overridden method
        public override string ToString()
        {
            return _name;
        }
        #endregion

        #region Public methods
        public byte[] GetDatagram()
        {
            if (0 == _parameterCount)
            {
                byte[] result = new byte[_template.Length];
                int index = -1;
                foreach (sbyte abyte in _template)
                    result[++index] = (byte)abyte;
                return result;
            }
            throw new Exception("This template is expecting parameters, GetDatagram() may only be called for hard coded templates that expect no parameters.");
        }

        public byte[] GetDatagram(params object[] parameters_)
        {
            if (parameters_.Length == _parameterCount)
            {
                int arraySize = 0;
                int parameterIndex = -1;
                foreach (short abyte in _template)
                {
                    ++arraySize;
                    if (abyte < 0)
                    {
                        ParameterTypeFlag flag = (ParameterTypeFlag)abyte;
                        switch (flag)
                        {
                            case ParameterTypeFlag.ptByte:
                                if (!(parameters_[++parameterIndex] is Int32 || parameters_[parameterIndex] is System.Byte))
                                    ThrowParameterTypeMismatchException(parameterIndex, parameters_[parameterIndex], flag);
                                if (parameters_[parameterIndex] is Int32 && (Int32)parameters_[parameterIndex] > byte.MaxValue)
                                    ThrowParameterValueOutOfRangeException(parameterIndex, flag);
                                break;
                            case ParameterTypeFlag.ptUShort:
                                if (!(parameters_[++parameterIndex] is Int32))
                                    ThrowParameterTypeMismatchException(parameterIndex, parameters_[parameterIndex], flag);
                                if ((Int32)parameters_[parameterIndex] > ushort.MaxValue)
                                    ThrowParameterValueOutOfRangeException(parameterIndex, flag);
                                ++arraySize;
                                break;
                            case ParameterTypeFlag.ptUInt:
                                if (!(parameters_[++parameterIndex] is Int32))
                                    ThrowParameterTypeMismatchException(parameterIndex, parameters_[parameterIndex], flag);
                                arraySize += 3;
                                break;
                            case ParameterTypeFlag.ptULong:
                                if (!(parameters_[++parameterIndex] is Int64) || (parameters_[++parameterIndex] is Int32))
                                    ThrowParameterTypeMismatchException(parameterIndex, parameters_[parameterIndex], flag);
                                arraySize += 7;
                                break;
                            case ParameterTypeFlag.ptChar:
                                if (!(parameters_[++parameterIndex] is Char))
                                    ThrowParameterTypeMismatchException(parameterIndex, parameters_[parameterIndex], flag);
                                break;
                            case ParameterTypeFlag.ptVariableLengthString:
                                if (!(parameters_[++parameterIndex] is string))
                                    ThrowParameterTypeMismatchException(parameterIndex, parameters_[parameterIndex], flag);
                                arraySize += ((string)parameters_[parameterIndex]).Length;
                                break;
                            default:
                                if (!(parameters_[++parameterIndex] is string))
                                    ThrowParameterTypeMismatchException(parameterIndex, parameters_[parameterIndex], flag);
                                arraySize += ((string)parameters_[parameterIndex]).Length;
                                break;
                        }
                    }
                }

                byte[] result = new byte[arraySize];
                parameterIndex = -1;
                int index = -1;
                foreach (short abyte in _template)
                {
                    if (abyte > -1)
                        result[++index] = (byte)abyte;
                    else
                    {
                        ParameterTypeFlag flag = (ParameterTypeFlag)abyte;
                        switch (flag)
                        {
                            case ParameterTypeFlag.ptByte:
                                if (parameters_[++parameterIndex] is Int32)
                                    result[++index] = Convert.ToByte((Int32)parameters_[parameterIndex]);
                                else
                                    result[++index] = (Byte)parameters_[parameterIndex];
                                break;
                            case ParameterTypeFlag.ptUShort:
                                ushort svalue = (ushort)Convert.ToInt16((ushort)parameters_[++parameterIndex]);
                                foreach (byte s in ShortToBytes(svalue))
                                    result[++index] = s;
                                break;
                            case ParameterTypeFlag.ptUInt:
                                uint ivalue = (uint)parameters_[++parameterIndex];
                                foreach (byte i in IntToBytes(ivalue))
                                    result[++index] = i;
                                break;
                            case ParameterTypeFlag.ptULong:
                                ulong lvalue = (ulong)parameters_[++parameterIndex];
                                foreach (byte l in LongToBytes(lvalue))
                                    result[++index] = l;
                                break;
                            case ParameterTypeFlag.ptChar:
                                result[++index] = (byte)(char)parameters_[++parameterIndex];
                                break;
                            case ParameterTypeFlag.ptVariableLengthString:
                                foreach (char character in (string)parameters_[++parameterIndex])
                                    result[++index] = (byte)character;
                                break;
                            default:
                                foreach (char character in (string)parameters_[++parameterIndex])
                                    result[++index] = (byte)character;
                                break;
                        }
                    }
                }

                return result;
            }

            string error = "GetDatagram() was expecting " + _parameterCount.ToString() + " argument";
            if (1 != _parameterCount)
                error += "s";
            error += ", " + parameters_.Length.ToString() + " parameter";
            error += 1 == parameters_.Length ? " was received." : "s were received.";
            throw new ArgumentException(error);
        }

        public List<short[]> Split()
        {
            List<short[]> result = new List<short[]>();
            List<short> element = new List<short>();
            foreach (short value in _template)
            {
                if (value > -1)
                    element.Add(value);
                else
                {
                    if (element.Count > 0)
                    {
                        short[] values = new short[element.Count];
                        int index = -1;
                        foreach (short elementValue in element)
                            values[++index] = elementValue;
                        element.Clear();
                        result.Add(values);
                    }
                    result.Add(new short[] { value });
                }
            }

            if (element.Count > 0)
            {
                short[] values = new short[element.Count];
                int index = -1;
                foreach (short elementValue in element)
                    values[++index] = elementValue;
                result.Add(values);
            }

            return result;
        }
        #endregion

        #region Public static methods
        public static string ForceStringLength(string value_, int length_)
        {
            if (value_.Length == length_)
                return value_;
            else if (value_.Length > length_)
                return value_.Substring(0, length_);
            int originalValueLength = value_.Length;
            for (int loop = 0; loop < length_ - originalValueLength; ++loop )
                value_ += " ";
            return value_;
        }

        public static byte[] ElementToBuffer(short[] element_)
        {
            if (element_.Length > 0)
            {
                byte[] result = new byte[element_.Length];
                int index = -1;
                foreach (short value in element_)
                {
                    if (value > -1 && value <= byte.MaxValue)
                        result[++index] = (byte)value;
                    else
                        return null;
                }
                return result;
            }
            return null;
        }

        public static short GetParam(ParameterTypeFlag flag_)
        {
            return (short)flag_;
        }

        public static short FixedStringParameterToShort(ushort length_)
        {
            if (0 == length_)
                throw new ArgumentOutOfRangeException("length_ - you may not call FixedStringParameterToShort() with an argument less than 1.");
            return (short)((short)ParameterTypeFlag.ptFixedLengthString - (length_ - 1));
        }
        #endregion

        #region Public r/o properties
        public bool IsValid
        {
            get { return _name.Length > 0 && null != _template && _template.Length > 0; }
        }

        public string Name
        {
            get { return _name; }
        }

        public int Length
        {
            get { return null == _template ? 0 : _template.Length; }
        }

        public short[] Template
        {
            get { return _template; }
        }

        public bool IsStringContentOnly
        {
            get
            {
                List<short[]> elements = Split();

                switch (elements.Count)
                {
                    case 2:
                        if (1 == elements[0].Length && elements[0][0] == (short)ParameterTypeFlag.ptVariableLengthString)
                            return (elements[1].Length > 1 || elements[1][0] > -1);
                        break;
                    case 3:
                        if (1 == elements[1].Length && elements[1][0] == (short)ParameterTypeFlag.ptVariableLengthString)
                            return ((elements[0].Length > 1 || elements[0][0] > -1) || (elements[2].Length > 1 || elements[2][0] > -1));
                        break;
                }

                return false;
            }
        }
        #endregion
    }

    public class DatagramPosition
    {
        public int Length = 0;
    }

    /// <summary>
    /// The type returned by DataBuffer.IndexOf() for searches of DatagramTemplates within buffers 
    /// </summary>
    public class DatagramTemplatePosition : DatagramPosition
    {
        #region Constructors
        public DatagramTemplatePosition()
        {
        }

        public DatagramTemplatePosition(int indexOfNext_)
        {
            IndexOfNext = indexOfNext_;
        }
        #endregion

        #region Public members
        public int StartIndex = -1;
        public int IndexOfNext = -1;
        #endregion

        #region Public r/o property
        public bool Found { get { return StartIndex > -1 && Length > 0; } }
        #endregion 
    }

    /// <summary>
    /// A class for manipulating an array of bytes in much the same way as a string
    /// </summary>
    //[Obsolete("This class has been deprecated, consider using DataBuffer instead.")]
    public class Databuffer
    {
        #region Private member
        private byte[] _buffer = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="buffer_">Buffer value to assign, may not be null else raises InvalidOperationException</param>
        /// <throws>InvalidOperationException</throws>
        public Databuffer(byte[] buffer_)
        {
            if (null == buffer_)
                throw new InvalidOperationException("DataBuffer cannot be initialised with a null value.");
            if (0 == buffer_.Length)
                throw new InvalidOperationException("DataBuffer cannot be initialised with an empty byte array value.");
            _buffer = buffer_;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="buffer_">Buffer value to assign from a string, may not be null else raises InvalidOperationException</param>
        /// <throws>InvalidOperationException</throws>
        public Databuffer(string buffer_)
        {
            if (null == buffer_)
                throw new InvalidOperationException("DataBuffer cannot be initialised with a null value.");
            if (0 == buffer_.Length )
                throw new InvalidOperationException("DataBuffer cannot be initialised with an empty string value.");
            _buffer = StringToBuffer(buffer_);
        }

        public Databuffer(byte byte_)
        {
            _buffer = new byte[1];
            _buffer[0] = byte_;
        }

        public Databuffer(char character_)
        {
            _buffer = new byte[1];
            _buffer[0] = (byte)character_;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Searches for the supplied sub-buffer within the suppled main buffer
        /// </summary>
        /// <param name="subBuffer_">Buffer to seek</param>
        /// <param name="mainBuffer_">Buffer in which to seek</param>
        /// <returns>The start index of the supplied sub-buffer within the suppled main buffer or -1 if not found</returns>
        private int IndexOf(byte[] subBuffer_, byte[] mainBuffer_)
        {
            if (mainBuffer_.Length >= subBuffer_.Length)
            {
                for (int loop = 0; loop < mainBuffer_.Length; ++loop)
                {
                    if (mainBuffer_[loop] == subBuffer_[0])
                    {
                        if (1 == subBuffer_.Length)
                            return loop;
                        if (loop <= mainBuffer_.Length - subBuffer_.Length)
                        {
                            bool haveMatch = false;
                            int subIndex = 0;
                            for (int iloop = loop + 1; iloop < loop + subBuffer_.Length; ++iloop)
                            {
                                haveMatch = mainBuffer_[iloop] == subBuffer_[++subIndex];
                                if (!haveMatch)
                                    break;
                            }
                            if (haveMatch)
                                return loop;
                        }
                    }
                }

            }
            return -1;
        }

        /// <summary>
        /// Searches for the supplied template within the supplied buffer.
        /// </summary>
        /// <param name="template_">Template to seek</param>
        /// <param name="mainBuffer_">Buffer in which to seek</param>
        /// <returns>The start index of the supplied template within the suppled main buffer or null if not found</returns>
        private DatagramTemplatePosition IndexOf(DataGramTemplate template_, byte[] mainBuffer_)
        {
            if (template_.IsValid && mainBuffer_.Length >= template_.Length)
            {
                long lastAssumedStringLength = -1;
                List<short[]> elements = template_.Split();
                if (elements.Count > 0)
                {
                    bool firstPass = true;
                    int index = -1;
                    DatagramTemplatePosition result = new DatagramTemplatePosition(0);
                    Databuffer buffer = new Databuffer(mainBuffer_);

                    if (template_.IsStringContentOnly)
                    {
                        switch (elements.Count)
                        {
                            case 2:
                                index = buffer.IndexOf(DataGramTemplate.ElementToBuffer(elements[1]));
                                if (index > -1)
                                {
                                    result.StartIndex = 0;
                                    result.Length = index + elements[1].Length;
                                    result.IndexOfNext = -1;
                                    return result;
                                }
                                break;
                            case 3:
                                int start = buffer.IndexOf(DataGramTemplate.ElementToBuffer(elements[0]));
                                int end = 0;
                                uint instance = 0;
                                while (end > -1 && end <= start)
                                    end = buffer.IndexOf(DataGramTemplate.ElementToBuffer(elements[2]), ++instance);

                                if (start > -1)
                                {
                                    result.StartIndex = start;
                                    result.Length = end > start ? (int)((end + elements[2].Length) - start) : -1;
                                    result.IndexOfNext = -1;
                                    return result;
                                }
                                break;
                        }

                        return null;
                    }

                    foreach (short[] element in elements)
                    {
                        byte[] subBuffer = DataGramTemplate.ElementToBuffer(element);
                        if (null != subBuffer || 1 == element.Length)
                        {
                            if (null != subBuffer)
                            {
                                index = buffer.IndexOf(subBuffer);
                                if (firstPass ? index > -1 : 0 == index)
                                {
                                    if (-1 == result.StartIndex)
                                        result.StartIndex = index;
                                    result.IndexOfNext += index + subBuffer.Length;
                                    result.Length += subBuffer.Length;
                                    mainBuffer_ = buffer.SubBuffer(index + subBuffer.Length);
                                    if (0 == mainBuffer_.Length)
                                        result.IndexOfNext = -1;
                                }
                                else
                                    return null;
                            }
                            else
                            {
                                if (1 == element.Length && element[0] > -1)
                                    return null;

                                buffer = new Databuffer(mainBuffer_);
                                ParameterTypeFlag flag = (ParameterTypeFlag)element[0];
                                byte[] intBuffer = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
                                switch (flag)
                                {
                                    case ParameterTypeFlag.ptByte:
                                    case ParameterTypeFlag.ptChar:
                                        intBuffer[0] = buffer.Buffer[0];
                                        ++result.IndexOfNext;
                                        mainBuffer_ = buffer.SubBuffer(1);
                                        ++result.Length;
                                        break;
                                    case ParameterTypeFlag.ptUShort:
                                        intBuffer[0] = buffer.Buffer[0];
                                        intBuffer[0] = buffer.Buffer[1];
                                        result.IndexOfNext += 2;
                                        mainBuffer_ = buffer.SubBuffer(2);
                                        result.Length += 2;
                                        break;
                                    case ParameterTypeFlag.ptUInt:
                                        for (int loop = 0; loop < 4; ++loop)
                                            intBuffer[loop] = buffer.Buffer[loop];
                                        result.IndexOfNext += 4;
                                        mainBuffer_ = buffer.SubBuffer(4);
                                        result.Length += 4;
                                        break;
                                    case ParameterTypeFlag.ptULong:
                                        for (int loop = 0; loop < 8; ++loop)
                                            intBuffer[loop] = buffer.Buffer[loop];
                                        result.IndexOfNext += 8;
                                        mainBuffer_ = buffer.SubBuffer(8);
                                        result.Length += 8;
                                        break;
                                    case ParameterTypeFlag.ptVariableLengthString:
                                        intBuffer = null;
                                        if (lastAssumedStringLength > -1)
                                        {
                                            result.IndexOfNext += (int)lastAssumedStringLength;
                                            mainBuffer_ = buffer.SubBuffer((int)lastAssumedStringLength);
                                            result.Length += (int)lastAssumedStringLength;
                                            lastAssumedStringLength = -1;
                                        }
                                        else
                                            return null;
                                        break;
                                    default: // Fixed string length
                                        intBuffer = null;
                                        short stringLength = FixedStringParameterFlagToStringLength((short)flag);
                                        if (-1 == stringLength)
                                            return null;
                                        result.IndexOfNext += (int)stringLength;
                                        mainBuffer_ = buffer.SubBuffer((int)stringLength);
                                        result.Length += (int)stringLength;
                                        lastAssumedStringLength = -1;
                                        break;
                                }
                                if (null == mainBuffer_)
                                    return null;
                                if (mainBuffer_.Length > 0)
                                    buffer = new Databuffer(mainBuffer_);
                                else
                                    buffer = null;
                                if (null != intBuffer)
                                    lastAssumedStringLength = BytesToLong(intBuffer);
                            }
                        }
                        else
                            return null;
                        firstPass = false;
                    }
                    return result;
                }
            }

            return null;
        }

        public short FixedStringParameterFlagToStringLength(short flag_)
        {
            if (flag_ <= (short)ParameterTypeFlag.ptFixedLengthString)
            {
                short result = (short)((short)ParameterTypeFlag.ptVariableLengthString - flag_);
                return result;
            }
            return -1;
        }

        public long BytesToLong(byte[] number_)
        {
            long result = 0;
            //long multiplier = true == _isLittleEndian ? 1 : GetFirstBigEndianValue(number_.Length);
            long multiplier = 1;
            for (int loop = 0; loop < number_.Length; ++loop)
            {
                result += number_[loop] * multiplier;
                /*
                if (true == _isLittleEndian)
                    multiplier *= 256;
                else
                    multiplier /= 256;
                */
                multiplier *= 256;
            }
            return result;
        }

        /// <summary>
        /// Splits the supplied buffer at the supplied index.
        /// </summary>
        /// <param name="buffer_">Buffer to split</param>
        /// <param name="splitIndex_">Index at which to split</param>
        /// <returns>The portion of the buffer starting at the split point or null if the split point is out of range</returns>
        private byte[] Split(byte[] buffer_, int splitIndex_, ref int bytesRemoved_)
        {
            if (splitIndex_ < buffer_.Length)
            {
                bytesRemoved_ += splitIndex_ + 1;
                Databuffer buffer = new Databuffer(buffer_);
                return buffer.SubBuffer(splitIndex_);
            }
            return null;
        }

        /// <summary>
        /// Converts a string to an array of bytes
        /// </summary>
        /// <param name="value_">String to convert</param>
        /// <returns>The string as an array of bytes</returns>
        private byte[] StringToBuffer(string value_)
        {
            byte[] result = new byte[value_.Length];
            int index = -1;
            foreach(char character in value_)
                result[++index] = (byte)character;
            return result;
        }

        private byte[] ByteToBuffer(byte byte_)
        {
            byte[] result = new byte[1];
            result[0] = byte_;
            return result;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Looks for the supplied buffer at the start of the internal buffer
        /// </summary>
        /// <param name="subBuffer_">Buffer to seek at start</param>
        /// <returns>True if the buffer was found</returns>
        public bool StartsWith(byte[] subBuffer_)
        {
            if (_buffer.Length >= subBuffer_.Length)
            {
                for (int loop = 0; loop < subBuffer_.Length; ++loop)
                {
                    if (_buffer[loop] != subBuffer_[loop])
                        return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Looks for the supplied string at the start of the internal buffer
        /// </summary>
        /// <param name="subBuffer_">Buffer as a string to seek at start</param>
        /// <returns>True if the buffer was found</returns>
        public bool StartsWith(string subBuffer_)
        {
            return StartsWith(StringToBuffer(subBuffer_));
        }

        /// <summary>
        /// Looks for the supplied buffer at the end of the internal buffer
        /// </summary>
        /// <param name="value_">Buffer to seek at end</param>
        /// <returns>True if the buffer was found</returns>
        public bool EndsWith(byte[] value_)
        {
            if (_buffer.Length >= value_.Length)
            {
                int index = _buffer.Length - value_.Length;
                foreach (byte abyte in value_)
                {
                    if (_buffer[index++] != abyte)
                        return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Looks for the supplied string at the end of the internal buffer
        /// </summary>
        /// <param name="value_">Buffer as a string to seek at end</param>
        /// <returns>True if the buffer was found</returns>
        public bool EndsWith(string value_)
        {
            return EndsWith(StringToBuffer(value_));
        }

        /// <summary>
        /// Looks for the supplied DataBuffer's buffer at the end of the internal buffer
        /// </summary>
        /// <param name="buffer_">DataBuffer to seek at end</param>
        /// <returns>True if the buffer was found</returns>
        public bool EndsWith(Databuffer buffer_)
        {
            return EndsWith(buffer_.Buffer);
        }

        /// <summary>
        /// Looks for the supplied byte at the end of the internal buffer
        /// </summary>
        /// <param name="byte_">Byte to seek at end</param>
        /// <returns>True if the byte was found</returns>
        public bool EndsWith(byte byte_)
        {
            return EndsWith(ByteToBuffer(byte_));
        }

        /// <summary>
        /// Searches for the first instance of the supplied buffer within the internal buffer
        /// </summary>
        /// <param name="subBuffer_">Buffer to seek</param>
        /// <returns>The start index of the supplied buffer within the internal buffer or -1 if not found</returns>
        public int IndexOf(byte[] subBuffer_)
        {
            return IndexOf(subBuffer_, _buffer);
        }
        
        /// <summary>
        /// Searches for the nth instance of the supplied buffer within the internal buffer
        /// </summary>
        /// <param name="subBuffer_">Buffer to seek</param>
        /// <param name="instance_">Instance to find</param>
        /// <returns>The start index of the nth instance of the supplied buffer within the internal buffer or -1 if not found</returns>
        public int IndexOf(byte[] subBuffer_, uint instance_)
        {
            if (0 == instance_)
                return -1;

            int result = IndexOf(subBuffer_);
            if (1 == instance_)
                return result;

            if (result > -1)
            {
                int start = result + subBuffer_.Length;
                result = -1;
                int currentInstance = 1;
                int end = _buffer.Length - subBuffer_.Length;
                bool matching = false;
                for (int loop = start; loop <= end; ++loop)
                {
                    if (_buffer[loop] == subBuffer_[0])
                    {
                        result = loop;
                        if (subBuffer_.Length > 1)
                        {
                            int subIndex = 0;
                            for (int iLoop = loop + 1; iLoop < loop + subBuffer_.Length; ++iLoop)
                            {
                                matching = _buffer[iLoop] == subBuffer_[++subIndex];
                                if (!matching)
                                    break;
                            }
                        }
                        else
                            matching = true;
                    }
                    if (matching)
                    {
                        ++currentInstance;
                        matching = currentInstance == instance_;
                        if (matching)
                            break;
                        else
                            result = -1;
                    }
                    else
                        result = -1;
                }
            }
            return result;
        }

        /// <summary>
        /// Searches for the first instance of the supplied string within the internal buffer
        /// </summary>
        /// <param name="value_">Buffer as string to seek</param>
        /// <returns>The start index of the supplied string within the internal buffer or null if not found</returns>
        public int IndexOf(string value_)
        {
            return IndexOf(StringToBuffer(value_));
        }

        /// <summary>
        /// Searches for the first instance of the supplied template within the internal buffer
        /// </summary>
        /// <param name="template_">Template to seek</param>
        /// <returns>A DataTemplatePosition whose StartIndex memner is the start index of the supplied template within the internal buffer or null if not found</returns>
        public DatagramTemplatePosition IndexOf(DataGramTemplate template_)
        {
            return IndexOf(template_, 1);
        }

        /// <summary>
        /// Searches for the nth instance of the supplied template within the internal buffer
        /// </summary>
        /// <param name="template_">Template to seek</param>
        /// <param name="instance_">Instance to find</param>
        /// <returns>A DatagramTemplatePosition whose StartIndex member is the start index of the supplied template within the internal buffer or -1 if not found</returns>
        public DatagramTemplatePosition IndexOf(DataGramTemplate template_, uint instance_)
        {
            if (null == template_)
                return null;

            if (instance_ > 0)
            {
                DatagramTemplatePosition result = new DatagramTemplatePosition();
                byte[] buffer = new byte[_buffer.Length];
                int index = -1;
                foreach (byte abyte in _buffer)
                    buffer[++index] = abyte;
                result.StartIndex = 0;
                uint iterations = 0;
                while (iterations < instance_)
                {
                    if (null == buffer)
                        return null;
                    DatagramTemplatePosition position = IndexOf(template_, buffer);
                    if (null == position || -1 == position.StartIndex)
                    {
                        result = null;
                        break;
                    }
                    else if (position.Length > 0)
                        result.Length = position.Length;
                    ++iterations;
                    if (iterations < instance_)
                    {
                        result.StartIndex += position.IndexOfNext;
                        Databuffer dataBuffer = new Databuffer(buffer);
                        // Cut the buffer
                        buffer = dataBuffer.SubBuffer(position.IndexOfNext);
                    }
                    else
                    {
                        if (1 == instance_)
                        {
                            result.StartIndex = position.StartIndex;
                            result.Length = position.Length;
                        }
                        else
                            result.StartIndex += position.StartIndex;
                    }
                }
                return result;
            }

            return null;
        }

        /// <summary>
        /// Searches for the nth instance of the supplied string within the internal buffer
        /// </summary>
        /// <param name="value_">Buffer as string to seek</param>
        /// <param name="instance_">Instance to find</param>
        /// <returns>The start index of the supplied string within the internal buffer or -1 if not found</returns>
        public int IndexOf(string value_, uint instance_)
        {
            return IndexOf(StringToBuffer(value_), instance_);
        }

        /// <summary>
        /// Searches for the supplied buffer within the internal buffer
        /// </summary>
        /// <param name="subBuffer_">Buffer to seek</param>
        /// <returns>True if the buffer was found</returns>
        public bool Contains(byte[] subBuffer_)
        {
            return IndexOf(subBuffer_) > -1;
        }

        /// <summary>
        /// Searches for the supplied string within the internal buffer
        /// </summary>
        /// <param name="subBuffer_">Buffer as a string to seek</param>
        /// <returns>True if the string was found</returns>
        public bool Contains(string value_)
        {
            return Contains(StringToBuffer(value_));
        }

        /// <summary>
        /// Searches for the supplied byte within the internal buffer
        /// </summary>
        /// <param name="byte_">Byte to seek</param>
        /// <returns>True if the byte was found</returns>
        public bool Contains(byte byte_)
        {
            return Contains(ByteToBuffer(byte_));
        }

        /// <summary>
        /// Searches for the supplied buffer within the internal buffer
        /// </summary>
        /// <param name="buffer_">Buffer to seek</param>
        /// <returns>True if the buffer was found</returns>
        public bool Contains(Databuffer buffer_)
        {
            return Contains(buffer_.Buffer);
        }

        public uint GetInstanceCount(string buffer_)
        {
            return GetInstanceCount(StringToBuffer(buffer_));
        }

        public uint GetInstanceCount(byte byte_)
        {
            return GetInstanceCount(ByteToBuffer(byte_));
        }

        public uint GetInstanceCount(Databuffer buffer_)
        {
            return GetInstanceCount(buffer_.Buffer);
        }

        public uint GetInstanceCount(byte[] buffer_)
        {
            uint result = 0;
            int index = 0;
            uint instance = 0;
            while (index > -1)
            {
                index = IndexOf(buffer_, ++instance);
                if (index > -1)
                    ++result;
            }
            return result;
        }

        /// <summary>
        /// Gets a sub buffer from the supplied start index to the end of the buffer
        /// </summary>
        /// <param name="startIndex_">Index to start at</param>
        /// <returns>A sub buffer starting at the supplied index up to the end of the buffer</returns>
        public byte[] SubBuffer(int startIndex_)
        {
            return SubBuffer(startIndex_, _buffer.Length - startIndex_);
        }

        /// <summary>
        /// Gets a sub buffer from the supplied start index for the amount of bytes specified 
        /// </summary>
        /// <param name="startIndex_">Index to start at</param>
        /// <param name="length_">Number of bytes to copy</param>
        /// <returns>A sub buffer starting at the supplied index up to the supplied length</returns>
        public byte[] SubBuffer(int startIndex_, int length_)
        {
            if (0 == length_)
                return null;

            if (length_ > -1 && startIndex_ > -1 && length_ + startIndex_ <= _buffer.Length)
            {
                byte[] result = new byte[length_];
                int index = -1;
                for (int loop = startIndex_; loop < length_ + startIndex_; ++loop)
                    result[++index] = _buffer[loop];
                return result;
            }
            return null;
        }

        /// <summary>
        /// Inserts the supplied buffer into the internal buffer at the specified zero based index
        /// </summary>
        /// <param name="index_">Index to insert at</param>
        /// <param name="buffer_">Buffer to insert</param>
        /// <returns>An array of bytes containing the internal buffer with the inserted data</returns>
        public byte[] Insert(int index_, byte[] buffer_)
        {
            if (index_ < 0 || index_ >= _buffer.Length)
                throw new IndexOutOfRangeException("The supplied index falls outside of the internal buffers' range.");

            byte[] result = new byte[_buffer.Length + buffer_.Length];

            for (int loop = 0; loop < index_; ++loop)
                result[loop] = _buffer[loop];
            for (int loop = index_; loop < index_ + buffer_.Length; ++loop)
                result[loop] = buffer_[loop - index_];
            for (int loop = index_ + buffer_.Length; loop < result.Length; ++loop)
                result[loop] = _buffer[loop - buffer_.Length];

            return result;
        }

        /// <summary>
        /// Inserts the supplied byte into the internal buffer at the specified zero based index
        /// </summary>
        /// <param name="index_">Index to insert at</param>
        /// <param name="byte_">Byte to insert</param>
        /// <returns>An array of bytes containing the internal buffer with the inserted byte</returns>
        public byte[] Insert(int index_, byte byte_)
        {
            return Insert(index_, ByteToBuffer(byte_));
        }

        /// <summary>
        /// Inserts the supplied buffer into the internal buffer at the specified zero based index
        /// </summary>
        /// <param name="index_">Index to insert at</param>
        /// <param name="buffer_">Buffer as a string to insert</param>
        /// <returns>An array of bytes containing the internal buffer with the inserted byte</returns>
        public byte[] Insert(int index_, string buffer_)
        {
            return Insert(index_, StringToBuffer(buffer_));
        }
        
        /// <summary>
        /// Returns a new buffer in which all the bytes in the current instance, beginning at a specified position and continuing through the last position, have been deleted.
        /// </summary>
        /// <param name="startIndex_">Zero based position to begin deleting bytes</param>
        /// <returns>A buffer with the bytes removed from this instance</returns>
        public byte[] Remove(int startIndex_)
        {
            return Remove(startIndex_, _buffer.Length - startIndex_);
        }

        /// <summary>
        /// Returns a new buffer in which a specified number of bytes in the current this instance beginning at a specified position have been deleted.
        /// </summary>
        /// <param name="startIndex_">Zero based position to begin deleting bytes</param>
        /// <param name="count_">Number of bytes from start position to delete</param>
        /// <returns>A buffer with the bytes removed from this instance</returns>
        public byte[] Remove(int startIndex_, int count_)
        {
            byte[] result = new byte[_buffer.Length - count_];
            int index = -1;
            int insertIndex = -1;
            foreach (byte abyte in _buffer)
            {
                if (++index < startIndex_ || index >= startIndex_ + count_)
                    result[++insertIndex] = abyte;
            }
            return result;
        }

        /// <summary>
        /// Replaces all instances of oldByte_ in the internal buffer with newByte_.
        /// </summary>
        /// <param name="oldByte_">Byte to replace</param>
        /// <param name="newByte_">Byte to replace with</param>
        /// <returns>Returns a new byte array with all occurrences of oldByte_ replaced with newByte_</returns>
        public byte[] Replace(byte oldByte_, byte newByte_)
        {
            byte[] result = new byte[_buffer.Length];
            int index = -1;
            foreach (byte abyte in _buffer)
            {
                if (abyte == oldByte_)
                    result[++index] = newByte_;
                else
                    result[++index] = abyte;
            }
            return result;
        }

        /// <summary>
        /// Replaces all instances of oldBytes_ in the internal buffer with newBytes_.
        /// </summary>
        /// <param name="oldBytes_">Bytes to replace</param>
        /// <param name="newBytes_">Bytes to replace with</param>
        /// <returns>Returns a new byte array with all occurrences of oldBytes_ replaced with newBytes_</returns>
        public byte[] Replace(byte[] oldBytes_, byte[] newBytes_)
        {
            uint instance = 0;
            int index = 0;

            // Get the number of instances to replace
            List<int> indicies = new List<int>();
            while (index > -1)
            {
                index = IndexOf(oldBytes_, ++instance);
                if (index > -1)
                    indicies.Add(index);
            }

            if (indicies.Count > 0)
            {
                int newLength = 0;
                if (oldBytes_.Length == newBytes_.Length)
                    newLength = _buffer.Length;
                else
                    newLength = (_buffer.Length - (oldBytes_.Length * indicies.Count)) + newBytes_.Length * indicies.Count;

                byte[] result = new byte[newLength];
                index = -1;
                int start = 0;
                foreach (int insertPoint in indicies)
                {
                    for (int loop = start; loop < insertPoint; ++loop)
                        result[++index] = _buffer[loop];
                    start = insertPoint + oldBytes_.Length;
                    for (int loop = 0; loop < newBytes_.Length; ++loop)
                        result[++index] = newBytes_[loop];
                }

                if (start < _buffer.Length)
                {
                    for (int loop = start; loop < _buffer.Length; ++loop)
                        result[++index] = _buffer[loop];
                }
                return result;
            }
            return null;
        }

        /// <summary>
        /// Replaces all instances of oldValue_ in the internal buffer with newValue_.
        /// </summary>
        /// <param name="oldValue_">String to replace</param>
        /// <param name="newValue_">String to replace with</param>
        /// <returns>Returns a new byte array with all occurrences of oldValue_ replaced with newValue_</returns>
        public byte[] Replace(string oldValue_, string newValue_)
        {
            return Replace(StringToBuffer(oldValue_), StringToBuffer(newValue_));
        }
        /// <summary>
        /// Returns the buffer in reverse order
        /// </summary>
        /// <returns></returns>
        public byte[] Reverse()
        {
            byte[] result = new byte[_buffer.Length];
            int index = -1;
            for (int loop = _buffer.Length - 1; loop > -1; --loop)
                result[++index] = _buffer[loop];
            return result;
        }

        /// <summary>
        /// Seeks all bytes within character range a-z and converts then to A-Z
        /// </summary>
        /// <returns>The buffer with all bytes in char range a-z converted to A-Z</returns>
        public byte[] ToUpper()
        {
            byte[] result = new byte[_buffer.Length];
            int index = -1;
            foreach (byte abyte in _buffer)
            {
                if (abyte >= 0x61 && abyte <= 0x7A)
                    result[++index] = ((byte)(abyte - 0x20));
                else
                    result[++index] = abyte;
            }
            return result;
        }

        /// <summary>
        /// Seeks all bytes within character range A-Z and converts then to a-z
        /// </summary>
        /// <returns>The buffer with all bytes in char range A-Z converted to a-z</returns>
        public byte[] ToLower()
        {
            byte[] result = new byte[_buffer.Length];
            int index = -1;
            foreach (byte abyte in _buffer)
            {
                if (abyte >= 0x41 && abyte <= 0x5A)
                    result[++index] = ((byte)(abyte + 0x20));
                else
                    result[++index] = abyte;
            }
            return result;
        }

        /// <summary>
        /// Returns the buffer as a list of 0x00 delimited strings
        /// </summary>
        /// <returns>A List of strings</returns>
        public List<string> ToStrings()
        {
            List<string> result = new List<string>();
            string bufferString = string.Empty;
            foreach (byte abyte in _buffer)
            {
                if (abyte > 0x00)
                    bufferString += (char)abyte;
                else
                {
                    if (bufferString.Length > 0)
                    {
                        result.Add(bufferString);
                        bufferString = string.Empty;
                    }
                }
            }
            if (bufferString.Length > 0)
                result.Add(bufferString);
            return result;
        }

        public uint GetInstanceCount(DataGramTemplate template_)
        {
            uint result = 0;
            int index = 0;
            uint instance = 0;
            while (index > -1)
            {
                index = IndexOf(template_, ++instance).StartIndex;
                if (index > -1)
                    ++result;
            }
            return result;
        }

        public bool StartsWith(DataGramTemplate template_)
        {
            return 0 == IndexOf(template_).StartIndex;
        }

        public bool Contains(DataGramTemplate template_)
        {
            return IndexOf(template_).StartIndex > -1;
        }

        public bool EndsWith(DataGramTemplate template_)
        {
            int index = IndexOf(template_).StartIndex;
            if (index > -1)
                return index == _buffer.Length - template_.Template.Length;
            return false;
        }
        /// <summary>
        /// Returns the buffer as a string omitting all 0x00's
        /// </summary>
        /// <returns>Buffer as a string</returns>
        public override string ToString()
        {
            string result = string.Empty;
            foreach (byte abyte in _buffer)
            {
                if (abyte > 0x00)
                    result += (char)abyte;
            }
            return result;
        }

        /// <summary>
        /// Determines whether the specified System.Object is equal to the current internal buffer. Valid System.Objects are DataBuffer's, byte[] arrays, single bytes, strings or
        /// DatagramTemplates.
        /// </summary>
        /// <param name="obj">Object to compare</param>
        /// <returns>True if the supplied object is a DataBuffer, byte array or string and the supplied byte array is identical to the internal buffer.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Databuffer || obj is byte[] || obj is string || obj is Int32 || obj is DataGramTemplate)
            {
                if (obj is DataGramTemplate)
                {
                    DataGramTemplate template = (DataGramTemplate)obj;
                    if (template.Length == _buffer.Length)
                    {
                        int index = -1;
                        foreach (byte abyte in _buffer)
                        {
                            if (template.Template[++index] > (short)ParameterTypeFlag.ptByte)
                            {
                                if (template.Template[index] != abyte)
                                    return false;
                            }
                        }
                        return true;
                    }
                }
                else
                {
                    byte[] buffer = null;
                    if (obj is string)
                        buffer = StringToBuffer((string)obj);
                    else if (obj is Databuffer)
                        buffer = ((Databuffer)obj).Buffer;
                    else if (obj is byte[])
                        buffer = (byte[])obj;
                    else
                    {
                        int test = (Int32)obj;
                        if (test < 0 || test > byte.MaxValue)
                            return false;
                        buffer = new byte[1];
                        buffer[0] = (byte)(Int32)obj;
                    }

                    if (buffer.Length == _buffer.Length)
                    {
                        int index = -1;
                        foreach (byte abyte in _buffer)
                        {
                            if (buffer[++index] != abyte)
                                return false;
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Serves as a hash function for a particular type
        /// </summary>
        /// <returns>An integer</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region Public void procedures that edit the internal buffer
        /// <summary>
        /// Adds the supplied byte to the end of the internal buffer.
        /// </summary>
        /// <param name="byte_">Byte to add</param>
        public void AppendToBuffer(byte byte_)
        {
            byte[] buffer = new byte[1];
            buffer[0] = byte_;
            AppendToBuffer(buffer);
        }

        /// <summary>
        /// Adds the supplied buffer to the end of the internal buffer.
        /// </summary>
        /// <param name="buffer_">Buffer to add</param>
        public void AppendToBuffer(byte[] buffer_)
        {
            byte[] copy = new byte[_buffer.Length];
            int index = -1;
            foreach (byte abyte in _buffer)
                copy[++index] = abyte;
            _buffer = new byte[copy.Length + buffer_.Length];
            index = -1;
            foreach (byte abyte in copy)
                _buffer[++index] = abyte;
            foreach (byte abyte in buffer_)
                _buffer[++index] = abyte;
        }

        /// <summary>
        /// Adds the supplied DataBuffer's buffer to the end of the internal buffer
        /// </summary>
        /// <param name="buffer_">Buffer to add</param>
        public void AppendToBuffer(Databuffer buffer_)
        {
            AppendToBuffer(buffer_.Buffer);
        }

        /// <summary>
        /// Adds the supplied string to the end of the internal buffer.
        /// </summary>
        /// <param name="value_">Buffer as a string to add</param>
        public void AppendToBuffer(string value_)
        {
            AppendToBuffer(StringToBuffer(value_));
        }

        public void DeleteFromBuffer(int startIndex_)
        {
            DeleteFromBuffer(startIndex_, _buffer.Length - startIndex_);
        }

        public void DeleteFromBuffer(int startIndex_, int count_)
        {
            byte[] buffer = Remove(startIndex_, count_);
            if (null == buffer)
                throw new NullReferenceException("Skulled");
            _buffer = new byte[buffer.Length];
            int index = -1;
            foreach (byte abyte in buffer)
                _buffer[++index] = abyte;
        }
        #endregion

        #region Public r/o properties
        /// <summary>
        /// Gets the buffer value
        /// </summary>
        public byte[] Buffer
        {
            get { return _buffer; }
        }

        /// <summary>
        /// Gets the buffer length
        /// </summary>
        public int Length
        {
            get { return _buffer.Length; }
        }

        /// <summary>
        /// Gets the buffer as a formatted hex string
        /// </summary>
        public string Hex
        {
            get { return BitConverter.ToString(_buffer); }
        }

        /// <summary>
        /// Gets a value reflecting as to whether all bytes represent character 0x20 (space) through 0x7E (tilde) 
        /// </summary>
        public bool IsPrintable
        {
            get
            {
                foreach (byte abyte in _buffer)
                {
                    if (!(abyte >= 0x20 && abyte <= 0x7E))
                        return false;
                }
                return true;
            }
        }
        #endregion

        #region Work in progress

        #endregion
    }
}