﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bncs.SysUtils;

namespace Fiesta
{
    public class FiestaXMLPreParser
    {
        private Databuffer _mainBuffer = null;

        private byte[] GetNextMessage()
        {
            if (null == _mainBuffer)
                return null;

            int startIndex = _mainBuffer.IndexOf("<");
            if (-1 == startIndex)
            {
                _mainBuffer = null;
                return null;
            }

            int endIndex = _mainBuffer.IndexOf(">");

            if (endIndex > startIndex)
            {
                byte[] messageStart = _mainBuffer.SubBuffer(startIndex, (endIndex - startIndex) + 1);
                byte[] messageEnd = new byte[messageStart.Length + 1];
                int element = 0;
                foreach (byte abyte in messageStart)
                {
                    if (1 != element)
                        messageEnd[element] = abyte;
                    else
                    {
                        messageEnd[element] = (byte)'/';
                        messageEnd[++element] = abyte;
                    }
                    ++element;
                }

                endIndex = _mainBuffer.IndexOf(messageEnd);
                if (endIndex > -1)
                {
                    byte[] result = _mainBuffer.SubBuffer(startIndex, endIndex + messageEnd.Length);
                    byte[] buffer = _mainBuffer.SubBuffer(endIndex + messageEnd.Length);
                    _mainBuffer = null == buffer ? null : new Databuffer(buffer);
                    return result;
                }
            }
            return null;
        }

        public List<byte[]> ParseData(byte[] data_)
        {
            List<byte[]> result = new List<byte[]>();

            if (null == _mainBuffer)
                _mainBuffer = new Databuffer(data_);
            else
                _mainBuffer.AppendToBuffer(data_);

            byte[] message;
            while (null != (message = GetNextMessage()))
                result.Add(message);
            
            return result.Count > 0 ? result : null;
        }

        public int BufferSize
        {
            get { return null == _mainBuffer ? 0 : _mainBuffer.Buffer.Length; }
        }
    }
}