﻿#define CC_G1

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using System.Xml;
using DigitalGiles.BNCS;
using DigitalGiles.INIFile;
using DigitalGiles.External;
using BNCS;
using Bncs.SysUtils;
using Bncs.Comms;
using Bncs.Controls;
using Bncs.WPF;
using Bncs.Execute;
using Bncs.Assembly;
using Bncs.Debug;
using Fiesta;

namespace s3_fiesta
{
    #region Enumerations
    public enum IDWriteResult { Success, Failure, NotConnected, Deffering };
    public enum EventCatergories { Transmit, Receive, Startup, Comms, Bncs, Error, Value, Registration, ConfigurationError, Operations, LifeCycle };
    public enum DriverProgress { LoggingIn, GatheringUnitsList, GatheringUnitParameters, RunningInitialPoll, SubscribingToValueChanges, WaitingForValueChanges, ShuttingDown, ClearToExit };
    #endregion

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private delegate
        private delegate void ClientReceiveCallbackDelegate(ProcessDataResult result_, byte[] data_);
        #endregion

        #region Private constants
        const uint SLOT_MAX_AVAILABLE = 4090;
        const uint SLOT_DISCOVERED_UNITS = 4091;
        const uint SLOT_CONFIGURED_INSTANCES = 4092;
        const uint SLOT_DEFINED_INSTANCES = 4093;
        const uint SLOT_OK_FAIL = 4094;
        const uint SLOT_STATE = 4095;
        const uint SLOT_COMMS = 4096;
        const string DOT_NET_DEV_VERSION = "4.0.30319.17929";

        const string PSLOT_NORMAL = "0";
        const string PSLOT_PRIMARY = "1";
        const string PSLOT_SECONDARY = "2";
        const string PSLOT_COMMS = "3";
        const string PSLOT_SERVER_COMMS = "4";
        const string PSLOT_DRIVER_CLOSE = "5";
        #endregion

        #region Private members
        private BNCSInfoDriver _infoDriver = null;
        private Hysteresis _hysteresis = null;
        private int _hysteresisWindow = 1000;
        private BNCSInternalInfoDriver _internalID = new BNCSInternalInfoDriver();
        #if (CC_G1)
        private CommsClient _client = new CommsClient();
        #else
        private CommsClient2 _client = new CommsClient2();
        #endif
        private FiestaProtocol _protocol = new FiestaProtocol();
        private FiestaXMLPreParser _xmlPreParser = new FiestaXMLPreParser();
        private int _deviceNumber = 0;
        private int _workstationNumber = 0;
        private string _fiestaUserName = string.Empty;
        private string _fiestaPassword = string.Empty;
        private string _iniFileName = string.Empty;
        private int _pollTimeSeconds = 30;
        private ObservableCollection<DeviceUnit> _units = new ObservableCollection<DeviceUnit>();
        private List<BNCSDeviceInstance> _instances = new List<BNCSDeviceInstance>();
        private FiestaDeviceTypes _types = null;
        private bool _saveIni = false;
        private DeviceUnit _selectedUnit = null;
        private DeviceParameter _selectedParameter = null;
        private CommandStack _stack = new CommandStack();
        private CommandStackItemList _alarmsCheckList = new CommandStackItemList();
        private bool _pollingInitialised = false;
        private BNCSIdSlotMap _slotMap = new BNCSIdSlotMap(1, SLOT_MAX_AVAILABLE);
        private BNCSLogFile _logger = null;
        private EventItemsLogViewModel _model = new EventItemsLogViewModel();
        private bool _externalDebugging = false;
        private DispatcherTimer _pollTimer = new DispatcherTimer();
        private DispatcherTimer _logTimer = new DispatcherTimer();
        private DispatcherTimer _commsStatusTimer = null;
        // This (along with the turn log on/off option on the menu) is just until we solve the high CPU load issue. This timer turns logging to the event viewer window off after 10 minutes.
        private DispatcherTimer _log10MinTimerShutdown = null;
        private DispatcherTimer _idReconTimer = null;
        private DispatcherTimer _summaryStatusReplyTimer = null;
        private bool _summaryStatusReplyReceived = false;
        private bool _canLogToViewer = true;
        private int _unitPollIndex = -1;
        private int _unitParameterIndex = -1;
        private Queue<SelectedUnitForExport> _exports = new Queue<SelectedUnitForExport>();
        private BNCSInstances _bncsInstances = null;
        private ConfigurationEditor _configwindow = null;
        private DataQueue _dataQueue = null;
        private bool[] _eventsToLog = new bool[] { true, true, true, true, true, true, true, true, true, true, true };
        private DriverProgress _progress = DriverProgress.ShuttingDown;
        private TXCommander _txCommander = null;
        private int _maximumInitCommandRetries = 5;
        private int _logClearMinutes = 60;
        private int _unitFilter = 0;
        private string _bncsLibDLL = string.Empty;
        private string _bncsControlLibDLL = string.Empty;
        private List<PseudoSlot> _pslots = new List<PseudoSlot>();
        private string _dotNetVersion = Environment.Version.ToString();
        private int _lastRXTick = 0;
        // Time out in seconds in which an RX is always expected, typically only used when regular <getUnitAlarms> commands are being dispatched
        private int _rxSilenceThreshold = 30;
        // Time out in seconds in which a reply to a <getSummaryStatus> command is expected
        private int _watchdogTimeoutSeconds = 10;
        // Interval for the comms timer which will either fire the next <getUntiAlarms> command or the <getSummaryStatus> command as applicable
        private int _fiestaCommsSeconds = 60;
        private ulong _rxByteCount = 0;
        private int _unitIndex = -1;
        private DateTime? _rxAnalysisStart = null;
        private TimeSpan? _rxAnalysisSpan = null;
        private PerformanceAnalysisAndControlWindow _performanceWindow = null;
        private bool _showCpuWindowOnStartup = false;
        private bool _useCpuWindow = false;
        private bool _canAutoTurnOffLogging = true;
        private bool _showLogOnExit = false;
        #endregion

        #region Constructor / start-up
        /// <summary>
        /// Constructor, sets the list view sources
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            _txCommander = new TXCommander(TXTransmit);
            _txCommander.CommandWindowSeconds = 20;
            _dataQueue = new DataQueue(OnDataQueueUpdate);
            
            lvUnits.ItemsSource = _units;
            dgUnits.ItemsSource = _units;
            DataContext = Model;
        }

        /// <summary>
        /// Loaded event handler, called when the UI is first loaded
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void AppLoaded(object sender, RoutedEventArgs e)
        {
            //WPFMessageHandler handler = new WPFMessageHandler(this);
            //handler.OnMessage += WndProc;

            if (1 == CommandLine.ParameterCount)
            {
                if (!Int32.TryParse(CommandLine.Parameters[0], out _deviceNumber))
                    _deviceNumber = 0;
            }

            if (0 == _deviceNumber)
            {
                MessageBox.Show("A valid device number must be supplied as the only command line parameter.", "Error - Missing Device Number");
                ForceClose();
            }
            else
            {
                if (!SingleApplicationInstance.IsOnlyInstance("{bdf56b6d-f844-4e49-99e5-afe0b1231d91}" + _deviceNumber.ToString()))
                {
                    MessageBox.Show("The S3 Fiesta Driver is currently running connected to InfoDriver #" + _deviceNumber.ToString() + ", please close this instance before launching a new one or use a different InfoDriver.", "Driver Already Running with ID" + _deviceNumber.ToString());
                    ForceClose();
                }
                else
                {
                    Title = "BNCS S3 Fiesta Driver - Connecting to InfoDriver #" + _deviceNumber.ToString() + "...";
                    tbDeviceID.Text = "Device ID: " + _deviceNumber.ToString();
                    UpdateEventLog("Starting Fiesta S3 Driver v" + WPFApplication.ProductVersion, EventCatergories.Startup);
                    UpdateEventLog("OS Version: " + Environment.OSVersion.ToString(), EventCatergories.Startup);
                    UpdateEventLog(".NET Version: " + _dotNetVersion, EventCatergories.Startup);
                    if (_dotNetVersion.CompareTo(DOT_NET_DEV_VERSION) < 0)
                        UpdateEventLog("Recommended .NET Version is " + DOT_NET_DEV_VERSION + " or higher", EventCatergories.Error);

                    // Create the InfoDriver and connect
                    UpdateEventLog("Connecting to ID #" + _deviceNumber.ToString() + "...", EventCatergories.Startup);
                    _infoDriver = pBNCSFactory.IDFactory("S3 Fiesta Driver", (uint)_deviceNumber);
                    _infoDriver.SlotMessageReceived += IDSlotMessageReceived;
                    _infoDriver.HostConnectStatusChanged += IDHostConnectStatusChanged;
                    _infoDriver.HostConnected += InfoDriverHostConnected;

                    ConnectToInfoDriver();

                    // Wait up to 5 seconds to connect to the InfoDriver
                    UpdateEventLog("Waiting for ID #" + _deviceNumber.ToString() + " to connect...", EventCatergories.Startup);
                    if (!WaitForInfoDriver(5))
                    {
                        MessageBox.Show("Not connected to InfoDriver #" + _deviceNumber.ToString() + ".\n\nThe Fiesta Driver will now close, please ensure the aforementioned InfoDriver is running and then restart this driver.", "BNCS System Error");
                        ForceClose();
                    }
                    else
                    {
                        _idReconTimer = new DispatcherTimer();
                        _idReconTimer.Interval = new TimeSpan(0, 0, 10);
                        _idReconTimer.Tick += IDReconTimerTick;
                        UpdateEventLog("Connected to ID #" + _deviceNumber.ToString(), EventCatergories.Startup);
                        UpdateEventLog("Calling LoadConfiguration() method...", EventCatergories.Startup);
                        try
                        {
                            LoadConfiguration();
                        }
                        catch (Exception exception)
                        {
                            string error = string.Empty;
                            while (null != exception)
                            {
                                error += exception.Message;
                                exception = exception.InnerException;
                            }
                            UpdateEventLog(error, EventCatergories.Bncs);
                        }

                        Title = "BNCS S3 Fiesta Driver - Device #" + _deviceNumber.ToString();

                        _pollTimer.Tick += PollTimerTick;
                        _logTimer.Tick += LogTimerTick;
                        _logTimer.Interval = new TimeSpan(_logClearMinutes / 60, _logClearMinutes % 60, 0);
                        _logTimer.Start();

                        if (_canAutoTurnOffLogging)
                        {
                            _log10MinTimerShutdown = new DispatcherTimer();
                            _log10MinTimerShutdown.Interval = new TimeSpan(0, 10, 0);
                            _log10MinTimerShutdown.Tick += VisualLog10MinTimerShutdownTick;
                            _log10MinTimerShutdown.Start();
                        }
                    }
                }
            }
        }

        /*
        private static IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            string message = string.Empty;

            if (msg == NativeMethods.BBC_CSICLOSEWARNING)
                message = "BBC_CSICLOSEWARNING";
            else if (msg == NativeMethods.BBC_CSICLOSING)
                message = "BBC_CSICLOSING";
            else if (msg == NativeMethods.BBC_CSIPOSTEDHANGUP)
                message = "BBC_CSIPOSTEDHANGUP";
            else if (msg == NativeMethods.BBC_CSIPOSTEDPOLL)
                message = "BBC_CSIPOSTEDPOLL";
            else if (msg == NativeMethods.BBC_IDCONNECT)
                message = "BBC_IDCONNECT";
            else if (msg == NativeMethods.BBC_IDCONNECT32)
                message = "BBC_IDCONNECT32";
            else if (msg == NativeMethods.BBC_IDDISCONNECT)
                message = "BBC_IDDISCONNECT";
            else if (msg == NativeMethods.BBC_IDGETSLOT)
                message = "BBC_IDGETSLOT";
            else if (msg == NativeMethods.BBC_IDGETTXRXMODE)
                message = "BBC_IDGETTXRXMODE";
            else if (msg == NativeMethods.BBC_IDHANDLE)
                message = "BBC_IDHANDLE";
            else if (msg == NativeMethods.BBC_IDREVINFO)
                message = "BBC_IDREVINFO";
            else if (msg == NativeMethods.BBC_IDSENDSLOTS)
                message = "BBC_IDSENDSLOTS";
            else if (msg == NativeMethods.BBC_IDSETSLOT)
                message = "BBC_IDSETSLOT";
            else if (msg == NativeMethods.BBC_IDSETSLOTONLY)
                message = "BBC_IDSETSLOTONLY";
            else if (msg == NativeMethods.BBC_IDSETTXRXMODE)
                message = "BBC_IDSETTXRXMODE";
            else if (msg == NativeMethods.BBC_IDSLOTMSG)
                message = "BBC_IDSLOTMSG";
            else if (msg == NativeMethods.BBC_IDUSERSTR)
                message = "BBC_IDUSERSTR";
            else if (msg == NativeMethods.BBC_GETMAXDATABASES)
                message = "BBC_GETMAXDATABASES";

            if (message.Length > 0)
            {
                TextWriter writer = new StreamWriter(@"c:\xbug.log", true);
                writer.WriteLine(message);
                writer.Close();
            }

            return IntPtr.Zero;
        }
        */
        #endregion

        #region CommsClient event handlers
        private ExceptionResponse OnException(object sender, OnExceptionEventArgs e)
        {
            string message = string.Empty;
            while (null != e.Exception)
            {
                message += e.Exception.Message;
                e.Exception = e.Exception.InnerException;
            }
            UpdateEventLog("COMMS EXCEPTION: " + message, EventCatergories.Comms);
            return message.Contains("An existing connection was forcibly closed by the remote host") ? ExceptionResponse.HostDisconnect : ExceptionResponse.Handled;
        }

        private void OnDebug(object sender, OnDebugEventArgs e)
        {
            UpdateEventLog("DEBUG: " + e.Message, EventCatergories.Comms);
        }

        private void OnConnectionStateChanged2(object sender, OnConnectionStatusChangedEventArgs2 e)
        {
            DoConnectionStateChange(e.Status, e.Instigator);
        }

        /// <summary>
        /// Event handler that fires when the Comms Clients connection status changes
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void OnConnectionStateChanged(object sender, OnConnectionStatusChangedEventArgs e)
        {
            ConnectionStatus2 status = ConnectionStatus2.Connected;
            switch (e.Status)
            {
                case ConnectionStatus.Disconnected:
                    status = ConnectionStatus2.Disconnected;
                    break;
                case ConnectionStatus.Reconnecting:
                    status = ConnectionStatus2.Reconnecting;
                    break;
            }
            DoConnectionStateChange(status, e.Instigator);
        }

        private ConnectionStatus ConnectionStatus2ToConnectionStatus(ConnectionStatus2 status_)
        {
            switch (status_)
            {
                case ConnectionStatus2.Connecting:
                case ConnectionStatus2.Disconnected:
                    return ConnectionStatus.Disconnected;
                case ConnectionStatus2.Reconnecting:
                    return ConnectionStatus.Reconnecting;
            }
            return ConnectionStatus.Connected;
        }

        private void CommsStatusTimerControl(bool activate_) // Bug #2825
        {
            if (null != _commsStatusTimer)
            {
                if (activate_)
                {
                    _commsStatusTimer.Start();
                    UpdateBNCSLog("Starting comms status timer for sending summary status or get unit alarms commands");
                }
                else
                {
                    _commsStatusTimer.Stop();
                    UpdateBNCSLog("Stopping comms status timer for sending summary status or get unit alarms commands");
                }
            }
        }

        private void DoConnectionStateChange(ConnectionStatus2 status_, ConnectChangeInstigator instigator_)
        {
            UpdateEventLog("Connection State Change to: " + status_.ToString() + ". Instigator: " + instigator_.ToString(), EventCatergories.Comms);

            string message = string.Empty;
            switch (status_)
            {
                case ConnectionStatus2.Connected:
                    if (_pslots.Count > 0)
                    {
                        foreach (PseudoSlot slot in _pslots)
                            WriteToInfodriverSlot(slot.LiteralSlotNumber, PSLOT_NORMAL);
                    }

                    if (_useCpuWindow && null == _performanceWindow && _showCpuWindowOnStartup)
                        Dispatcher.BeginInvoke(new Action(() => PerformanceClick(null, null)));
                    Progress = DriverProgress.LoggingIn;
                    message = "Connected to ";
                    _client.Send(_protocol.GetUnregisterAllNotifyCommand());
                    Transmit(_protocol.LogUserInCommand(_fiestaUserName, _fiestaPassword));
                    break;
                case ConnectionStatus2.Disconnected:
                    _txCommander.CommandWindowSeconds = 20;
                    if (null != _commsStatusTimer)
                        CommsStatusTimerControl(false);
                    ClearAnalysis(true);
                    if (_progress < DriverProgress.ShuttingDown)
                    {
                        foreach (PseudoSlot slot in _pslots)
                            WriteToInfodriverSlot(slot.LiteralSlotNumber, PSLOT_SERVER_COMMS);
                    }
                    _txCommander.MaximumRetries = -1;
                    _stack.Clear();
                    _txCommander.Complete();
                    _slotMap.Clear();
                    _unitIndex = -1;
                    Progress = DriverProgress.LoggingIn;
                    _pollTimer.Stop();
                    message = "Disconnected from ";
                    break;
                case ConnectionStatus2.Reconnecting:
                    message = "Attempting reconnection to ";
                    break;
                case ConnectionStatus2.Connecting:
                    message = "Connecting to ";
                    break;
                case ConnectionStatus2.Disconnecting:
                    message = "Disconnecting from ";
                    break;
            }
            UpdateEventLog(message + "the Fiesta Server at " + _client.Settings.ToString(), EventCatergories.Comms);
            _dataQueue.Add(status_);
            _dataQueue.Add(message + "the Fiesta Server at " + _client.Settings.ToString());
        }

        private void RXThroughputAnalysis(int bufferLength_)
        {
            if (_progress < DriverProgress.WaitingForValueChanges)
            {
                if (null == _rxAnalysisStart)
                {
                    _rxAnalysisStart = DateTime.Now;
                    _rxAnalysisSpan = null;
                }
                _rxByteCount += (ulong)bufferLength_;
            }
            else if (null == _rxAnalysisSpan)
            {
                _rxAnalysisSpan = DateTime.Now.Subtract((DateTime)_rxAnalysisStart);
                ulong bytesPerSecond = _rxByteCount / (ulong)((TimeSpan)_rxAnalysisSpan).TotalSeconds;
                _logger.Write("SOD ANALYSIS (bytes): " + _rxByteCount.ToString());
                _logger.Write("SOD ANALYSIS (period): " + ((TimeSpan)_rxAnalysisSpan).ToString());
                _logger.Write("SOD ANALYSIS (b/s): " + bytesPerSecond.ToString());
                ClearAnalysis(false);
            }
        }

        private void ClearAnalysis(bool clearAll_)
        {
            if (clearAll_)
                _rxAnalysisSpan = null;
            _rxAnalysisStart = null;
            _rxByteCount = 0;
        }

        /// <summary>
        /// Event handler that fires when the client completes a read from the socket (data in from the Fiesta server)
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void ClientReceive(object sender, OnReceiveEventArgs e)
        {
            RXThroughputAnalysis(e.Buffer.Length);

            _lastRXTick = Environment.TickCount;
            List<byte[]> messages = _xmlPreParser.ParseData(e.Buffer);
            if (null != messages)
            {
                foreach (byte[] message in messages)
                    _protocol.AppendData(message);
            }

            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new DispatcherOperationCallback(delegate { UpdateBNCSLog("RX: " + new Databuffer(e.Buffer).ToString()); ; return null; }), null);
            UpdateEventLog(e.Buffer, EventCatergories.Receive);
        }

        private void OnBufferFull(object sender, OnBufferFullEventArgs e, ref bool DontWarnAgain)
        {
            string message = "WARNING: Read buffer was full on last read, " + e.Buffersize.ToString() + "b";
            UpdateEventLog(message, EventCatergories.Comms);
            DontWarnAgain = true;
        }
        #endregion

        #region Protocol event handlers
        /// <summary>
        /// Event handler that fires when the FiestaProtocol object has finished parsing received data from the socket
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void OnProtocolParse(object sender, ref Fiesta.OnParseResultEventArgs e)
        {
            if (e.Info == ParseInfo.ValidDatagram)
            {
                try
                {
                    ProcessDataResult result = _protocol.ProcessData(e.Data);

                    if (result > ProcessDataResult.Failed)
                        Dispatcher.Invoke(new ClientReceiveCallbackDelegate(ReceiveCallback), result, e.Data);
                }
                catch (Exception ex)
                {
                    string x = ex.Message;
                }
            }
        }

        private string GetCurrentValue(int unit_, int parameter_)
        {
            foreach (DeviceUnit unit in _units)
            {
                if (unit.Number == unit_)
                {
                    foreach (DeviceParameter parameter in unit.Parameters)
                    {
                        if (parameter.Number == parameter_)
                            return parameter.Value;
                    }
                }
            }
            return null;
        }

        private void OnProtocolGetParameter(object sender, OnGetParameterValueEventArgs e)
        {
            UpdateEventLog("Unit #" + e.Unit.ToString() + " / Param #" + e.Parameter.ToString() + " / Value==\"" + e.Value + "\"", EventCatergories.Value);
            UpdateParameter(e);
        }

        private void OnProtocolSetNotifyResponse(object sender, OnSetNotifyOnValueChangedResponseEventArgs e)
        {
            string message = "Registered Unit #" + e.Unit.ToString() + " for changes: Parameter #" + e.Parameter.ToString() + " Reason: " + e.TriggerReason + " Max value: " + e.MaximumValue.ToString() + " Min Value: " + e.MinimumValue.ToString();
            UpdateEventLog(message, EventCatergories.Registration);
            // Check value and if a change has occured then update UI and ID
            Dispatcher.BeginInvoke(new Action(() => UpdateSubscribedParameter(e.Unit, e.Parameter, e.Value)));
        }

        private void OnProtocolError(object sender, string e)
        {
            UpdateEventLog(e, EventCatergories.Error);
        }

        private void OnProtocolXmlError(object sender, OnXmlErrorEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new DispatcherOperationCallback(delegate { UpdateEventLog(e.ToString(), EventCatergories.Error); ; return null; }), null);
        }
        #endregion

        #region InfoDriver events and methods
        /// <summary>
        /// InfoDriver host connected event handler
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void InfoDriverHostConnected(object sender, BncsConnectedEventArgs e)
        {
            if (null == _hysteresis)
            {
                _hysteresis = new Hysteresis((uint)_deviceNumber, (uint)_hysteresisWindow);
                _hysteresis.OnRevert += HysteresisOnRevert;
            }
        }

        /// <summary>
        /// Info driver host connected status change event handler
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void IDHostConnectStatusChanged(object sender, BncsConnectStatusChangedEventArgs e)
        {
            bool reconnect = false;
            string message = e.ConnectStatus.ToString() + " ";
            switch (e.ConnectStatus)
            {
                case HostDriverConnectState.Connecting:
                    message += "to ";
                    break;
                case HostDriverConnectState.Connected:
                    _infoDriver.OkToGoRxOnly = false;
                    _infoDriver.OkToGoTXRX = true;
                    try
                    {
                        _infoDriver.ChangeIfMode(HostDriverIFMode.TxRx);
                    }
                    catch { }

                    if (null != _idReconTimer && _idReconTimer.IsEnabled)
                    {
                        _idReconTimer.Stop();
                        Dispatcher.BeginInvoke(new Action(() => UpdateBNCSLog("Stopping InfoDriver reconnection timer.")));
                        if (_internalID.HasData)
                        {
                            Dispatcher.BeginInvoke(new Action(() => UpdateBNCSLog("Updating InfoDriver data.")));
                            foreach (KeyValuePair<uint, string> pair in _internalID.GetSlots())
                                WriteToInfodriverSlot(pair.Key, pair.Value);
                        }
                    }
                    message += "to ";
                    break;
                case HostDriverConnectState.Disconnected:
                case HostDriverConnectState.Disconnecting:
                    message += " from ";
                    reconnect = _progress < DriverProgress.ShuttingDown;
                    break;
            }
            message += " Infodriver #" + _infoDriver.DevNum.ToString() + ", \"" + _infoDriver.Name + "\"";
            UpdateEventLog(message, EventCatergories.Comms);
            _dataQueue.Add("Infodriver " + _deviceNumber.ToString() + " " + e.ConnectStatus.ToString());

            if (reconnect && !_idReconTimer.IsEnabled)
            {
                Dispatcher.BeginInvoke(new Action(() => UpdateBNCSLog("Starting InfoDriver reconnection timer.")));
                _idReconTimer.Start();
            }
        }

        /// <summary>
        /// Info driver slot message received event handler, fires when data is written to a slot
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void IDSlotMessageReceived(object sender, BncsSlotMessageEventArgs e)
        {
            SetFiestaValue((int)e.Slot, e.Message);
            if (SLOT_OK_FAIL == e.Slot)
                WriteToInfodriverSlot(SLOT_OK_FAIL, "OK");
        }

        private IDWriteResult WriteToInfodriverSlot(uint slot_, string value_)
        {
            if (_hysteresis.RevertDataRequest(slot_, value_))
                return ExecuteWriteToInfodriverSlot(slot_, value_);
            return IDWriteResult.Deffering;
        }

        /// <summary>
        /// Wrapper for writing data to an info driver slot
        /// </summary>
        /// <param name="slot_">Target slot</param>
        /// <param name="value_">Value to write</param>
        /// <returns>Success, Failure or NotConnected</returns>
        private IDWriteResult ExecuteWriteToInfodriverSlot(uint slot_, string value_)
        {
            if (null == _infoDriver || _infoDriver.HostConnectState != HostDriverConnectState.Connected)
            {
                _internalID.WriteToSlot(slot_, value_);
                return IDWriteResult.NotConnected;
            }

            try
            {
                if (value_.Length > 255)
                {
                    string overspill = "ID revertive message truncated from " + value_.Length.ToString() + " characters to 255: " + value_;
                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, new DispatcherOperationCallback(delegate { UpdateBNCSLog(overspill); ; return null; }), null);
                    value_ = value_.Substring(0, 255);
                }
                return _infoDriver.SendRevertive(slot_, value_, true) ? IDWriteResult.Success : IDWriteResult.Failure;
            }
            catch (BncsNotConnectedException)
            {
                return IDWriteResult.NotConnected;
            }
        }

        /// <summary>
        /// Iterates all units and their parameters and writes all their values to their slots
        /// </summary>
        private void WriteToInfoDriverSlots()
        {
            foreach (DeviceUnit unit in _units)
            {
                foreach (DeviceParameter parameter in unit.Parameters)
                {
                    int slot = _slotMap.GetSlot(unit.Number, parameter.Number);
                    if (slot > 0)
                        ExecuteWriteToInfodriverSlot((uint)slot, parameter.Value);
                }
            }
        }

        /// <summary>
        /// Sets a parameter value on the Fiesta
        /// </summary>
        /// <param name="slot_">Slot whose data updated</param>
        /// <param name="value_">Updated value in the slot</param>
        private void SetFiestaValue(int slot_, string value_)
        {
            // Convert the slot into a parameter address by slot number
            UnitParameterAddress address = _slotMap.SlotToDeviceParameter(slot_);
            if (null == address) // Nothing to do with us so just log it
                UpdateEventLog("Slot #" + slot_.ToString() + " change: \"" + value_ + "\"", EventCatergories.Bncs);
            else // Send the set parameter command to the unit for the parameter with the new value
            {
                if (address.IsUserSlot) // user slot
                {
                    WriteToInfodriverSlot((uint)slot_, value_);
                    UpdateEventLog("User Slot #" + slot_.ToString() + " updated:\"" + value_ + "\"", EventCatergories.Bncs);
                }
                else
                {
                    Transmit(_protocol.SetParameterValueCommand(address.UnitNumber, address.ParameterNumber, value_));
                    _protocol.SetNextSetParameter(new UnitParameter(address.UnitNumber, address.ParameterNumber));
                }
            }
        }

        /// <summary>
        /// Waits for the supplied amount of seconds for the InfoDriver connection state to change to Connected.
        /// </summary>
        /// <param name="seconds_"></param>
        /// <returns></returns>
        private bool WaitForInfoDriver(int seconds_)
        {
            if (seconds_ < 1)
                seconds_ = 5;

            int timeout = Environment.TickCount + (seconds_ * 1000);

            while (Environment.TickCount < timeout)
            {
                if (HostDriverConnectState.Connected == _infoDriver.HostConnectState)
                    break;
            }
            return HostDriverConnectState.Connected == _infoDriver.HostConnectState;
        }

        private void ConnectToInfoDriver()
        {
            try
            {
                _infoDriver.Connect(true);
            }
            catch (Exception exception)
            {
                string error = string.Empty;
                while (null != exception)
                {
                    error += exception.Message;
                    exception = exception.InnerException;
                }
                UpdateEventLog(error, EventCatergories.Bncs);
            }
        }
        #endregion

        #region Private event handlers
        private void HysteresisOnRevert(object sender, HysteresisRevertEventArgs e)
        {
            if (_progress != DriverProgress.ShuttingDown)
            {
                foreach (KeyValuePair<uint, string> revertive in e.Revertives)
                    Dispatcher.BeginInvoke(new Action(() => ExecuteWriteToInfodriverSlot(revertive.Key, revertive.Value)));
            }
        }

        private void TXTransmit(object sender, TransmitEventArgs e)
        {
            if (_client.IsConnected)
            {
                if (e.ExecuteRetry)
                {
                    if (e.RetryCount > 0)
                    {
                        UpdateEventLog("Retry attempt #" + e.RetryCount.ToString() + " for command \"" + new Databuffer(e.Command).ToString() + "\"", EventCatergories.Comms);
                        UpdateEventLog(e.Command, EventCatergories.Transmit);
                    }
                    _client.Send(e.Command);
                }
                else
                {
                    if (_progress == DriverProgress.ShuttingDown)
                        ForceClose();
                    else
                    {
                        UpdateEventLog("Abandoning command \"" + new Databuffer(e.Command).ToString() + "\" after " + e.RetryCount.ToString() + " retry attempts", EventCatergories.Comms);
                        Point unitParameter = GetUnitParameterFromXml(e.Command);
                        if (unitParameter.X > 0 && unitParameter.Y > 0)
                        {
                            int slot = _slotMap.GetSlot((int)unitParameter.X, (int)unitParameter.Y);
                            if (slot > 0)
                                WriteToInfodriverSlot((uint)slot, string.Empty);
                        }
                        if (_stack.IsEmpty)
                        {
                            if (_progress < DriverProgress.WaitingForValueChanges)
                                PopulateStack();
                        }
                        else
                            Transmit(_stack.GetNextCommand());
                    }
                }
            }
        }

        private void OnDataQueueUpdate(object sender, List<object> logItems)
        {
            foreach (object item in logItems)
            {
                if (item is EventItem)
                    _model.Items.Add((EventItem)item);
                else if (item is string)
                    UpdateStatus((string)item);
                #if (CC_G1)
                else
                {
                    if (item is ConnectionStatus)
                        UpdateLED((ConnectionStatus)item);
                    else if (item is ConnectionStatus2)
                        UpdateLED(ConnectionStatus2ToConnectionStatus((ConnectionStatus2)item));
                }
                #else
                else if (item is ConnectionStatus2)
                    UpdateLED((ConnectionStatus2)item);
                #endif
            }
            if (true == miScroll.IsChecked)
                lbEventLog.ScrollIntoView(lbEventLog.SelectedItem);
        }
        
        private void ParametersDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (null != _selectedUnit && null != _selectedParameter & _progress < DriverProgress.ShuttingDown)
                Transmit(_protocol.GetParameterValueCommand(_selectedUnit.Number, _selectedParameter.Number));
        }
        
        private void PollTimerTick(object sender, EventArgs e)
        {
            ++_unitParameterIndex;
            if (_unitParameterIndex == _units[_unitPollIndex].Parameters.Count)
            {
                ++_unitPollIndex;
                if (_unitPollIndex == _units.Count)
                    _unitPollIndex = 0;
                _unitParameterIndex = 0;
            }
            UpdateEventLog("Polling unit " + _units[_unitPollIndex].ToString() + " Parameter #" + _units[_unitPollIndex].Parameters[_unitParameterIndex].Number.ToString(), EventCatergories.Operations);
            Transmit(_protocol.GetParameterValueCommand(_units[_unitPollIndex].Number, _units[_unitPollIndex].Parameters[_unitParameterIndex].Number));
        }

        private void LogTimerTick(object sender, EventArgs e)
        {
            ClearDebugClick(null, null);
            UpdateEventLog("Visual log cleared", EventCatergories.Operations);
        }


        private void IDReconTimerTick(object sender, EventArgs e)
        {
            ConnectToInfoDriver();
        }

        private void VisualLog10MinTimerShutdownTick(object sender, EventArgs e)
        {
            _log10MinTimerShutdown.Stop();
            UpdateEventLog("Logging auto disabled", EventCatergories.Operations);
            miLogToViewer.IsChecked = false;
            _canLogToViewer = false;
        }

        private void CommsStatusTimerTick(object sender, EventArgs e)
        {
            if (_client.IsConnected)
            {
                // pseudo call or timeout here
                if (_rxSilenceThreshold > 0 && Environment.TickCount > _lastRXTick + (_rxSilenceThreshold * 1000))
                {
                    // send summary status here - pfd
                    string message = "Sending Summary Status command as no RX received within the last " + _rxSilenceThreshold.ToString() + " seconds";
                    UpdateBNCSLog(message);
                    UpdateEventLog(message, EventCatergories.Comms);
                    // pfd 31/10/2016 - this can fail if awaiting previous command, so instead always fire it.....
                    //Transmit(_protocol.GetSummaryStatusCommand()); 
                    // pfd 31/10/2016 - fire the GetSummaryStatus
                    _client.Send(_protocol.GetSummaryStatusCommand());
                    if (null == _summaryStatusReplyTimer)
                    {
                        _summaryStatusReplyTimer = new DispatcherTimer();
                        _summaryStatusReplyTimer.Interval = new TimeSpan(0, 0, _watchdogTimeoutSeconds);
                        _summaryStatusReplyTimer.Tick += SummaryStatusReplyTimerTick;
                    }
                    _summaryStatusReplyReceived = false;
                    _summaryStatusReplyTimer.Start();
                }
                else
                {
                    foreach (PseudoSlot slot in _pslots)
                        _alarmsCheckList.Add(_stack.Add(_protocol.GetUnitAlarms((int)slot.DeviceNumber), (int)slot.DeviceNumber));
                    Transmit(_stack.GetNextCommand());
                }
            }
        }

        private void SummaryStatusReplyTimerTick(object sender, EventArgs e)
        {
            _summaryStatusReplyTimer.Stop();
            if (!_summaryStatusReplyReceived)
            {
                _client.AssumeServerDisconnect();
                string message = "Disconnecting from Server as no Summary Status reply received within the last " + _watchdogTimeoutSeconds.ToString() + " seconds, suspicion of S3 Server Comms issue.";
                UpdateBNCSLog(message);
                UpdateEventLog(message, EventCatergories.Comms);
            }
            else
                _summaryStatusReplyReceived = false;
        }

        /// <summary>
        /// Selection changed event handler for the ListView containing the units
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void UnitsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            object unit = lvUnits.SelectedItem;
            if (null != unit)
            {
                if (unit.GetType().ToString() == "BNCS.DeviceUnit")
                {
                    _selectedUnit = (DeviceUnit)unit;
                    if (null != _selectedUnit)
                        lvParams.ItemsSource = _selectedUnit.Parameters;
                }
            }
        }

        private void ParameterSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            object parameter = lvParams.SelectedItem;
            if (null != parameter)
            {
                if (parameter.GetType().ToString().Equals("BNCS.DeviceParameter"))
                    _selectedParameter = (DeviceParameter)parameter;
            }
        }
        #endregion

        #region Private Menu Command event handlers
        /// <summary>
        /// File|Edit INI File command
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void EditIniClick(object sender, RoutedEventArgs e)
        {
            if (!Execute.ShellExecute(_iniFileName))
                MessageBox.Show("Can't find the INI file at \"" + _iniFileName + "\".", "Error");
        }
      
        /// <summary>
        /// File|Configuration Editor command, shows the config editor
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void ConfigureClick(object sender, RoutedEventArgs e)
        {
            if (null == _configwindow)
            {
                _configwindow = new ConfigurationEditor(_protocol, _deviceNumber);
                _configwindow.OnExport += OnExport;
            }
            if (!_configwindow.IsVisible)
                _configwindow.Show();
            _configwindow.WindowState = WindowState.Normal;
        }

        private string FileNameToTypeName(string filename_)
        {
            string result = new FileInfoEx(filename_).Information.Name;
            int index = result.LastIndexOf('.');
            if (index > -1)
                result = result.Substring(0, index);
            return result;
        }

        private void OnExport(object sender, OnExportEventArgs e)
        {
            
            if (null != e.Unit)
            {
                DeviceUnit unit = _protocol.GetDeviceByNumber(e.Unit.UnitNumber);
                if (null != unit && unit.Parameters.Count > 0 && !e.ForceFiestaRefresh)
                {
                    if (e.Unit.ForceTypeNameToMatchFileName)
                        unit.Name = FileNameToTypeName(e.Unit.FileTarget);
                    if (BNCSDeviceType.ExportDeviceType(e.Unit.FileTarget, unit.Name, unit.Parameters))
                    {
                        _configwindow.InformationUpdate("The device type \"" + unit.Name + "\" was successfully saved to \"" + e.Unit.FileTarget + "\".");
                        UpdateEventLog("The device type \"" + unit.Name + "\" was successfully saved to \"" + e.Unit.FileTarget + "\".", EventCatergories.Operations);
                    }
                    else
                    {
                        _configwindow.InformationUpdate("An error occured while trying to export the device type \"" + unit.Name + "\" to file \"" + e.Unit.FileTarget + "\".");
                        UpdateEventLog("An error occured while trying to export the device type \"" + unit.Name + "\" to file \"" + e.Unit.FileTarget + "\".", EventCatergories.Error);
                    }
                }
                else if (_client.IsConnected)
                {
                    _exports.Enqueue(e.Unit);
                    Transmit(_protocol.GetUnitCommand(e.Unit.UnitNumber));
                }
                else
                    _configwindow.InformationUpdate("Unable to retrieve the parameters for the selected unit, the driver is no longer connected to the Fiesta Server");
            }
        }

        private void LogClick(object sender, RoutedEventArgs e)
        {
            if (!Execute.ShellExecute(_logger.Filename))
                MessageBox.Show("Can't find the log file at \"" + _logger.Filename + "\".", "Error");
        }

        /// <summary>
        /// File|Exit command
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void ExitClick(object sender, RoutedEventArgs e)
        {
            UpdateBNCSLog("File|Exit clicked by user");
            Close();
        }

        private void ExitLogClick(object sender, RoutedEventArgs e)
        {
            _showLogOnExit = true;
            ExitClick(null, null);
        }

        /// <summary>
        /// Edit | Edit Value command
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void QuereyValueClick(object sender, RoutedEventArgs e)
        {
            if (null != _selectedUnit && null != _selectedParameter)
                Transmit(_protocol.GetParameterValueCommand(_selectedUnit.Number, _selectedParameter.Number));
        }

        private void CopyLogClick(object sender, RoutedEventArgs e)
        {
            StringBuilder clipData = new StringBuilder();
            foreach (EventItem item in lbEventLog.Items)
                clipData.AppendLine(item.Text);
            Clipboard.SetText(clipData.ToString());
        }

        private void CutLogClick(object sender, RoutedEventArgs e)
        {
            CopyLogClick(sender, e);
            ClearDebugClick(sender, e);
        }

        private void OpenEventLog(object sender, RoutedEventArgs e)
        {
            miCopy.IsEnabled = lbEventLog.Items.Count > 0;
            miCut.IsEnabled = miCopy.IsEnabled;
            miClear.IsEnabled = miCopy.IsEnabled;
        }
        
        /// <summary>
        /// Event Log|Clear Event Viewer command
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void ClearDebugClick(object sender, RoutedEventArgs e)
        {
            _model.Items.Clear();
        }

        /// <summary>
        /// Event Log|Auto Scroll command
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void AutoScrollClick(object sender, RoutedEventArgs e)
        {
            miScroll.IsChecked = !miScroll.IsChecked;
            _saveIni = true;
        }

        private void LogToViewerClick(object sender, RoutedEventArgs e)
        {
            bool logToViewer = !miLogToViewer.IsChecked;
            if (!logToViewer)
                UpdateEventLog("Logging manually disabled", EventCatergories.Operations);
            miLogToViewer.IsChecked = logToViewer;
            _canLogToViewer = logToViewer;
            if (logToViewer)
                UpdateEventLog("Logging manually enabled", EventCatergories.Operations);
        }

        private void FilterOffClick(object sender, RoutedEventArgs e)
        {
            _unitFilter = 0;
            CheckFilterMenuItem((MenuItem)sender);
        }

        private void FilterOnClick(object sender, RoutedEventArgs e)
        {
            _unitFilter = (int)((MenuItem)sender).Tag;
            CheckFilterMenuItem((MenuItem)sender);
        }
        /// <summary>
        /// Event Log catergory view commands
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void LogCheckClick(object sender, RoutedEventArgs e)
        {
            ((MenuItem)sender).IsChecked = !((MenuItem)sender).IsChecked;
            _saveIni = true;
            int reference;
            if (Int32.TryParse(((MenuItem)sender).Tag.ToString(), out reference))
                _eventsToLog[reference] = ((MenuItem)sender).IsChecked;
        }

        private void ShowUnitsClick(object sender, RoutedEventArgs e)
        {
            miShowUnits.IsChecked = !miShowUnits.IsChecked;
            ShowUnits(miShowUnits.IsChecked);
            _saveIni = true;
        }

        /// <summary>
        /// View|Refresh command
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void RefreshClick(object sender, RoutedEventArgs e)
        {
            _txCommander.MaximumRetries = -1;
            _stack.Clear();
            _txCommander.Complete();
            Progress = DriverProgress.GatheringUnitsList;
            _pollTimer.Stop();
            GetUnitList();
        }

        private void ViewMenuOpened(object sender, RoutedEventArgs e)
        {
            miRefresh.IsEnabled = miShowUnits.IsChecked;
            miQuereyParameter.IsEnabled = lvParams.SelectedIndex > -1;
            miSetParameter.IsEnabled = null != lvUnits.SelectedItem && null != lvParams.SelectedItem;
            if (_useCpuWindow)
                miPerformance.IsChecked = null != _performanceWindow && _performanceWindow.Visibility == System.Windows.Visibility.Visible;
        }

        private void OpenFileMenu(object sender, RoutedEventArgs e)
        {
            miLog.IsEnabled = FileFuncs.Exists(_logger.Filename);
        }

        private void OpenConfigurationMenu(object sender, RoutedEventArgs e)
        {
            miConfigurationEditor.IsEnabled = null != _protocol && _protocol.Units.Count > 0 && _client.IsConnected && _progress == DriverProgress.WaitingForValueChanges;
        }

        private void SetValueClick(object sender, RoutedEventArgs e)
        {
            DeviceUnit unit = (DeviceUnit)lvUnits.SelectedItem;
            DeviceParameter parameter =  (DeviceParameter)lvParams.SelectedItem;
            ParameterEditor editor = new ParameterEditor(unit, parameter);
            editor.ShowDialog();
            if (null != editor.Value)
                Transmit(_protocol.SetParameterValueCommand(unit.Number, parameter.Number, editor.Value));
        }

        private void PerformanceClick(object sender, RoutedEventArgs e)
        {
            miPerformance.IsChecked = !miPerformance.IsChecked;
            if (miPerformance.IsChecked)
            {
                if (null == _performanceWindow)
                {
                    _performanceWindow = new PerformanceAnalysisAndControlWindow(_client.Settings.CpuMilliseconds2, 500, "s3_fiesta");
                    _performanceWindow.OnDelayChange += PerformanceWindowOnDelayChange;
                    _performanceWindow.OnCPUPeak += PerformanceWindowOnCPUPeak;
                }
                _performanceWindow.Show();
            }
            else
                _performanceWindow.Hide();
        }

        private void PerformanceWindowOnDelayChange(object sender, OnCPUDelayChangeEventArgs e)
        {
            #if (CC_G1)
            _client.Settings.CpuMilliseconds = e.Delay;
            #else
            _client.Settings.CpuMilliseconds2 = e.Delay;

            UpdateBNCSLog("TCP Delay changed to " + e.Delay.ToString() + "ms");
            #endif
        }

        private void PerformanceWindowOnCPUPeak(object sender, OnCPUPeakEventArgs e)
        {
            _logger.Write("New CPU peak: " + e.Percentage.ToString() + "%");
        }

        private void CommsMenuOpened(object sender, RoutedEventArgs e)
        {
            miConDiscon.Header = _client.IsConnected ? "Disconnect From Server" : "Reconnect To Server";
        }

        private void ConnectDisconnectClick(object sender, RoutedEventArgs e)
        {
            if (_client.IsConnected)
                _client.Disconnect();
            else
            {
                _client.Connect();
                #if (CC_G1)
                miConDiscon.IsEnabled = _client.Status != ConnectionStatus.Reconnecting;
                #else
                miConDiscon.IsEnabled = _client.Status != ConnectionStatus2.Reconnecting;
                #endif
            }
        }

        private void PingClick(object sender, RoutedEventArgs e)
        {
            BncsPing ping = new BncsPing(_client.Settings.Host);
            ping.OnPing += OnPing;
            ping.AsynchPing();
        }

        private void OnPing(object sender, AsynchPingResultEventArgs e)
        {
            string message = "Cancelled: " + (e.Cancelled ? "Yes" : "No");
            message += "\nError: " + e.Error;
            message += BncsPing.PingReplyToString(e.Reply);
            MessageBox.Show(message, "Ping " + e.Reply.Status.ToString());
        }
        
        private void PollTimeClick(object sender, RoutedEventArgs e)
        {
            PollTimeEditor editor = new PollTimeEditor(_pollTimeSeconds);
            editor.ShowDialog();
            if (null != editor.Value)
            {
                _saveIni = true;
                _pollTimeSeconds = (int)editor.Value;
                if (0 == _pollTimeSeconds)
                    _pollTimer.Stop();
                else
                    CommencePolling(true);
                tbPolling.Text = 0 == _pollTimeSeconds ? "Polling: OFF" : "Polling: " + _pollTimeSeconds.ToString() + " seconds";
            }
        }

        private void AboutClick(object sender, RoutedEventArgs e)
        {
            string about = "BNCS S3 Fiesta Driver v" + WPFApplication.ProductVersion;
            FileInfoEx info = new FileInfoEx(WPFApplication.ExecutablePath);
            string directory = DirFuncs.IncludeTrailingBackslash(info.Information.DirectoryName);
            about += "\n[" + directory + "]";

            if (!System.IO.File.Exists(_bncsLibDLL))
                _bncsLibDLL = DirFuncs.IncludeTrailingBackslash(directory) + "BNCSLib.dll";

            if (System.IO.File.Exists(_bncsLibDLL))
            {
                about += "\n";
                info = new FileInfoEx(_bncsLibDLL);
                directory = DirFuncs.IncludeTrailingBackslash(info.Information.DirectoryName);
                about += "\nBNCS Library v" + FileVersionInfo.GetVersionInfo(_bncsLibDLL).ProductVersion + "\n[" + directory + "]";
            }

            if (!System.IO.File.Exists(_bncsControlLibDLL))
                _bncsControlLibDLL = DirFuncs.IncludeTrailingBackslash(directory) + "BncsControlLib.dll";

            if (System.IO.File.Exists(_bncsControlLibDLL))
            {
                about += "\n";
                info = new FileInfoEx(_bncsControlLibDLL);
                directory = DirFuncs.IncludeTrailingBackslash(info.Information.DirectoryName);
                about += "\nBNCS Visual Library v" + FileVersionInfo.GetVersionInfo(_bncsControlLibDLL).ProductVersion + "\n[" + directory + "]";
            }

            
            /*
            #if CC_G1
            about += "\n\nComms: Gen 1";
            #else
            about += "\n\nComms: Gen 2";
            #endif
            */
            MessageBox.Show(about + "\n\nBy The BNCS Team\n\n©" + DateTime.Now.Year.ToString() + " Atos\n\n", "About " + "BNCS S3 Fiesta Driver");
        }
        #endregion

        #region Private methods
        private void CloseWithMessage(string message_, string caption_)
        {
            MessageBox.Show(message_, caption_);
            Close();
        }

        private void CheckFilterMenuItem(MenuItem item_)
        {
            foreach (object item in miFilter.Items)
            {
                if (item is MenuItem)
                    ((MenuItem)item).IsChecked = ((MenuItem)item).Tag == item_.Tag;
            }
        }
        
        private void ClearUnits()
        {
            _units.Clear();
            //_parameters.Clear();
            _unitFilter = 0;
            for (int loop = miFilter.Items.Count - 1; loop > 0; --loop)
                miFilter.Items.RemoveAt(loop);
        }
        
        /// <summary>
        /// Saves changes to the INI file
        /// </summary>
        private void SaveIni()
        {
            INIFileWrangler inifile = new INIFileWrangler(_iniFileName, false);
            // Save S3_FIESTA section
            inifile.SaveSetting("S3_FIESTA", "Port", ((CommsTCPSettings)_client.Settings).Port.ToString(), false);
            inifile.SaveSetting("S3_FIESTA", "Server", _client.Settings.Host, false);
            inifile.SaveSetting("S3_FIESTA", "ReconnectSeconds", ((CommsTCPSettings)_client.Settings).AutoReconnectSeconds.ToString(), false);
            inifile.SaveSetting("S3_FIESTA", "Username", _fiestaUserName, false);
            inifile.SaveSetting("S3_FIESTA", "Password", _fiestaPassword, false);
            inifile.SaveSetting("S3_FIESTA", "PollSeconds", _pollTimeSeconds.ToString(), false);

            // Save EVENT LOGGING section
            inifile.SaveSetting("EVENT LOGGING", "scroll", true == miScroll.IsChecked ? "1" : "0", false);
            inifile.SaveSetting("EVENT LOGGING", "bncs", true == miLogBncs.IsChecked ? "1" : "0", false);
            inifile.SaveSetting("EVENT LOGGING", "comms", true == miLogComms.IsChecked ? "1" : "0", false);
            inifile.SaveSetting("EVENT LOGGING", "errors", true == miLogErrors.IsChecked ? "1" : "0", false);
            inifile.SaveSetting("EVENT LOGGING", "receive", true == miLogReceive.IsChecked ? "1" : "0", false);
            inifile.SaveSetting("EVENT LOGGING", "startup", true == miLogStartup.IsChecked ? "1" : "0", false);
            inifile.SaveSetting("EVENT LOGGING", "transmit", true == miLogTransmit.IsChecked ? "1" : "0", false);
            inifile.SaveSetting("EVENT LOGGING", "values", true == miLogValues.IsChecked ? "1" : "0", false);
            inifile.SaveSetting("EVENT LOGGING", "reg", true == miLogRegistration.IsChecked ? "1" : "0", false);
            inifile.SaveSetting("EVENT LOGGING", "cerrors", true == miLogConfigErrors.IsChecked ? "1" : "0", false);

            // Save VIEW section
            //inifile.SaveSetting("VIEW", "viewConf", true == miViewConfigured.IsChecked ? "1" : "0", false);
            inifile.SaveSetting("VIEW", "show", true == miShowUnits.IsChecked ? "1" : "0", false);

            // Save the DEBUG section
            inifile.SaveSetting("DEBUG", "external", _externalDebugging ? "1" : "0", false);

            inifile.Dispose();
        }

        /// <summary>
        /// Loads the configuration from the INI file, Instances xml and device types xml.
        /// </summary>
        private void LoadConfiguration()
        {
            BNCSFilePaths paths = null;
            try
            {
                UpdateEventLog("Loading configuration", EventCatergories.Startup);
                // Get the file paths we need
                try
                {
                    UpdateEventLog("Getting BNCS Paths", EventCatergories.Startup);
                    paths = BNCSUtilities.GetBNCSFilePaths();
                }
                catch (Exception exception)
                {
                    string error = string.Empty;
                    while (null != exception)
                    {
                        error += exception.Message;
                        exception = exception.InnerException;
                    }
                    UpdateEventLog(error, EventCatergories.Error);
                }

                try
                {
                    UpdateEventLog("Setting up BNCS Log File", EventCatergories.Startup);
                    // Set up the log file
                    _logger = new BNCSLogFile("S3Fiesta", _deviceNumber, paths.LogFilePath);
                }
                catch (Exception exception)
                {
                    string error = string.Empty;
                    while (null != exception)
                    {
                        error += exception.Message;
                        exception = exception.InnerException;
                    }
                    UpdateEventLog(error, EventCatergories.Error);
                }

                UpdateBNCSLog("Starting Fiesta S3 Driver v" + WPFApplication.ProductVersion);
                UpdateBNCSLog("OS Version: " + Environment.OSVersion.ToString());
                UpdateBNCSLog(".NET Version: " + _dotNetVersion);
                if (_dotNetVersion.CompareTo(DOT_NET_DEV_VERSION) < 0)
                    UpdateBNCSLog("Recommended .NET Version is " + DOT_NET_DEV_VERSION + " or higher");

                UpdateBNCSLog(".NET Version: " + Environment.Version.ToString());

                // Read instances xml
                UpdateEventLog("Reading BNCS Instances", EventCatergories.Startup);
                _bncsInstances = new BNCSInstances(paths.InstancesPath);

                // Get the workstation number
                _workstationNumber = BNCSUtilities.GetWorkstation();
                tbWorkstation.Text = "Workstation: " + _workstationNumber.ToString();
                // Get the INI file name
                _iniFileName = paths.IniFilePath.Length > 0 ? paths.IniFilePath + "\\" : string.Empty;
                miEditIni.Header = "Edit INI File (" + _iniFileName + ")";
                _iniFileName += string.Format("DEV_{0:000}.ini", _deviceNumber);
                UpdateEventLog("Reading settings from \"" + _iniFileName + "\"", EventCatergories.Startup);

                // Read the INI file
                INIFileWrangler inifile = new INIFileWrangler(_iniFileName, false);
                // Read S3_FIESTA section
                _hysteresisWindow = inifile.ReadInt("S3_FIESTA", "Hysteresis", 1000);
                if (_hysteresisWindow < 1)
                    _hysteresisWindow = 1000;
                UpdateBNCSLog("Hysteresis window initialised to " + _hysteresisWindow.ToString() + "ms");
                int port = inifile.ReadInt("S3_FIESTA", "Port", 2004);
                string ipAddress = inifile.ReadSetting("S3_FIESTA", "Server", CommsClient.Localhost);
                int bufferSize = inifile.ReadInt("S3_FIESTA", "BufferSizeKB", 64);
                if (bufferSize < 1)
                    bufferSize = 1;
                bufferSize *= 1024;
                uint autoReconnectSettings = (uint)inifile.ReadInt("S3_FIESTA", "ReconnectSeconds", 10);
                #if (CC_G1)
                uint cpuMilliseconds = (uint)inifile.ReadInt("S3_FIESTA", "CpuMs", 1);
                #else
                uint cpuMilliseconds = (uint)inifile.ReadInt("S3_FIESTA", "CpuMs", 0);
                #endif
                if (cpuMilliseconds > 5000) 
                    cpuMilliseconds = 5000;
                UpdateBNCSLog("TCP Delay initialised to " + cpuMilliseconds.ToString() + "ms");
                _canAutoTurnOffLogging = inifile.ReadBool("S3_FIESTA", "AutoTurnOffLogging", true);
                _showCpuWindowOnStartup = inifile.ReadBool("S3_FIESTA", "ShowCpuWindowAtStartup", false);
                _useCpuWindow = inifile.ReadBool("S3_FIESTA", "UseCpuWindow", false);
                if (!_useCpuWindow)
                {
                    miView.Items.Remove(sepPerformance);
                    miView.Items.Remove(miPerformance);
                }
                _fiestaUserName = inifile.ReadSetting("S3_FIESTA", "Username", string.Empty);
                _fiestaPassword = inifile.ReadSetting("S3_FIESTA", "Password", string.Empty);
                _pollTimeSeconds = inifile.ReadInt("S3_FIESTA", "PollSeconds", 0);
                if (_pollTimeSeconds < 0 || (_pollTimeSeconds > 0 && _pollTimeSeconds < 5))
                    _pollTimeSeconds = 0;
                tbPolling.Text = 0 == _pollTimeSeconds ? "Polling: OFF" : "Polling: " + _pollTimeSeconds.ToString() + " seconds";
                _maximumInitCommandRetries = inifile.ReadInt("S3_FIESTA", "Retries", 5);
                _logClearMinutes = inifile.ReadOrInitInt("S3_FIESTA", "ClearLogMinutes", 60);
                if (_logClearMinutes < 1)
                    _logClearMinutes = 60;
                _logger.ArchiveSeconds = (uint)(_logClearMinutes * 60);
                bool logRawTransmissionData = inifile.ReadBool("S3_FIESTA", "LogRawTransmissionData", false);
                _rxSilenceThreshold = inifile.ReadOrInitInt("S3_FIESTA", "RXSilenceReconnectThresholdSeconds", 30);
                _watchdogTimeoutSeconds = inifile.ReadOrInitInt("S3_FIESTA", "WatchdogTimeoutSeconds", 10);
                if (_watchdogTimeoutSeconds < 3)
                    _watchdogTimeoutSeconds = 3;
                _fiestaCommsSeconds = inifile.ReadOrInitInt("S3_FIESTA", "FiestaCommsSeconds", 60);
                if (_fiestaCommsSeconds < 10)
                    _fiestaCommsSeconds = 60;

                // Read EVENT LOGGING section
                miScroll.IsChecked = inifile.ReadBool("EVENT LOGGING", "scroll", true);
                miLogBncs.IsChecked = inifile.ReadBool("EVENT LOGGING", "bncs", true);
                _eventsToLog[(int)EventCatergories.Bncs] = miLogBncs.IsChecked;
                miLogComms.IsChecked = inifile.ReadBool("EVENT LOGGING", "comms", true);
                _eventsToLog[(int)EventCatergories.Comms] = miLogComms.IsChecked;
                miLogErrors.IsChecked = inifile.ReadBool("EVENT LOGGING", "errors", true);
                _eventsToLog[(int)EventCatergories.Error] = miLogErrors.IsChecked;
                miLogReceive.IsChecked = inifile.ReadBool("EVENT LOGGING", "receive", true);
                _eventsToLog[(int)EventCatergories.Receive] = miLogReceive.IsChecked;
                miLogStartup.IsChecked = inifile.ReadBool("EVENT LOGGING", "startup", true);
                _eventsToLog[(int)EventCatergories.Startup] = miLogStartup.IsChecked;
                miLogTransmit.IsChecked = inifile.ReadBool("EVENT LOGGING", "transmit", true);
                _eventsToLog[(int)EventCatergories.Transmit] = miLogTransmit.IsChecked;
                miLogValues.IsChecked = inifile.ReadBool("EVENT LOGGING", "values", true);
                _eventsToLog[(int)EventCatergories.Value] = miLogValues.IsChecked;
                miLogRegistration.IsChecked = inifile.ReadBool("EVENT LOGGING", "reg", true);
                _eventsToLog[(int)EventCatergories.Registration] = miLogRegistration.IsChecked;
                miLogConfigErrors.IsChecked = inifile.ReadBool("EVENT LOGGING", "cerrors", true);
                _eventsToLog[(int)EventCatergories.ConfigurationError] = miLogConfigErrors.IsChecked;
                miLifeCycle.IsChecked = inifile.ReadBool("EVENT LOGGING", "lcycle", true);
                _eventsToLog[(int)EventCatergories.LifeCycle] = miLifeCycle.IsChecked;

                // Read the View section
                miShowUnits.IsChecked = ShowUnits(inifile.ReadBool("VIEW", "show", false));

                // Read the debug section
                _externalDebugging = inifile.ReadBool("DEBUG", "external", false);

                // Read the UNITS section to load the instances
                for (int loop = 1; loop < 1000; ++loop)
                {
                    string unitId = inifile.ReadSetting("UNITS", "Unit_" + String.Format("{0:D3}", loop), string.Empty);
                    if (unitId.Length > 0)
                    {
                        BNCSDeviceInstance instance = _bncsInstances.GetInstance(unitId);
                        if (null != instance && instance.IsValid)
                        {
                            if (instance.Reference.DeviceNumber == _deviceNumber)
                            {
                                instance.UnitNumber = loop;
                                _instances.Add(instance);
                            }
                            else
                                UpdateEventLog("Device #" + instance.Reference.DeviceNumber.ToString() + " for \"" + instance.ToString() + "\", does not match command line device ID #" + _deviceNumber.ToString(), EventCatergories.Error);
                        }
                    }
                }

                WriteToInfodriverSlot(SLOT_CONFIGURED_INSTANCES, _instances.Count.ToString());
                WriteToInfodriverSlot(SLOT_DEFINED_INSTANCES, _bncsInstances.Count.ToString());

                // Load the device types
                UpdateEventLog("Loading device types", EventCatergories.Startup);
                _types = new FiestaDeviceTypes(paths.DeviceTypesPath, _instances);

                foreach (string error in _types.RemovedParameters)
                    UpdateEventLog(error, EventCatergories.ConfigurationError);
                foreach (string error in _types.RemovedDevices)
                    UpdateEventLog(error, EventCatergories.ConfigurationError);

                if (port > 0) // Connect to the Fiesta server
                {
                    string logPath = logRawTransmissionData ? paths.LogFilePath : string.Empty;
                    #if (CC_G1)
                    _client = new CommsClient(ipAddress, port, autoReconnectSettings, bufferSize);
                    #else
                    _client = new CommsClient2(ipAddress, port, autoReconnectSettings, bufferSize);
                    #endif
                    _client.Receive += ClientReceive;
                    _client.OnDebug += OnDebug;
                    _client.OnException += OnException;
                    _client.OnBufferFull += OnBufferFull;
                    #if (CC_G1)
                    _client.OnConnectionStateChanged += OnConnectionStateChanged;
                    #else
                    _client.OnConnectionStateChanged += OnConnectionStateChanged2;
                    #endif

                    if (logPath.Length > 0)
                    {
                        logPath = DirFuncs.IncludeTrailingBackslash(logPath);
                        string rawTransmissionLogFile = logPath + DateTime.Now.Day.ToString("D2") + "_" + ipAddress + ".log";
                        if (File.Exists(rawTransmissionLogFile))
                            rawTransmissionLogFile = FileFuncs.GenerateUniqueFilename(logPath + DateTime.Now.Day.ToString("D2") + "_" + ipAddress + ".log", string.Empty, '_');
                        CommsClientDebugger debugger = new CommsClientDebugger(rawTransmissionLogFile, DebugContent.dcBoth, true, ContentDataType.cdtText, ContentFormat.cfDated, false, true);
                        _client.Debugger = debugger;
                        _client.Debugger.OnDailyRollover += DebuggerOnDailyRollover;
                    }

                    _protocol.OnParse += OnProtocolParse;
                    _protocol.OnGetParameter += OnProtocolGetParameter;
                    _protocol.OnSetNotifyResponse += OnProtocolSetNotifyResponse;
                    _protocol.OnError += OnProtocolError;
                    _protocol.OnXmlError += OnProtocolXmlError;
                    #if (CC_G1)     
                    _client.Settings.CpuMilliseconds = cpuMilliseconds;
                    #else
                    _client.Settings.CpuMilliseconds2 = cpuMilliseconds;
                    #endif
                    _client.Connect();
                    tbSettings.Text = "Settings: " + _client.Settings.ToString();
                }
                else
                    tbSettings.Text = "Settings: TCP/IP settings not configured";

                inifile.Dispose();
            }
            catch (Exception exception)
            {
                UpdateEventLog("Unhandled exception in method LoadConfiguration()", EventCatergories.Error);
                string error = string.Empty;
                while (null != exception)
                {
                    error += exception.Message;
                    exception = exception.InnerException;
                }
                UpdateEventLog(error, EventCatergories.Error);
            }

            if (null != paths)
            {
                if (!DirFuncs.Exists(paths.LogFilePath))
                    DirFuncs.MakeDirectory(paths.LogFilePath);
            }
        }

        private string DebuggerOnDailyRollover(object sender)
        {
            string logPath = BNCSUtilities.GetBNCSFilePaths().LogFilePath;

            if (logPath.Length > 0)
            {
                string ipAddress = ((CommsTCPSettings)_client.Settings).Host;
                logPath = DirFuncs.IncludeTrailingBackslash(logPath);
                string newRawTransmissionLogFile = logPath + DateTime.Now.Day.ToString("D2") + "_" + ipAddress + ".log";
                if (File.Exists(newRawTransmissionLogFile))
                    newRawTransmissionLogFile = FileFuncs.GenerateUniqueFilename(logPath + DateTime.Now.Day.ToString("D2") + "_" + ipAddress + ".log", string.Empty, '_');
                return newRawTransmissionLogFile;
            }

            return null;
        }

        /// <summary>
        /// Toggles the height of the unit/parameter list view row
        /// </summary>
        /// <param name="show_">If true then height is set to 250 else 0</param>
        /// <returns>True if showing else false</returns>
        private bool ShowUnits(bool show_)
        {
            rdUnits.Height = new GridLength(show_ ? 250 : 0);
            if (!show_)
            {
                lvUnits.SelectedIndex = -1;
                lvParams.SelectedIndex = -1;
            }
            return show_;
        }

        /// <summary>
        /// Wrapper to transmit a buffer to the Fiesta server over the socket
        /// </summary>
        /// <param name="buffer_">Buffer to write</param>
        private void Transmit(byte[] buffer_)
        {
            if (null != buffer_)
            {
                _txCommander.Transmit(buffer_);
                UpdateEventLog(buffer_, EventCatergories.Transmit);
            }
        }

        /// <summary>
        /// Clears the UI and sends the get unit list command to the Fiesta server over the socket
        /// </summary>
        private void GetUnitList()
        {
            _selectedUnit = null;
            ClearUnits();
            Transmit(_protocol.GetUnitListCommand());
        }

        private BNCSDeviceInstance GetDeviceInstanceByUnitNumber(int unitNumber_)
        {
            foreach (BNCSDeviceInstance instance in _instances)
            {
                if (unitNumber_ == instance.UnitNumber)
                    return instance;
            }
            return null;
        }

        private byte[] ProcessParameterValueUpdate(string xmlData_, bool isPolling_)
        {
            return isPolling_ ? _stack.GetNextCommand() : null;
        }

        private void ReceiveCallback(ProcessDataResult result_, byte[] data_)
        {
            string data = new Databuffer(data_).ToString();
            
            switch (_progress)
            {
                case DriverProgress.LoggingIn: // Expecting a log in message only
                    if (ProcessDataResult.LoggedIn == result_)
                    {
                        _txCommander.Complete();
                        _txCommander.CommandWindowSeconds = 240;
                        UpdateEventLog(_protocol.Status, EventCatergories.Startup);
                        fiestaSessionLamp.IsActive = true;
                        WriteToInfodriverSlot(SLOT_STATE, "1");
                        UpdateStatus("Connected to Fiesta");
                        ++Progress;
                        Transmit(_protocol.GetUnitListCommand());
                        return;
                    }
                    else if (ProcessDataResult.GotUnregisterAll == result_)
                        return;
                    break;
                case DriverProgress.GatheringUnitsList: // Expecting a got unit list message only
                    if (ProcessDataResult.GotUnitList == result_)
                    {
                        _txCommander.Complete();
                        UpdateEventLog("Received " + result_.ToString(), EventCatergories.Operations);
                        foreach (BNCSDeviceInstance instance in _instances)
                        {
                            DeviceUnit unit = _protocol.GetDeviceByNumber(instance.UnitNumber);
                            if (null != unit)
                            {
                                if (1 == miFilter.Items.Count)
                                    miFilter.Items.Add(new Separator());
                                unit.BncsName = instance.Id;
                                _units.Add(unit);
                                MenuItem item = new MenuItem();
                                item.Header = "Unit #" + unit.Number.ToString();
                                item.Click += FilterOnClick;
                                item.Tag = unit.Number;
                                miFilter.Items.Add(item);
                            }
                            else
                                UpdateEventLog("Missing Unit#" + instance.UnitNumber.ToString() + ": " + instance.ToString(), EventCatergories.Error);
                        }
                        WriteToInfodriverSlot(SLOT_DISCOVERED_UNITS, _units.Count.ToString());

                        foreach (DeviceUnit unit in _units)
                            _stack.Add(_protocol.GetUnitCommand(unit.Number));

                        if (_stack.IsEmpty) // nothing to do, no parameters so skip to ...
                            Progress = DriverProgress.WaitingForValueChanges;
                        else
                        {
                            // Now set the maximum retries, this is deactivated for log in and get units list as until we have satisfied these we cannot proceed
                            _txCommander.MaximumRetries = _maximumInitCommandRetries;
                            ++Progress;
                            Transmit(_stack.GetNextCommand());
                        }
                        return;
                    }
                    break;
                case DriverProgress.GatheringUnitParameters: // Expecting a got unit message only
                    if (ProcessDataResult.GotUnit == result_)
                    {
                        _txCommander.Complete();
                        int unitIndex = GetUnitIndex();
                        if (unitIndex > -1)
                        {
                            //int unitNumber = _units[_stack.SelectedIndex].Number;
                            int unitNumber = _units[unitIndex].Number;
                            // Find the device instance for this unit
                            BNCSDeviceInstance deviceInstance = GetDeviceInstanceByUnitNumber(unitNumber);
                            if (null != deviceInstance)
                            {
                                int offset = deviceInstance.Reference.SlotOffset;
                                foreach (BNCSDeviceType deviceType in _types.DeviceTypes)
                                {
                                    if (deviceInstance.DeviceType.Name.Equals(deviceType.Name) || 0 == deviceInstance.DeviceType.Name.Length)
                                    {
                                        BNCSIdSlotRange range = new BNCSIdSlotRange(unitNumber, offset);
                                        if (null == range)
                                            UpdateEventLog("NULL slot range returned for device #" + unitNumber.ToString(), EventCatergories.Error);
                                        else
                                        {
                                            foreach (BNCSDeviceTypeParameter parameter in deviceType.Parameters)
                                            {
                                                if (parameter.IsPseudoParameter)
                                                {
                                                    PseudoSlot pslot = new PseudoSlot(parameter.Name, (uint)deviceInstance.UnitNumber, (uint)parameter.Slot + (uint)offset);
                                                    // Right now FiestaComms is the only pseudo parameter in use in the Fiesta system
                                                    if (parameter.Name.Equals("FiestaComms"))
                                                    {
                                                        if (!_pslots.Contains(pslot))
                                                        {
                                                            _pslots.Add(pslot);
                                                            WriteToInfodriverSlot(pslot.LiteralSlotNumber, PSLOT_NORMAL);
                                                        }
                                                    }
                                                }
                                                else
                                                    range.SetSlot(parameter.ParameterNumber, parameter.Slot);
                                            }
                                            _slotMap.Add(range);
                                            UpdateEventLog("Slots assigned for " + deviceType.Parameters.Count.ToString() + " parameters on device #" + unitNumber.ToString() + " and added to range \" " + range.ToString(), EventCatergories.Bncs);
                                        }
                                    }
                                }

                                foreach (string error in _slotMap.ErrorMessages)
                                    UpdateEventLog(error, EventCatergories.Error);

                                // Add parameters to the unit
                                int unitInErrorNumber = _protocol.LastXmlErrorUnitNumber;
                                /*
                                if (unitInErrorNumber == _units[_stack.SelectedIndex].Number && _units[_stack.SelectedIndex].Parameters.Count == 0)
                                    _units[_stack.SelectedIndex].XmlErrorDetected = true;
                                foreach (DeviceParameter parameter in _protocol.Parameters)
                                {
                                    if (_slotMap.ContainsParameter(_units[_stack.SelectedIndex].Number, parameter.Number))
                                        _units[_stack.SelectedIndex].Parameters.Add(parameter);
                                }
                                */
                                if (unitInErrorNumber == _units[unitIndex].Number && _units[unitIndex].Parameters.Count == 0)
                                    _units[unitIndex].XmlErrorDetected = true;
                                foreach (DeviceParameter parameter in _protocol.Parameters)
                                {
                                    if (_slotMap.ContainsParameter(_units[unitIndex].Number, parameter.Number))
                                        _units[unitIndex].Parameters.Add(parameter);
                                }

                                byte[] command = _stack.GetNextCommand();
                                if (null == command)
                                {
                                    ++Progress;
                                    _txCommander.Transmit(_protocol.GetAllUnitsCommand());
                                }
                                else
                                    Transmit(command);
                            }
                        }
                        return;
                    }
                    break;
                case DriverProgress.RunningInitialPoll: // Expecting a parameter result

                    switch (result_)
                    {
                        case ProcessDataResult.GotAllUnits:
                            _txCommander.Complete();
                            UpdateParameterDisplay();
                            WriteToInfoDriverSlots();
                            PopulateStack();
                            return;
                        case ProcessDataResult.GotParameterValue:
                            ExecuteGetValue(data, false);
                            return;
                        case ProcessDataResult.SetParameterValue:
                            ExecuteSetValue(data);
                            return;
                        case ProcessDataResult.GotUnitAlarms: // Added for bug #2825
                            ProcessUnitAlarm(data);
                            return;
                    }
                    break;
                case DriverProgress.SubscribingToValueChanges:
                    switch (result_)
                    {
                        case ProcessDataResult.SetNotifyOnValue:
                            _txCommander.Complete();
                            UpdateEventLog("Received " + result_.ToString(), EventCatergories.Operations);
                            if (_stack.IsEmpty)
                            {
                                ++Progress;
                                if (_pollTimeSeconds > 0)
                                    CommencePolling(true);
                            }
                            else
                                Transmit(_stack.GetNextCommand());
                            return;
                        case ProcessDataResult.GotParameterValue:
                            ExecuteGetValue(data, false);
                            return;
                        case ProcessDataResult.SetParameterValue:
                            ExecuteSetValue(data);
                            return;
                        case ProcessDataResult.GotUnitAlarms: // Added for bug #2825
                            ProcessUnitAlarm(data);
                            return;
                    }
                    break;
                case DriverProgress.WaitingForValueChanges:
                    _txCommander.Complete();
                    switch (result_)
                    {
                        case ProcessDataResult.GotSummaryStatus:
                            _summaryStatusReplyReceived = true;
                            return;
                        case ProcessDataResult.GotParameterValue:
                            ExecuteGetValue(data, false);
                            return;
                        case ProcessDataResult.SetParameterValue:
                            ExecuteSetValue(data);
                            return;
                        case ProcessDataResult.GotUnit:
                            ExecuteSetValue(data);
                            return;
                        case ProcessDataResult.GotUnitAlarms:
                            ProcessUnitAlarm(data);
                            return;
                    }
                    break;
                case DriverProgress.ShuttingDown:
                    if (result_ == ProcessDataResult.GotUnregisterAll)
                    {
                        _txCommander.Complete();
                        ++Progress;
                        Close();
                    }
                    return;
            }

            // Stll here? Then log the message and ignore
            UpdateEventLog("Received unexpected message: " + result_.ToString() + " - " + data, EventCatergories.Error);
        }

        private int GetUnitIndex()
        {
            if (++_unitIndex >= _units.Count)
                _unitIndex = _units.Count > 0 ? 0 : -1;
            return _unitIndex;
        }

        private void ExecuteSetValue(string data_)
        {
            UpdateEventLog("Received " + ProcessDataResult.SetParameterValue.ToString(), EventCatergories.Operations);
            if (_exports.Count > 0)
                UnitReceived(data_);
            Point parameter = GetUnitParameterFromXml(data_);
            if (parameter.X > 0 && parameter.Y > 0)
                Transmit(_protocol.GetParameterValueCommand((int)((Point)parameter).X, (int)((Point)parameter).Y));
        }

        private void ExecuteGetValue(string data_, bool isPolling_)
        {
            ProcessParameterValueUpdate(data_, isPolling_);
            ProcessGotParameter(data_);
        }

        /// <summary>
        /// Populates the next logical stack of commands according to progress
        /// </summary>
        private void PopulateStack()
        {
            ++Progress;

            switch (_progress)
            {
                case DriverProgress.RunningInitialPoll:
                    foreach (DeviceUnit unit in _units)
                    {
                        foreach (DeviceParameter parameter in unit.Parameters)
                            _stack.Add(_protocol.GetParameterValueCommand(unit.Number, parameter.Number), unit.Number, parameter.Number);
                    }
                    if (_stack.IsEmpty) // nothing to do, no parameters so skip to ...
                        Progress = DriverProgress.WaitingForValueChanges;
                    else
                        Transmit(_stack.GetNextCommand());
                    break;
                case DriverProgress.SubscribingToValueChanges:
                    foreach (DeviceUnit unit in _units)
                    {
                        foreach (DeviceParameter parameter in unit.Parameters)
                            _stack.Add(_protocol.SetNotifyOnValueChangedCommand(unit.Number, parameter.Number, NotificationCriteria.All));
                    }
                    if (_stack.IsEmpty) // nothing to do, no parameters so skip to ...
                        Progress = DriverProgress.WaitingForValueChanges;
                    else
                    {
                        byte[] XCommand = _stack.GetNextCommand();
                        Transmit(XCommand);
                    }
                    break;
            }

        }

        private Point GetUnitParameterFromXml(byte[] xmlData_)
        {
            return GetUnitParameterFromXml(new Databuffer(xmlData_).ToString());
        }

        private Point GetUnitParameterFromXml(string xmlData_)
        {
            xmlData_ = xmlData_.Replace("&", "&amp;");
            try
            {
                var xmlValue = XElement.Parse(xmlData_);
                var unitValue = xmlValue.Element("uN");
                var parameterValue = xmlValue.Element("pN");
                int unitNumber;
                int parameterNumber = -1;
                if (null != unitValue)
                {
                    if (Int32.TryParse(unitValue.Value, out unitNumber))
                    {
                        if (parameterValue != null)
                        {
                            if (Int32.TryParse(parameterValue.Value, out parameterNumber))
                            {
                                if (unitNumber > 0 && parameterNumber > 0)
                                    return new Point(unitNumber, parameterNumber);
                            }
                        }
                    }
                }
            }
            catch (XmlException exception)
            {
                UpdateEventLog("XML Format Error from S3 Server - please refer to log file", EventCatergories.Error);
                UpdateBNCSLog("Exception in GetUnitParameterFromXml() - " + Xception.Traverse(exception));
            }

            return new Point(0, 0);
        }

        private void ProcessUnitAlarm(string xmlData_)
        {
            /*
                Exception in ProcessUnitAlarm() - Nullable object must have a value.
              
                <getUnitAlarms><uN>151</uN><uSS><uN>151</uN><f>1</f><m>0</m><l>0</l><a>0</a><AT>N</AT></uSS></getUnitAlarms>
             
                <getUnitAlarms>
                	<uN>151</uN>
                	<uSS>
                		<uN>151</uN>
                		<f>1</f>
                		<m>0</m>
                		<l>0</l>
                		<a>0</a>
                		<AT>N</AT>
                	</uSS>
                </getUnitAlarms>
            */


            // Bug #2825
            ProcessUnitAlarmResult result = ProcessUnitAlarmResult.NoXmlValue;
            IDWriteResult? idResult = null;
            string alarm = null;
            int unitNumber = -1;

            try
            {
                var xmlValue = XElement.Parse(xmlData_);
                if (null != xmlValue)
                {
                    result = ProcessUnitAlarmResult.NoUSSValue; // Bug #2825
                    var uSSValue = xmlValue.Element("uSS");
                    if (null != uSSValue)
                    {
                        result = ProcessUnitAlarmResult.NoAlarmType;// Bug #2825
                        var alarmType = uSSValue.Element("AT");
                        if (null != alarmType)
                        {
                            result = ProcessUnitAlarmResult.NoAlarmTypeData;// Bug #2825
                            alarm = alarmType.Value;
                            if (null != alarm)
                            {
                                result = ProcessUnitAlarmResult.ZeroLengthAlarmType;// Bug #2825
                                if (alarm.Length > 0)
                                {
                                    result = ProcessUnitAlarmResult.UnknownAlarmType;// Bug #2825
                                    string value = string.Empty;

                                    switch (alarm.ToUpper()[0])
                                    {
                                        case 'N':
                                            value = PSLOT_NORMAL;
                                            result = ProcessUnitAlarmResult.Normal;// Bug #2825
                                            break;
                                        case 'S':
                                            value = PSLOT_SECONDARY;
                                            result = ProcessUnitAlarmResult.Secondary;// Bug #2825
                                            break;
                                        case 'P':
                                            value = PSLOT_PRIMARY;
                                            result = ProcessUnitAlarmResult.Primary;// Bug #2825
                                            break;
                                        case 'C':
                                            value = PSLOT_COMMS;
                                            result = ProcessUnitAlarmResult.Comms;// Bug #2825
                                            break;
                                    }

                                    if (value.Length > 0)
                                    {
                                        var unitValue = uSSValue.Element("uN");
                                        if (null != unitValue)
                                        {
                                            if (Int32.TryParse(unitValue.Value, out unitNumber))
                                            {
                                                // Bug #2825 - initialise span to null
                                                TimeSpan? span = null;
                                                try
                                                {
                                                    span = _alarmsCheckList.ProcessItem(unitNumber);
                                                }
                                                catch (Exception exception)
                                                {
                                                    _logger.Write("ProcessUnitAlarm() exception: While computing time span when processing unit alarm for unit #" + unitNumber.ToString() + ": " + exception.Message);
                                                }

                                                if (null != span)
                                                    _logger.Write("Unit Alarm response time for unit #" + unitNumber.ToString() + ": " + ((TimeSpan)span).ToString());
                                                else
                                                    _logger.Write("Unit Alarm response time for unit #" + unitNumber.ToString() + ": unknown");
                                                PseudoSlot slot = GetPseudoSlotByUnitNumber(unitNumber);
                                                if (null != slot)
                                                {
                                                    if (!slot.Value.Equals(value))
                                                    {
                                                        slot.Value = value;
                                                        idResult = WriteToInfodriverSlot(slot.LiteralSlotNumber, slot.Value);
                                                        if (idResult > IDWriteResult.Success && idResult < IDWriteResult.Deffering)
                                                            result = ProcessUnitAlarmResult.FailedToWriteToInfoDriver;
                                                    }
                                                }
                                                else // No evidence of this but to be sure log the fact that we couldn't ascertain the pseudo slot
                                                    _logger.Write("ProcessUnitAlarm() error: Failed to get pseudo slot for unit #" + unitNumber.ToString());
                                            }
                                            else
                                                result = ProcessUnitAlarmResult.InvalidUnitValue;
                                        }
                                        else
                                            result = ProcessUnitAlarmResult.NoUnitValue;
                                    }
                                }
                            }
                        }
                    }
                }

                if (result < ProcessUnitAlarmResult.Normal) // Bug #2825
                {
                    string error = "ProcessUnitAlarms() error: Result - " + result.ToString();
                    if (ProcessUnitAlarmResult.FailedToWriteToInfoDriver == result)
                        error += " - " + idResult.ToString();
                    UpdateBNCSLog(error + ": \"" + xmlData_ + "\"");
                }
                
                byte[] nextCommsCommand = _stack.GetNextCommand();
                if (null != nextCommsCommand)
                    Transmit(nextCommsCommand);
            }
            catch (Exception exception)
            {
                if (exception is XmlException)
                    UpdateEventLog("XML Format Error from S3 Server - please refer to log file", EventCatergories.Error);
                UpdateBNCSLog("ProcessUnitAlarm() exception: " + Xception.Traverse(exception));
            }
        }

        private PseudoSlot GetPseudoSlotByUnitNumber(int unitNumber_)
        {
            foreach (PseudoSlot slot in _pslots)
            {
                if (unitNumber_ == slot.DeviceNumber)
                    return slot;
            }
            return null;
        }

        private void ProcessGotParameter(string xmlData_)
        {
            try
            {
                var xmlValue = XElement.Parse(xmlData_);
                var unitValue = xmlValue.Element("uN");
                var parameterValue = xmlValue.Element("pN");
                var value = xmlValue.Element("value");

                int unitNumber;
                int parameterUnit = -1;
                if (Int32.TryParse(unitValue.Value, out unitNumber))
                {
                    if (!Int32.TryParse(parameterValue.Value, out parameterUnit))
                        parameterUnit = -1;

                }
                else
                    unitNumber = -1;

                if (unitNumber > -1 && parameterUnit > -1)
                {
                    foreach (DeviceUnit unit in _units)
                    {
                        if (unit.Number == unitNumber)
                        {
                            int index = -1;
                            foreach (DeviceParameter parameter in unit.Parameters)
                            {
                                ++index;
                                if (parameter.Number == parameterUnit)
                                {
                                    // Use this line if there is a requirement to show a time on start-up, as is only actual change in value will show the time
                                    //if (!unit.Parameters[index].Value.Equals(value.Value) || null == unit.Parameters[index].LastUpdateTime || 0 == unit.Parameters[index].LastUpdateTime.Length)
                                    if (!unit.Parameters[index].Value.Equals(value.Value))
                                    {
                                        unit.Parameters[index].Value = value.Value;
                                        unit.Parameters[index].LastUpdateTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                if (exception is XmlException)
                    UpdateEventLog("XML Format Error from S3 Server - please refer to log file", EventCatergories.Error);
                UpdateBNCSLog("Exception in ProcessGotParameter() - " + Xception.Traverse(exception));
            }
        }

        private void CommencePolling(bool force_)
        {
            if (!_pollingInitialised || force_)
            {
                _pollingInitialised = true;
                if (_pollTimeSeconds > 0)
                {
                    _pollTimer.Interval = new TimeSpan(0, 0, 0, _pollTimeSeconds, 0);
                    _unitPollIndex = 0;
                    _unitParameterIndex = -1;
                    _pollTimer.Start();
                }
            }
        }

        private void UnitReceived(string unitParameterData_)
        {
            if (_exports.Count > 0)
            {
                SelectedUnitForExport exportUnit = _exports.Dequeue();
                bool success = false;
                string errorInfo = string.Empty;
                DeviceUnit unit = _protocol.GetDeviceByNumber(exportUnit.UnitNumber);
                try
                {
                    if (exportUnit.ForceTypeNameToMatchFileName)
                        unit.Name = FileNameToTypeName(exportUnit.FileTarget);
                    success = BNCSDeviceType.ExportDeviceType(exportUnit.FileTarget, unit.Name, _protocol.TranslateUnitParameters(unitParameterData_));
                }
                catch (Exception exception)
                {
                    success = false;
                    while (null != exception)
                    {
                        errorInfo += exception.Message + "\n";
                        exception = exception.InnerException;
                    }
                }

                if (success)
                {
                    _configwindow.InformationUpdate("The device type \"" + unit.Name + "\" was successfully saved to \"" + exportUnit.FileTarget + "\".");
                    UpdateEventLog("The device type \"" + unit.Name + "\" was successfully saved to \"" + exportUnit.FileTarget + "\".", EventCatergories.Operations);
                }
                else
                {
                    _configwindow.InformationUpdate("An error occured while trying to export the device type \"" + unit.Name + "\" to file \"" + exportUnit.FileTarget + "\".");
                    UpdateEventLog("An error occured while trying to export the device type \"" + unit.Name + "\" to file \"" + exportUnit.FileTarget + "\".", EventCatergories.Error);
                    if (0 != errorInfo.Length)
                        UpdateEventLog(errorInfo, EventCatergories.Error);
                }
            }
        }
        #endregion

        #region Private UI update methods (typically dispatched from another thread)
        /// <summary>
        /// Updates the status label
        /// </summary>
        /// <param name="message_">Text to update label with</param>
        private void UpdateStatus(string message_)
        {
            tbStatus.Text = "Status: " + message_;
        }

        /// <summary>
        /// Updates the LED lights according to connection status
        /// </summary>
        /// <param name="status_">Comms Client connection status</param>
        #if (CC_G1)
        private void UpdateLED(ConnectionStatus status_)
        #else
        private void UpdateLED(ConnectionStatus2 status_)
        #endif
        {
            fiestaLamp.Flashing = false;
            switch (status_)
            {
                #if (CC_G1)
                case ConnectionStatus.Connected:
                #else
                case ConnectionStatus2.Connected:
                #endif
                    fiestaLamp.IsActive = true;
                    fiestaLamp.ColourOn = Colors.Green;
                    ExecuteWriteToInfodriverSlot(SLOT_COMMS, "1");
                    break;
                #if (CC_G1)
                case ConnectionStatus.Disconnected:
                #else
                case ConnectionStatus2.Disconnected:
                #endif
                    fiestaLamp.IsActive = false;
                    ExecuteWriteToInfodriverSlot(SLOT_COMMS, "0");
                    fiestaSessionLamp.IsActive = false;
                    ExecuteWriteToInfodriverSlot(SLOT_STATE, "0");
                    _selectedUnit = null;
                    ClearUnits();
                    break;
                #if (CC_G1)
                case ConnectionStatus.Reconnecting:
                #else
                case ConnectionStatus2.Reconnecting:
                #endif
                    fiestaLamp.IsActive = true;
                    fiestaLamp.Flashing = true;
                    fiestaLamp.ColourOn = Colors.Orange;
                    fiestaSessionLamp.IsActive = false;
                    ExecuteWriteToInfodriverSlot(SLOT_STATE, "0");
                    break;
            }
        }

        /// <summary>
        /// Updates the event log ListBox according to catergory with the supplied buffer
        /// </summary>
        /// <param name="buffer_">Buffer to add to list</param>
        /// <param name="catergory_">Catergory used to colour the output</param>
        private void UpdateEventLog(byte[] buffer_, EventCatergories catergory_)
        {
            UpdateEventLog(new Databuffer(buffer_).ToString(), catergory_);
        }

        private string MaskPasswordInXml(string message_)
        {
            // Mask the logged password with stars
            string passwordStr = message_.Substring(message_.IndexOf("<password>"));
            message_ = message_.Substring(0, message_.IndexOf("<password>"));
            string password = string.Empty;
            int start = 10;
            int end = passwordStr.IndexOf("</password>");
            for (int loop = 10; loop < 10 + (end - start); ++loop)
                password += passwordStr[loop];
            string maskedPassword = string.Empty;
            foreach (char character in password)
                maskedPassword += "*";
            message_ += passwordStr.Replace(password, maskedPassword);
            return message_;
        }

        private string GetError(string xmlText_)
        {
            string xmlText = xmlText_.ToLower();

            if (xmlText.Contains("<errormessage>") && xmlText.Contains("</errormessage>"))
            {
                int start = xmlText.IndexOf("<errormessage>");
                int end = xmlText.IndexOf("</errormessage>");
                if (start > -1 && end > start)
                {
                    string result = xmlText_.Substring(0, end);
                    return result.Substring(start + 14);
                }
            }

            return null;
        }

        /// <summary>
        /// Updates the event log ListBox according to catergory with the supplied string, checks for password submission and masks to log
        /// </summary>
        /// <param name="message_">String to add to list</param>
        /// <param name="catergory_">Catergory used to colour the output</param>
        private void UpdateEventLog(string message_, EventCatergories catergory_)
        {
            if (_canLogToViewer)
            {
                bool updateUI = 0 == _unitFilter || EventCatergories.LifeCycle == catergory_;

                if (!updateUI)
                {
                    string message = message_.ToLower();
                    string filter1 = "unit #" + _unitFilter.ToString();
                    string filter2 = "<un>" + _unitFilter.ToString() + "</un>";
                    updateUI = message.Contains(filter1) || message.Contains(filter2);
                }

                if (updateUI)
                {
                    string error = GetError(message_);
                    if (null != error)
                    {
                        _dataQueue.Add(new EventItem(error, EventCatergories.Error));
                        UpdateBNCSLog(error);
                    }

                    if (_eventsToLog[(int)catergory_])
                    {
                        // When logging a password submission ensure we mask it to the log
                        if (Regex.Match(message_, @"<logUserIn><user>([A-Za-z0-9\-]+)</user><password>([A-Za-z0-9\-]+)</password></logUserIn>$", RegexOptions.IgnoreCase).Success)
                            message_ = MaskPasswordInXml(message_);

                        _dataQueue.Add(new EventItem(message_, catergory_));

                        switch (catergory_)
                        {
                            case EventCatergories.Receive:
                                // Do nothing
                                break;
                            case EventCatergories.Transmit:
                                message_ = "TX: " + message_;
                                UpdateBNCSLog(message_);
                                break;
                            case EventCatergories.Error:
                                UpdateBNCSLog(message_);
                                break;
                            default:
                                UpdateBNCSLog(message_);
                                break;
                        }
                    }
                }
                else
                    UpdateBNCSLog(message_);
            }
        }

        private void UpdateBNCSLog(string message_)
        {
            if (null != _logger)
                _logger.Write(message_, _externalDebugging);
        }

        /// <summary>
        /// Updates a parameter value to the InfoDriver
        /// </summary>
        /// <param name="parameter_">Parameter</param>
        private void UpdateParameter(OnGetParameterValueEventArgs parameter_)
        {
            int slot = _slotMap.GetSlot(parameter_.Unit, parameter_.Parameter);
            switch (slot)
            {
                case -1:
                    UpdateEventLog("Allocated slot for Device #" + parameter_.Unit.ToString() + " Parameter #" + parameter_.Parameter.ToString() + " exceeds the highest slot number reserved for this device.", EventCatergories.Error);
                    break;
                case 0:
                    UpdateEventLog("Device #" + parameter_.Unit.ToString() + " not found in internal slot map.", EventCatergories.Error);
                    break;
                default:
                    string value = null == parameter_.Value ? string.Empty : parameter_.Value;
                    switch (WriteToInfodriverSlot((uint)slot, value))
                    {
                        case IDWriteResult.Failure:
                            UpdateEventLog("Failed to write value \"" + value + " to slot #" + slot.ToString(), EventCatergories.Bncs);
                            break;
                        case IDWriteResult.NotConnected:
                            UpdateEventLog("Not connected to InfoDriver #" + _deviceNumber.ToString(), EventCatergories.Error);
                            break;
                    }
                    break;
            }
        }

        private void UpdateParameterDisplay()
        {
            if (null != lvUnits.SelectedItem)
                UnitsSelectionChanged(null, null);
        }

        /// <summary>
        /// Checks the reported value from SetNoitifyOnValueChanged and if a change has occured then both the UI and ID are updated
        /// </summary>
        /// <param name="unitNumber_">Unit number subscribed to</param>
        /// <param name="parameterNumber_">Parameter subscribed to</param>
        /// <param name="value_">Latest value</param>
        private void UpdateSubscribedParameter(int unitNumber_, int parameterNumber_, string value_)
        {
            // For backwards compatability ignore null values.
            if (null != value_)
            {
                // Traverse the devices...
                foreach (DeviceUnit unit in _units)
                {
                    // ... until we find the subscribed device
                    if (unit.Number == unitNumber_)
                    {
                        // Traverse the subscribed devices parameters...
                        foreach (DeviceParameter parameter in unit.Parameters)
                        {
                            // ... until we find the subscribed parameter
                            if (parameter.Number == parameterNumber_)
                            {
                                // Only do something if the value has chamnged
                                if (!parameter.Value.Equals(value_))
                                {
                                    // Update the UI here
                                    parameter.Value = value_;
                                    // Update the InfoDriver
                                    int slot = _slotMap.GetSlot(unitNumber_, parameterNumber_);
                                    if (slot > 0)
                                        WriteToInfodriverSlot((uint)slot, value_);
                                    string message = "Unit #" + unitNumber_.ToString() + " Parameter #" + parameterNumber_.ToString() + " value change: " + value_;
                                    UpdateBNCSLog(message);
                                    UpdateEventLog(message, EventCatergories.Operations);
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
        #endregion

        #region Shutdown
        private void ForceClose()
        {
            Progress = DriverProgress.ClearToExit;
            Close();
        }

        /// <summary>
        /// Closing event handler, called when the driver is closing
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void AppClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = _progress != DriverProgress.ClearToExit && _client.IsConnected;
            if (e.Cancel)
            {
                if (DriverProgress.ShuttingDown != _progress)
                {
                    _stack.Clear();
                    _txCommander.Complete();
                    _txCommander.MaximumRetries = 2;
                    _txCommander.CommandWindowSeconds = 20;
                    Progress = DriverProgress.ShuttingDown;
                    Transmit(_protocol.GetUnregisterAllNotifyCommand());
                    tbShutdownMessage.Visibility = Visibility.Visible;
                    mmMain.Visibility = Visibility.Hidden;
                }
                else
                {
                    ShutdownMessage("The S3 Fiesta Driver is running it's shutdown routine, please wait...", false);
                    tbShutdownMessage.Foreground = Brushes.Red;
                }
            }
            else
            {
                try
                {
                    ShutdownMessage("Exiting...", true);
                    if (null != _configwindow)
                    {
                        ShutdownMessage("Closing configuration editor...", true);
                        _configwindow.CloseEditor();
                    }

                    OnDataQueueUpdate(null, _dataQueue.Close());
                    UpdateBNCSLog("Driver closing");
                    ShutdownMessage("Disconnecting from the Fiesta Server...", true);
                    WPFApplication.DoEvents();

                    if (null != _hysteresis)
                        _hysteresis.Close();
                    if (null != _commsStatusTimer)
                        CommsStatusTimerControl(false); // Bug #2825
                    _pollTimer.Stop();
                    _logTimer.Stop();

                    // Update the pseudo slots
                    foreach (PseudoSlot slot in _pslots)
                        WriteToInfodriverSlot(slot.LiteralSlotNumber, PSLOT_DRIVER_CLOSE);

                    // Disconnect from the Fiesta server
                    if (_client.IsConnected)
                        _client.Disconnect();
                    // Ensure we set the comms slots to zero
                    ShutdownMessage("Updating InfoDriver Comms slots...", true);
                    string message = string.Empty;
                    switch (WriteToInfodriverSlot(SLOT_STATE, "0"))
                    {
                        case IDWriteResult.Failure:
                            message = "Failed to write to state slot at shutdown";
                            break;
                        case IDWriteResult.NotConnected:
                            message = "Failed to write to state slot at shutdown - InfoDriver is not connected";
                            break;
                        case IDWriteResult.Success:
                            message = "Write to state slot succeeded";
                            break;
                    }
                    UpdateBNCSLog(message);

                    switch (WriteToInfodriverSlot(SLOT_COMMS, "0"))
                    {
                        case IDWriteResult.Failure:
                            message = "Failed to write to comms slot at shutdown";
                            break;
                        case IDWriteResult.NotConnected:
                            message = "Failed to write to comms slot at shutdown - InfoDriver is not connected";
                            break;
                        case IDWriteResult.Success:
                            message = "Write to comms slot succeeded";
                            break;
                    }
                    UpdateBNCSLog(message);

                    if (null != _performanceWindow)
                    {
                        UpdateBNCSLog("Session CPU peak: " + _performanceWindow.PeakCpu.ToString() + "%");
                        UpdateBNCSLog("Session CPU average: " + _performanceWindow.AverageCpu.ToString() + "%");
                        _performanceWindow.CloseWindow();
                    }

                    // Disconnect from the InfoDriver
                    if (null != _infoDriver)
                    {
                        ShutdownMessage("Disconnecting from InfoDriver #" + _deviceNumber.ToString() + "...", true);
                        if (_infoDriver.HostConnectState == HostDriverConnectState.Connected)
                        {
                            _infoDriver.OkToGoRxOnly = true;
                            _infoDriver.OkToGoTXRX = false;
                            try
                            {
                                _infoDriver.ChangeIfMode(HostDriverIFMode.RxOnly);
                            }
                            catch { }
                            _infoDriver.Disconnect();
                        }
                        _infoDriver.Dispose();
                    }

                    // Save INI file if user display changes have occured
                    if (_saveIni)
                    {
                        ShutdownMessage("Updating INI file...", true);
                        SaveIni();
                    }

                    if (null != _logger)
                    {
                        ShutdownMessage("Finalising log...", true);
                        _logger.Close();
                    }

                    if (_showLogOnExit)
                    {
                        if (null != _client.Debugger)
                        {
                            if (File.Exists(_client.Debugger.TxFilename))
                                Execute.ShellExecute(_client.Debugger.TxFilename);
                            if (_client.Debugger.SplitFiles && File.Exists(_client.Debugger.RxFilename))
                                Execute.ShellExecute(_client.Debugger.RxFilename);
                        }
                        Execute.ShellExecute(_logger.Filename);
                    }
                }
                finally
                {
                    ShutdownMessage("Bye", true);
                    SingleApplicationInstance.Close();
                }
            }
        }

        private void ShutdownMessage(string message_, bool forceEvent_)
        {
            tbShutdownMessage.Text = message_;
            if (forceEvent_)
                WPFApplication.DoEvents();
        }
        #endregion

        #region Private property
        private DriverProgress Progress
        {
            get { return _progress; }
            set 
            {
                if (_progress != value)
                {
                    _progress = value;
                    string message = string.Empty;
                    switch (_progress)
                    {
                        case DriverProgress.LoggingIn:
                            message = "Driver is awaiting log in confirmation ...";
                            break;
                        case DriverProgress.GatheringUnitsList:
                            message = "Driver is awaiting unit lists ...";
                            break;
                        case DriverProgress.GatheringUnitParameters:
                            message = "Driver is awaiting unit parameter lists ...";
                            break;
                        case DriverProgress.RunningInitialPoll:
                            message = "Driver is commencing initial poll of units ...";
                            break;
                        case DriverProgress.SubscribingToValueChanges:
                            message = "Driver is subscribing to unit/parameter value changes ...";
                            break;
                        case DriverProgress.WaitingForValueChanges:
                            message = "Driver is now in normal operation mode and responding to value changes ...";
                            if (null == _commsStatusTimer && (_rxSilenceThreshold > 0 || _pslots.Count > 0))
                            {
                                _commsStatusTimer = new DispatcherTimer();
                                _commsStatusTimer.Interval = new TimeSpan(0, 0, _fiestaCommsSeconds);
                                _commsStatusTimer.Tick += CommsStatusTimerTick;
                            }
                            CommsStatusTimerControl(true);
                            _lastRXTick = Environment.TickCount;
                            
                            if (null != _performanceWindow)
                            {
                                UpdateBNCSLog("Initialisation CPU peak: " + _performanceWindow.PeakCpu.ToString() + "%");
                                UpdateBNCSLog("Initialisation CPU average: " + _performanceWindow.AverageCpu.ToString() + "%");
                            }
                            break;
                        case DriverProgress.ShuttingDown:
                            message = "Driver has commenced shutdown ...";
                            break;
                        case DriverProgress.ClearToExit:
                            message = "Driver exiting.";
                            break;
                    }

                    UpdateEventLog(message, EventCatergories.LifeCycle);
                    string val = (((int)_progress) + 1).ToString();
                    WriteToInfodriverSlot(SLOT_STATE, val);
                }
            }
        }
        #endregion

        #region Public method
        public void HandleUnhandledException(Exception exception_)
        {
            if (null == _logger)
            {
                TextWriter writer = new StreamWriter(AssemblyResolver.FailoverLogFileName, false);
                writer.WriteLine("UNHANDLED EXCEPTION THROWN - driver closing!");
                writer.WriteLine("UNHANDLED EXCEPTION IN OBJECT:  \"" + exception_.Source + "\" IN METHOD: \"" + exception_.TargetSite + "\"");
                writer.WriteLine("STACK TRACE: " + Environment.StackTrace);
                writer.WriteLine("EXCEPTION TYPE: " + exception_.GetType().ToString());
                writer.Write("TRAVERSED EXCEPTION MESSAGE(S): " + Xception.Traverse(exception_));
                writer.Close();
            }
            else
            {
                _logger.Write("UNHANDLED EXCEPTION THROWN - driver closing!");
                _logger.Write("UNHANDLED EXCEPTION IN OBJECT:  \"" + exception_.Source + "\" IN METHOD: \"" + exception_.TargetSite + "\"");
                _logger.Write("STACK TRACE: " + Environment.StackTrace);
                _logger.Write("EXCEPTION TYPE: " + exception_.GetType().ToString());
                _logger.Write("TRAVERSED EXCEPTION MESSAGE(S): " + Xception.Traverse(exception_));
            }
            ForceClose();
        }
        #endregion

        #region Public properties
        public EventItemsLogViewModel Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public ObservableCollection<DeviceUnit> Units
        {
            get { return _units; }
            set { _units = value; }
        }

        public string BncsLibDLL
        {
            get { return _bncsLibDLL; }
            set { _bncsLibDLL = value; }
        }

        public string BncsControlLibDLL
        {
            get { return _bncsControlLibDLL; }
            set { _bncsControlLibDLL = value; }
        }
        #endregion
    }
}
