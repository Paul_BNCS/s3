﻿using System;
using BNCS;

namespace s3_fiesta
{
    public class DeviceTypeItem
    {
        private DeviceUnit _device = null;
        private bool _instantiate = false;

        public DeviceTypeItem(DeviceUnit device_)
        {
            _device = device_;
        }

        public DeviceUnit Device
        {
            get { return _device; }
        }

        public bool Instantiate
        {
            get { return _instantiate; }
            set { _instantiate = value; }
        }
    }
}
