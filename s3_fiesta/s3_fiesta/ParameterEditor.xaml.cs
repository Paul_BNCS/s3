﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BNCS;

namespace s3_fiesta
{
    /// <summary>
    /// Interaction logic for ParameterEditor.xaml
    /// </summary>
    public partial class ParameterEditor : Window
    {
        #region Private members
        private string _currentValue = string.Empty;
        private bool _ok = false;
        #endregion

        #region Constructor
        public ParameterEditor(DeviceUnit unit_, DeviceParameter parameter_)
        {
            InitializeComponent();
            Title = "Edit " + unit_.Name + " Parameter #" + parameter_.Number.ToString();
            tbValue.Text = parameter_.Value;
            _currentValue = parameter_.Value;
            tbValue.Focus();
            tbValue.SelectAll();
        }
        #endregion

        #region Private event handlers
        private void TextChange(object sender, TextChangedEventArgs e)
        {
            bnOk.IsEnabled = !_currentValue.Equals(tbValue.Text);
        }

        private void TextDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && true == bnOk.IsEnabled)
                OKClick(null, null);
        }

        private void OKClick(object sender, RoutedEventArgs e)
        {
            _ok = true;
            Close();
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion

        #region Public property
        /// <summary>
        /// Returns the new parameter value or null if cancelled
        /// </summary>
        public string Value
        {
            get
            {
                return _ok ? tbValue.Text : null;
            }
        }
        #endregion
    }
}
