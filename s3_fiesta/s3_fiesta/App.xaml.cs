﻿using System;
using System.IO;
using System.Windows;
using System.Reflection;
using System.Windows.Threading;
using Bncs.Assembly;

using System.Collections.Generic;

namespace s3_fiesta
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Private members
        private MainWindow _mainWindow = null;
        private string _bncsLibDLL = string.Empty;
        private string _bncsControlLibDLL = string.Empty;
        #endregion

        #region Private methods
        /// <summary>
        /// Application entry point
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void AppStartup(object sender, StartupEventArgs e)
        {
            string[] test = AssemblyResolver.CheckForExpectedDLLs(new string[] { "bncslib.dll", "bncscontrollib.dll" });

            if (null == test)
            {
                AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainAssemblyResolve;
                AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;

                // Create main application window
                _mainWindow = null;
                try
                {
                    _mainWindow = new MainWindow();
                    _mainWindow.BncsControlLibDLL = _bncsControlLibDLL;
                    _mainWindow.BncsLibDLL = _bncsLibDLL;
                    DispatcherUnhandledException += DispatchUnhandledException;
                    _mainWindow.Show();
                }
                catch (FileNotFoundException exception)
                {
                    Exception fullException = (Exception)exception;
                    string message = string.Empty;
                    string caption = "Missing DLL";
                    if (fullException.Message.StartsWith("Could not load file or assembly 'BNCSLib,"))
                        message = "Could not load missing DLL: BNCSLib.dll - please ensure that this resides in a directory on your System Path or the same directory from which you are running this driver.";
                    else if (fullException.Message.StartsWith("Could not load file or assembly 'BncsControlLib,"))
                        message = "Could not load missing DLL: BncsControlLib.dll - please ensure that this resides in a directory on your System Path or the same directory from which you are running this driver.";
                    if (0 == message.Length)
                    {
                        while (null != fullException)
                        {
                            message += fullException.Message + "\n\n";
                            caption = fullException.GetType().ToString() + " exception";
                            fullException = fullException.InnerException;
                        }
                    }
                    if (null != _mainWindow)
                        _mainWindow.HandleUnhandledException(exception);
                    else
                        Shutdown();
                }
                catch (Exception exception)
                {
                    if (null != _mainWindow)
                        _mainWindow.HandleUnhandledException(exception);
                    else
                    {
                        string message = string.Empty;
                        while (null != exception)
                        {
                            message += exception.GetType().ToString() + ": " + exception.Message + "\n\n";
                            exception = exception.InnerException;
                        }
                        ShutdownMessage(message);
                    }
                }
            }
            else
            {
                string error = "The following DLL" + (1 == test.Length ? string.Empty : "'s") + " could not be found on the System Path nor the Application Directory:\n";
                foreach (string dll in test)
                    error += "\n" + dll;
                error += "\n\nPlease rectify this before retrying.";
                MessageBox.Show(error, "Missing DLL(s)");
                Shutdown();
            }
        }

        private void ShutdownMessage(string message_)
        {
            TextWriter writer = new StreamWriter(AssemblyResolver.FailoverLogFileName, false);
            writer.WriteLine(message_);
            writer.Close();

            Shutdown();
        }
        #endregion

        #region Private event handlers
        /// <summary>
        /// Fires when a domain level exception occurs, this will catch missing BncsControlLib.dll (if kept in project) else pass exception to main
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (null != _mainWindow)
                _mainWindow.HandleUnhandledException((Exception)e.ExceptionObject);
            else
                Shutdown();
        }

        /// <summary>
        /// Fires when any other unhandled exception occurs, passes the exception to main
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void DispatchUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            _mainWindow.HandleUnhandledException(e.Exception);
        }

        /// <summary>
        /// This handler is called only when the common language runtime tries to bind to the assembly and fails. This is our opportunity to resolve it from the system path.
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        /// <returns>An object of type Assembly if resolved else null</returns>
        private Assembly CurrentDomainAssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.ToLower().StartsWith("bncs"))
            {
                AssemblyWithFilename assembly = AssemblyResolver.ResolveFromPath(args.Name);
                if (assembly.Loaded)
                {
                    if (assembly.Filename.ToLower().Contains("bncslib.dll"))
                    {
                        _bncsLibDLL = assembly.Filename;
                        if (null != _mainWindow)
                            _mainWindow.BncsLibDLL = _bncsLibDLL;
                    }
                    return assembly.Assembly;
                }
            }
            return null;
        }
        #endregion
    }
}