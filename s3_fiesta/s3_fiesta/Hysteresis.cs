﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace BNCS
{
    /// <summary>
    /// An internal class that holds hysteresis data for a single slot, this class is instantiated automatically by the Hysteresis class and there is nnever a need to do this in code at the driver level..
    /// </summary>
    internal class InfoDriverSlotData
    {
        #region Private members
        private string _currentValue = string.Empty;
        private string _lastRevertedValue = string.Empty;
        private uint _hysteresisMilliSeconds = 1000;
        private DateTime _lastRevertedTime = DateTime.Now;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="data_">The initial value to revert to the slot</param>
        /// <param name="hysteresisMilliSeconds_">A period of time in milliseconds defining the hysteresis window, minimum is 1ms, defaults to 1000ms</param>
        public InfoDriverSlotData(string data_, uint hysteresisMilliSeconds_)
        {
            _currentValue = data_;
            _lastRevertedValue = data_;
            if (hysteresisMilliSeconds_ > 0)
                _hysteresisMilliSeconds = hysteresisMilliSeconds_;
            _lastRevertedTime = DateTime.Now; ;
        }
        #endregion

        #region Private r/o property
        private string LastRevertedTime
        {
            get { return _lastRevertedTime.ToLongTimeString() + "." + _lastRevertedTime.Millisecond.ToString("D3"); }
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Attempts to set the current value to that supplied.
        /// </summary>
        /// <param name="value_">Requested value to revert to the slot.</param>
        /// <returns>True if the calling method may send the revertive else false. A false value means that either the value has not changed or it has but will be cleared to revert once the hysteresis time window has elapsed</returns>
        public bool SetCurrentValue(string value_)
        {
            if (!value_.Equals(_currentValue))
            {
                _currentValue = value_;
                if (_currentValue.Equals(_lastRevertedValue))
                {
                    _lastRevertedTime = DateTime.Now;
                    return false;
                }
                TimeSpan span = DateTime.Now.Subtract(_lastRevertedTime);
                if (span.Milliseconds >= (int)_hysteresisMilliSeconds)
                {
                    _lastRevertedValue = _currentValue;
                    _lastRevertedTime = DateTime.Now;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Queries the data to ascertain as to whether the calling process may revert the current value to the slot.
        /// </summary>
        /// <returns>True if the calling method may send the revertive else false. A false value means that either the value has not changed or it has but will be cleared to revert once the hysteresis time window has elapsed</returns>
        public bool RevertDataQuerey()
        {
            if (!_currentValue.Equals(_lastRevertedValue))
            {
                TimeSpan span = DateTime.Now.Subtract(_lastRevertedTime);
                if (span.TotalMilliseconds >= (int)_hysteresisMilliSeconds)
                {
                    _lastRevertedValue = _currentValue;
                    _lastRevertedTime = DateTime.Now;
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region Public overridden method
        /// <summary>
        /// Gets the instance as a string.
        /// </summary>
        /// <returns>The current value followed by the time and value of the  last permitted revertive.</returns>
        public override string ToString()
        {
            return "\"" + _currentValue + "\" [Reverted \"" + _lastRevertedValue + "\" @ " + LastRevertedTime + "]";
        }
        #endregion

        #region Public r/o property
        /// <summary>
        /// Gets the current value.
        /// </summary>
        public string CurrentValue
        {
            get { return _currentValue; }
        }
        #endregion
    }

    /// <summary>
    /// A class that is employed to introduce a level of hysteresis to frequent infodriver slot changes
    /// </summary>
    public class Hysteresis
    {
        #region Private members
        private uint _deviceNumber = 0;
        private uint _hysteresisMilliSeconds = 1000;
        private Dictionary<uint, InfoDriverSlotData> _infoDriverStates = new Dictionary<uint, InfoDriverSlotData>();
        private Thread _thread = null;
        private bool _isMonitoring = false;
        private event HysteresisEventHandler _onRevert = null;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="deviceNumber_">The device number to revert</param>
        /// <param name="hysteresisMilliSeconds_">A period of time in milliseconds defining the hysteresis window, minimum is 1ms, defaults to 1000ms</param>
        public Hysteresis(uint deviceNumber_, uint hysteresisMilliSeconds_)
        {
            _deviceNumber = deviceNumber_;
            if (hysteresisMilliSeconds_ > 0)
                _hysteresisMilliSeconds = hysteresisMilliSeconds_;
        }
        #endregion

        #region Private thread execute method
        private void MonitorThreadExecute()
        {
            while (_isMonitoring)
            {
                Dictionary<uint, string> revertives = null;
                Thread.Sleep((int)_hysteresisMilliSeconds);

                if (null != _onRevert)
                {
                    foreach (KeyValuePair<uint, InfoDriverSlotData> infoDriverState in _infoDriverStates)
                    {
                        if (infoDriverState.Value.RevertDataQuerey())
                        {
                            if (null == revertives)
                                revertives = new Dictionary<uint, string>();
                            revertives.Add(infoDriverState.Key, infoDriverState.Value.CurrentValue);
                        }
                    }
                }

                if (null != revertives)
                {
                    HysteresisRevertEventArgs args = new HysteresisRevertEventArgs();
                    args.Revertives = revertives;
                    _onRevert(this, args);
                }
            }
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Requests permission to revert the provided value to the specified slot
        /// </summary>
        /// <param name="slot_">Slot number</param>
        /// <param name="message_">Message to write</param>
        /// <returns>True if the calling method may send the revertive else false. A false value means that either the value has not changed or it has but will be cleared to revert once the hysteresis time window has elapsed</returns>
        public bool RevertDataRequest(uint slot_, string message_)
        {
            if (_infoDriverStates.ContainsKey(slot_))
                return _infoDriverStates[slot_].SetCurrentValue(message_);
            else
            {

                _infoDriverStates.Add(slot_, new InfoDriverSlotData(message_, _hysteresisMilliSeconds));
                if (null == _thread)
                {
                    _thread = new Thread(new ThreadStart(MonitorThreadExecute));
                    _thread.Start();
                    _isMonitoring = true;
                }
                return true;
            }
        }

        /// <summary>
        /// Stops the monitoring thread. Close() MUST be called when your driver is terminating to ensure that the thread is terminated and hence ensuring that the driver will go out of process.
        /// </summary>
        public void Close()
        {
            _isMonitoring = false;
        }
        #endregion

        #region Public overridden method
        /// <summary>
        /// Gets the object instance as a string
        /// </summary>
        /// <returns>The device number and the current number of slots for which requests have been made</returns>
        public override string ToString()
        {
            return _deviceNumber.ToString("D4") + " - Slots reverted: " + _infoDriverStates.Count.ToString();
        }
        #endregion

        #region Public event handler
        /// <summary>
        /// Fires when one or more revertive requests have been cleared to process.
        /// </summary>
        public event HysteresisEventHandler OnRevert
        {
            add { _onRevert += value;  }
            remove { _onRevert -= value; }
        }
        #endregion
    }

    /// <summary>
    /// An EventArgs derivitive that contains a dictionary of slot/value pairs to revert.
    /// </summary>
    public class HysteresisRevertEventArgs : EventArgs
    {
        #region Public member
        /// <summary>
        /// The dictionary of slot/value pairs to revert
        /// </summary>
        public Dictionary<uint, string> Revertives = null;
        #endregion
    }
    /// <summary>
    /// The delegate for the Hysteresis.OnRevert event handler.
    /// </summary>
    /// <param name="sender">Calling object</param>
    /// <param name="e">Arguments</param>
    public delegate void HysteresisEventHandler(object sender, HysteresisRevertEventArgs e);
}
