﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BNCS;

namespace s3_fiesta
{
    /// <summary>
    /// BNCSDeviceTypes derivitive to ensure that all BNCSDeviceType members BNCSDeviceTypeParameter parameters have picked up a ParameterIndex, i.e. this index is greater
    /// than the default -1. This is actually set through the msg parameter in the XML containing:
    /// 
    /// pN=n
    /// 
    /// ... where n is a value greater than -1. This child class returns error information for each parameter removed. If all parameters are removed then the parent BNCSDeviceType
    /// is remove also.
    /// </summary>
    public class FiestaDeviceTypes : BNCSDeviceTypes
    {
        #region Private members
        private List<string> _removedDevices = new List<string>();
        private List<string> _removedParameters = new List<string>();
        #endregion

        #region Constructor
        /// <summary>
        /// Sets the file path and populates the instances in the base class
        /// </summary>
        /// <param name="filePath_">Path to xml file</param>
        /// <param name="instances_">List of instances</param>
        public FiestaDeviceTypes(string filePath_, List<BNCSDeviceInstance> instances_) : base(filePath_, instances_)
        {

        }
        #endregion

        #region Protected overridden method
        /// <summary>
        /// Initialises the instances
        /// </summary>
        /// <param name="instances_">List of instances</param>
        protected override void Initialise(List<BNCSDeviceInstance> instances_)
        {
            base.Initialise(instances_);

            List<int> parameterNumbers = new List<int>();

            for (int typeLoop = _deviceTypes.Count - 1; typeLoop > -1; --typeLoop)
            {
                for (int parameterLoop = _deviceTypes[typeLoop].Parameters.Count - 1; parameterLoop > -1; --parameterLoop)
                {
                    if (-1 == _deviceTypes[typeLoop].Parameters[parameterLoop].ParameterNumber && !(_deviceTypes[typeLoop].Parameters[parameterLoop].IsPseudoParameter || _deviceTypes[typeLoop].Parameters[parameterLoop].IsUserParameter))
                    {
                        _removedParameters.Add(_deviceTypes[typeLoop].Name + "." + _deviceTypes[typeLoop].Parameters[parameterLoop].Name + " parameter removed - expected a parameter number in the \"msg\" attribute e.g. msg=\"pN=n\", where n is an integer greater than -1");
                        _deviceTypes[typeLoop].Parameters.RemoveAt(parameterLoop);
                    }
                    else
                    {
                        if (parameterNumbers.Contains(_deviceTypes[typeLoop].Parameters[parameterLoop].ParameterNumber) && _deviceTypes[typeLoop].Parameters[parameterLoop].ParameterNumber > 0)
                        {
                            _removedParameters.Add(String.Format(_deviceTypes[typeLoop].Name + "." + _deviceTypes[typeLoop].Parameters[parameterLoop].Name + " parameter removed - duplicate parameter number in the \"msg\" attribute, \"pN={0}\" found in a previous parameter", _deviceTypes[typeLoop].Parameters[parameterLoop].ParameterNumber));
                            _deviceTypes[typeLoop].Parameters.RemoveAt(parameterLoop);
                        }
                        else
                        {
                            if (!_deviceTypes[typeLoop].Parameters[parameterLoop].IsPseudoParameter && !_deviceTypes[typeLoop].Parameters[parameterLoop].IsUserParameter)
                                parameterNumbers.Add(_deviceTypes[typeLoop].Parameters[parameterLoop].ParameterNumber);
                        }
                    }
                }

                if (0 == _deviceTypes[typeLoop].Parameters.Count)
                {
                    _removedDevices.Add(_deviceTypes[typeLoop].Name + " device removed - device contains no addressable parameters - see parameter errors for more information.");
                    _deviceTypes.RemoveAt(typeLoop);
                }

                parameterNumbers.Clear();
            }
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Returns a list of removed parameters (if any)
        /// </summary>
        public List<string> RemovedParameters
        {
            get { return _removedParameters; }
        }

        /// <summary>
        /// Returns a list of removed devices (if any)
        /// </summary>
        public List<string> RemovedDevices
        {
            get { return _removedDevices; }
        }
        #endregion
    }
}
