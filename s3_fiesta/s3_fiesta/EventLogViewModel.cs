﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Media;

namespace s3_fiesta
{
    /// <summary>
    /// Class that manages a list of EventItems that may be bound to a ListBox
    /// </summary>
    public class EventItemsLogViewModel : INotifyPropertyChanged
    {
        #region Private members
        private ObservableCollection<EventItem> _items = new ObservableCollection<EventItem>();
        private int _lastIndex = -1;
        #endregion

        #region Constructor
        public EventItemsLogViewModel()
        {
            _items.CollectionChanged += ItemsChanged;
        }
        #endregion

        #region Private event handler
        private void ItemsChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            LastIndex = _items.Count - 1;
        }
        #endregion

        #region Public properties
        public int LastIndex
        {
            get { return _lastIndex; }
            set
            {
                _lastIndex = value;
                if (null != PropertyChanged)
                    PropertyChanged(this, new PropertyChangedEventArgs("LastIndex"));
            }
        }

        public ObservableCollection<EventItem> Items
        {
            get { return _items; }
        }
        #endregion

        #region Public event
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }

    /// <summary>
    /// A class encapsulating an event item
    /// </summary>
    public class EventItem
    {
        #region Private members
        private string _text = string.Empty;
        private Brush _foreground = Brushes.White;
        private Brush _background = Brushes.Black;
        private EventCatergories _catergory;
        private bool _processed = false;
        #endregion

        #region Constructor
        public EventItem(string text_, EventCatergories catergories_)
        {
            // Get rid of newline characters
            _text = text_.Replace("\n", string.Empty);
            _catergory = catergories_;
            // Set foreground (sometimes background) colours by catergory
            switch (catergories_)
            {
                case EventCatergories.Bncs:
                    _foreground = Brushes.White;
                    break;
                case EventCatergories.Comms:
                    _foreground = Brushes.LightGreen;
                    break;
                case EventCatergories.Error:
                    _text = "Error: " + _text;
                    _foreground = Brushes.Yellow;
                    _background = Brushes.Red;
                    break;
                case EventCatergories.Receive:
                    _text = "RX: " + _text;
                    _foreground = Brushes.YellowGreen;
                    break;
                case EventCatergories.Startup:
                    _foreground = Brushes.LightBlue;
                    break;
                case EventCatergories.Transmit:
                    _text = "TX: " + _text;
                    _foreground = Brushes.AliceBlue;
                    break;
                case EventCatergories.Value:
                    _text = "Value Update: " + _text;
                    _foreground = Brushes.DarkGreen;
                    break;
                case EventCatergories.Registration:
                    _text = "REG: " + _text;
                    _foreground = Brushes.BlueViolet;
                    break;
                case EventCatergories.ConfigurationError:
                    _text = "Error: " + _text;
                    _foreground = Brushes.White;
                    _background = Brushes.Red;
                    break;
                case EventCatergories.Operations:
                    _text = DateTime.Now.ToLongTimeString() + " - " + _text;
                    _foreground = Brushes.White;
                    break;
                case EventCatergories.LifeCycle:
                    _foreground = Brushes.Green;
                    _background = Brushes.White;
                    break;
            }
        }
        #endregion

        #region Public r/o properties
        public Brush Foreground
        {
            get { return _foreground; }
        }

        public Brush Background
        {
            get { return _background; }
        }

        public string Text
        {
            get { return _text; }
        }

        public EventCatergories Catergory
        {
            get { return _catergory; }
        }

        public bool Processed
        {
            get { return _processed; }
            set { _processed = value; }
        }
        #endregion
    }
}
