﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Bncs.Controls;

namespace s3_fiesta
{
    /// <summary>
    /// Interaction logic for PollTimeEditor.xaml
    /// </summary>
    public partial class PollTimeEditor : Window
    {
        #region Private members
        private bool _ok = false;
        private int _oldValue = 0;
        #endregion

        #region Constructor
        public PollTimeEditor(int currentValue_)
        {
            InitializeComponent();
            _oldValue = currentValue_;
            cbEnabled.IsChecked = currentValue_ > 0;
            nsPoll.IsEnabled = currentValue_ > 0;
            if (nsPoll.IsEnabled)
                nsPoll.Value = currentValue_;
        }
        #endregion

        #region Private event handlers
        private void ValueChanged(object sender, OnBeforeNumericSelectorValueChangeEventArgs e, ref bool accept)
        {
            try
            {
                bnOk.IsEnabled = _oldValue != e.NewValue;
            }
            catch (NullReferenceException)
            {
                accept = false;
            }
        }

        private void CheckChange(object sender, RoutedEventArgs e)
        {
            nsPoll.IsEnabled = true == cbEnabled.IsChecked;
            bnOk.IsEnabled = _oldValue != 0;
        }

        private void OKClick(object sender, RoutedEventArgs e)
        {
            _ok = true;
            Close();
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion

        #region Public property
        /// <summary>
        /// Returns the selected value else null if no value was selected
        /// </summary>
        public int? Value
        {
            get { return _ok ? (true == cbEnabled.IsChecked ? nsPoll.Value : (int?)0) : null; }
        }
        #endregion
    }
}
