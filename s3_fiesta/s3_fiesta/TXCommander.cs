﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace s3_fiesta
{
    public delegate void RetryEventHandler(object sender, byte[] command, int retryCount);

    public class TXCommander
    {
        private DispatcherTimer _timer = null;
        private byte[] _command = null;
        private event TransmitEventHandler _onTransmit = null;
        private int _retryCount = -1;
        private int _maximumRetries = -1;
        private int _commandWindowSeconds = 240;

        public TXCommander(TransmitEventHandler onTransmit_)
        {
            _onTransmit = onTransmit_;
            _timer = new DispatcherTimer();
            _timer.Interval = new TimeSpan(0, 0, _commandWindowSeconds);
            _timer.Tick += TimerTick;
        }

        private void TimerTick(object sender, EventArgs e)
        {
            Transmit();
        }

        private bool Transmit()
        {
            if (null != _command)
            {
                if (null != _onTransmit)
                {
                    TransmitEventArgs args = new TransmitEventArgs();
                    args.RetryCount = ++_retryCount;
                    args.ExecuteRetry = -1 == _maximumRetries || _retryCount <= _maximumRetries;
                    args.Command = _command;
                    if (!args.ExecuteRetry)
                    {
                        --args.RetryCount;
                        Complete();
                    }
                    _onTransmit(this, args);
                }
                _timer.Start();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Transmits the supplied command
        /// </summary>
        /// <param name="buffer_">Buffer to send</param>
        /// <returns>True on success, false if not connected, the supplied buffer is null or if a command is awaiting completion</returns>
        public bool Transmit(byte[] buffer_)
        {
            if (null == _command)
            {
                _retryCount = -1;
                _command = buffer_;
                return Transmit();
            }
            return false;
        }

        public void Complete()
        {
            _timer.Stop();
            _command = null;
            _retryCount = 0;
        }

        public int MaximumRetries
        {
            get { return _maximumRetries; }
            set { _maximumRetries = value; }
        }

        public int CommandWindowSeconds
        {
            get { return _commandWindowSeconds; }
            set 
            {
                if (value != _commandWindowSeconds && value > 0)
                {
                    _commandWindowSeconds = value;
                    bool running = _timer.IsEnabled;
                    if (running)
                        _timer.Stop();
                    _timer.Interval = new TimeSpan(0, 0, _commandWindowSeconds);
                    if (running)
                        _timer.Start();
                }
            }
        }
    }

    public class TransmitEventArgs : EventArgs
    {
        public byte[] Command;
        public int RetryCount;
        public bool ExecuteRetry;
    }
    public delegate void TransmitEventHandler(object sender, TransmitEventArgs e);
}
