﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace s3_fiesta
{
    public delegate void OnDataQueueUpdateEventHandler(object sender, List<object> items);

    public class DataQueue
    {
        private Queue<object> _items = new Queue<object>();
        private DispatcherTimer _timer = null;
        private bool _active = true;
        private event OnDataQueueUpdateEventHandler _onUpdate = null;

        public DataQueue(OnDataQueueUpdateEventHandler onUpdate_)
        {
            Initialise(onUpdate_, 10);
        }

        public DataQueue(OnDataQueueUpdateEventHandler onUpdate_, int milliseconds_)
        {
            Initialise(onUpdate_, milliseconds_);
        }

        private void Initialise(OnDataQueueUpdateEventHandler onUpdate_, int milliseconds_)
        {
            if (null == onUpdate_)
                throw new Exception("The OnUpdate argument to the constructor must never be null.");
            else if (milliseconds_ <= 0)
                throw new Exception("The Milliseconds argument to the constructor must be at least 1 millisecond.");
            else
            {
                _onUpdate = onUpdate_;
                _timer = new DispatcherTimer();
                _timer.Tick += TimerTick;
                _timer.Interval = new TimeSpan(0, 0, 0, 0, milliseconds_);
                _timer.Start();
            }
        }

        private void TimerTick(object sender, EventArgs e)
        {
            if (null != _onUpdate)
            {
                int target = _items.Count;
                if (target > 0)
                {
                    List<object> items = new List<object>();
                    for (int loop = 0; loop < target; ++loop)
                        items.Add(_items.Dequeue());
                    _onUpdate(this, items);
                }
            }
        }

        public bool Add(object item_)
        {
            if (_active)
            {
                try
                {
                    _items.Enqueue(item_);
                }
                catch (ArgumentException)
                {
                    return false;
                }
            }
            return _active;
        }

        public List<object> Close()
        {
            _timer.Stop();
            _active = false;
            List<object> result = new List<object>();
            while (_items.Count > 0)
                result.Add(_items.Dequeue());
            return result;
        }
    }
}
