﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using SysAssembly = System.Reflection.Assembly;

namespace Bncs.Assembly
{
    /// <summary>
    /// This helper class attempts to resolve an assembly from a supplied assembly name. This class helps when wishing to load a DLL from your applications References from a location other than the application
    /// directory. When adding the reference the references CopyLocal property should be set to false to avoid it being copied in at design time.
    /// 
    /// This static class has methods to resolve an Assembly from a supplied assembly name. Typically these methods would be called from the AppDomain.CurrentDomain.AssemblyResolve event handler usually, but 
    /// not necessarily, defined in a WPF applcations "App.xaml.cs" unit.
    /// 
    /// To use this class simply add it to your project. For obvious reasons don't bother adding this to a DLL!
    /// </summary>
    public static class AssemblyResolver
    {
        #region Public static methods
        /// <summary>
        /// Looks for the supplied assembly name anywhere in the system path, the first instance found is returned. Only seeks files with a .dll extension.
        /// </summary>
        /// <param name="assemblyName_">Name of assembly sought</param>
        /// <returns>A AssemblyWithFilename instance which contains the Assembly and the file name</returns>
        public static AssemblyWithFilename ResolveFromPath(string assemblyName_)
        {
            return ResolveFromPath(assemblyName_, ".dll"); ;
        }

        /// <summary>
        /// Looks for the supplied assembly name anywhere in the system path, the first instance found is returned.
        /// </summary>
        /// <param name="assemblyName_">Name of assembly sought</param>
        /// <param name="dllExtension_">Extension of the library file</param>
        /// <returns>A AssemblyWithFilename instance which contains the Assembly and the file name</returns>
        public static AssemblyWithFilename ResolveFromPath(string assemblyName_, string dllExtension_)
        {
            string path = Environment.GetEnvironmentVariable("Path");
            List<string> directories = new List<string>();
            string[] systemDirectories = path.Split(new char[] { ';' });
            foreach (string directory in systemDirectories)
                directories.Add(directory);
            return Resolve(assemblyName_, directories, dllExtension_);
        }

        /// <summary>
        /// Looks for the supplied assembly name in the directory specified. Only seeks files with a .dll extension.
        /// </summary>
        /// <param name="assemblyName_">Name of assembly sought</param>
        /// <param name="directory_">Directory to seek assembly</param>
        /// <returns>A AssemblyWithFilename instance which contains the Assembly and the file name</returns>
        public static AssemblyWithFilename Resolve(string assemblyName_, string directory_)
        {
            return Resolve(assemblyName_, directory_, ".dll");
        }

        /// <summary>
        /// Looks for the supplied assembly name in the directory specified.
        /// </summary>
        /// <param name="assemblyName_">Name of assembly sought</param>
        /// <param name="directory_">Directory to seek assembly</param>
        /// <param name="dllExtension_">Extension of the library file</param>
        /// <returns>A AssemblyWithFilename instance which contains the Assembly and the file name</returns>
        public static AssemblyWithFilename Resolve(string assemblyName_, string directory_, string dllExtension_)
        {
            List<string> directories = new List<string>();
            directories.Add(directory_);
            return Resolve(assemblyName_, directories, dllExtension_);
        }

        /// <summary>
        /// Looks for the supplied assembly name in the list of directories specified, the first instance found is returned. Only seeks files with a .dll extension.
        /// </summary>
        /// <param name="assemblyName_">Name of assembly sought</param>
        /// <param name="directories_">Directories to seek assembly</param>
        /// <returns>A AssemblyWithFilename instance which contains the Assembly and the file name</returns>
        public static AssemblyWithFilename Resolve(string assemblyName_, List<string> directories_)
        {
            return Resolve(assemblyName_, directories_, ".dll");
        }

        /// <summary>
        /// Looks for the supplied assembly name in the list of directories specified, the first instance found is returned.
        /// </summary>
        /// <param name="assemblyName_">Name of assembly sought</param>
        /// <param name="directories_">Directories to seek assembly</param>
        /// <param name="dllExtension_">Extension of the library file</param>
        /// <returns>A AssemblyWithFilename instance which contains the Assembly and the file name</returns>
        public static AssemblyWithFilename Resolve(string assemblyName_, List<string> directories_, string dllExtension_)
        {
            if (0 == dllExtension_.Length)
                dllExtension_ = "*";
            if (!dllExtension_.StartsWith("."))
                dllExtension_ = "." + dllExtension_;

            string filename = string.Empty;
            SysAssembly assembly = null;

            //Retrieve the list of referenced assemblies in an array of AssemblyName.
            AssemblyName[] references = SysAssembly.GetExecutingAssembly().GetReferencedAssemblies();

            //Loop through the array of referenced assembly names
            foreach (AssemblyName assemblyName in references)
            {
                // Check for the supplied assembly name
                string suppliedAssemblyName = assemblyName_.Substring(0, assemblyName_.IndexOf(","));
                if (assemblyName.FullName.Substring(0, assemblyName.FullName.IndexOf(",")).Equals(suppliedAssemblyName))
                {
                    // Iterate through the supplied paths
                    foreach (string directory in directories_)
                    {
                        //Build the path of the assembly from where it might be loaded
                        string dir = directory;
                        if (!dir.EndsWith("\\"))
                            dir += "\\";
                        string dllFilename = dir + suppliedAssemblyName + dllExtension_;
                        if (File.Exists(dllFilename))
                        {
                            assembly = SysAssembly.LoadFrom(dllFilename);
                            if (null != assembly)
                            {
                                filename = dllFilename;
                                break;
                            }
                        }
                    }
                }
            }

            return new AssemblyWithFilename(assembly, filename);
        }

        /// <summary>
        /// Checks to see if an expected DLL exists on the system path or current directory 
        /// </summary>
        /// <param name="dllFilename_">Name of DLL without a path</param>
        /// <returns>True if the DLL could was located on the system path or current directory</returns>
        public static bool CheckForExpectedDLL(string dllFilename_)
        {
            return null == CheckForExpectedDLLs(new string[] { dllFilename_ });
        }

        /// <summary>
        /// Checks to see if an array of expected DLL's exist on the system path or current directory
        /// </summary>
        /// <param name="dllFilenames_"></param>
        /// <returns>NULL on success else an array of DLL's that could not be located in the system path or current directory</returns>
        public static string[] CheckForExpectedDLLs(string[] dllFilenames_)
        {
            string[] result = null;
            Dictionary<string, string> dlls = CheckForExpectedDLLsAndVersions(dllFilenames_);
            if (dlls.Count > 0)
            {
                List<string> notFound = new List<string>();
                foreach (KeyValuePair<string, string> pair in dlls)
                {
                    if (pair.Value.Equals(AssemblyResolver.DLL_NOT_FOUND))
                        notFound.Add(pair.Key);
                }
                if (notFound.Count > 0)
                {
                    result = new string[notFound.Count];
                    int index = -1;
                    foreach (string dll in notFound)
                        result[++index] = dll;
                }
            }
            return result;
        }

        /// <summary>
        /// Checks to see if an array of expected DLL's exist on the system path or current directory
        /// </summary>
        /// <param name="dllFilenames_"></param>
        /// <returns>A dictionary of DLL's, for DLL's that were located each Key contains the full path, if not found the key contains the DLL file name supplied. The Values consist of the version 
        /// number of a found DLL else if not found it will contain AssemblyResolver.DLL_NOT_FOUND</returns>
        public static Dictionary<string, string> CheckForExpectedDLLsAndVersions(string[] dllFilenames_)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            string path = Environment.GetEnvironmentVariable("Path");
            List<string> directories = new List<string>();
            string[] systemDirectories = path.Split(new char[] { ';' });
            directories.Add(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location));

            foreach (string directory in systemDirectories)
                directories.Add(directory);

            foreach (string dll in dllFilenames_)
            {
                bool found = false;
                string dir = string.Empty;
                foreach (string directory in directories)
                {
                    dir = directory;
                    if (!dir.EndsWith("\\"))
                        dir += "\\";
                    if (File.Exists(dir + dll))
                        found = true;
                    if (found)
                        break;
                }
                if (found)
                    result.Add(dir + dll, FileVersionInfo.GetVersionInfo(dir + dll).ProductVersion);
                else
                    result.Add(dll, DLL_NOT_FOUND);
            }

            return result;
        }

        #endregion

        #region Public static r/o property
        public static string FailoverLogFileName
        {
            get
            {
                string ccRoot = Environment.GetEnvironmentVariable("CC_ROOT");
                string ccSystem = Environment.GetEnvironmentVariable("CC_SYSTEM");
                string result = string.Empty;
                if (null != ccRoot && null != ccSystem && ccRoot.Length > 0 && ccSystem.Length > 0)
                {
                    result = string.Format("{0}\\{1}", ccRoot, ccSystem);
                    if (Directory.Exists(result))
                    {
                        result += "\\logs";
                        if (!Directory.Exists(result))
                        {
                            try
                            {
                                Directory.CreateDirectory(result);
                            }
                            catch { }
                        }
                        if (Directory.Exists(result))
                            result += "\\EXCEPTION_" + String.Format("{0:0000}{1:00}{2:00}_{3:00}{4:00}{5:00}_", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second) + ".log";
                        else
                            result = string.Empty;
                    }
                    else
                        result = string.Empty;
                }

                if (0 == result.Length)
                    result = "EXCEPTION_" + String.Format("{0:0000}{1:00}{2:00}_{3:00}{4:00}{5:00}_", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second) + ".log";

                return result;
            }
        }
        #endregion

        #region Public static const
        public const string DLL_NOT_FOUND = "fc067b29-b713-4a82-b42b-62101194e648";
        #endregion
    }

    /// <summary>
    /// Class that consists of a System.Reflection.Assembly object and a string
    /// </summary>
    public class AssemblyWithFilename
    {
        #region Constructor
        public AssemblyWithFilename(SysAssembly assembly_, string filename_)
        {
            Assembly = assembly_;
            Filename = filename_;
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Returns null or an assembly
        /// </summary>
        public SysAssembly Assembly = null;

        /// <summary>
        /// Returns an empty string or the full path and file name of the file containing the assembly
        /// </summary>
        public string Filename = string.Empty;

        /// <summary>
        /// Returns true if the assembly and file name properties are assigned with valid data
        /// </summary>
        public bool Loaded
        {
            get
            {
                return File.Exists(Filename) && null != Assembly;
            }
        }
        #endregion
    }
}
