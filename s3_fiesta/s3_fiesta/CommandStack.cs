﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Bncs.SysUtils;

namespace s3_fiesta
{
    public class CommandStack
    {
        private Queue<CommandStackItem> _commands = new Queue<CommandStackItem>();
        private int _selectedIndex = -1;
        private int _expectedUnit = -1;
        private int _expectedParameter = -1;

        public void Add(byte[] command_)
        {
            _commands.Enqueue(new CommandStackItem(command_));
        }

        public CommandStackItem Add(byte[] command_, int unitNumber_)
        {
            CommandStackItem result = new CommandStackItem(command_, unitNumber_, 0);
            _commands.Enqueue(result);
            return result;
        }

        public void Add(byte[] command_, int unitNumber_, int parameterNumber_)
        {
            _commands.Enqueue(new CommandStackItem(command_, unitNumber_, parameterNumber_));
        }

        public byte[] GetNextCommand()
        {
            if (_commands.Count > 0)
            {
                ++_selectedIndex;
                CommandStackItem item = _commands.Dequeue();
                _expectedUnit = item.ExpectedUnit;
                _expectedParameter = item.ExpectedParameter;
                item.Dispatched = DateTime.Now;
                return item.Command;
            }
            else
            {
                _selectedIndex = -1;
                return null;
            }
        }

        public void Clear()
        {
            _selectedIndex = -1;
            _commands.Clear();
            _expectedUnit = -1;
            _expectedParameter = -1;
        }

        public bool IsExpectedUnit(int unitNumber_)
        {
            return _expectedUnit == unitNumber_;
        }

        public bool IsExpectedParameter(Point unitParameter_)
        {
            if (0 == unitParameter_.X || 0 == unitParameter_.Y)
                return false;
            return _expectedUnit == unitParameter_.X && _expectedParameter == unitParameter_.Y;
        }

        public bool IsEmpty
        {
            get { return 0 == _commands.Count; }
        }

        public int SelectedIndex
        {
            get { return _selectedIndex; }
        }

        public int XebuxCount
        {
            get { return _commands.Count; }
        }
    }

    public class CommandStackItem
    {
        private byte[] _command = null;
        private int _expectedUnit = -1;
        private int _expectedParameter = -1;
        private DateTime? _dispatched = null;

        public CommandStackItem(byte[] command_)
        {
            _command = command_;
        }

        public CommandStackItem(byte[] command_, int expectedUnit_, int expectedParameter_)
        {
            _command = command_;
            _expectedUnit = expectedUnit_;
            _expectedParameter = expectedParameter_;
        }

        public override string ToString()
        {
            return new DataBuffer(_command).ToString();
        }

        public byte[] Command
        {
            get { return _command; }
        }

        public int ExpectedUnit
        {
            get { return _expectedUnit; }
        }

        public int ExpectedParameter
        {
            get { return _expectedParameter; }
        }

        public DateTime? Dispatched
        {
            get { return _dispatched; }
            set { _dispatched = value; }
        }
    }

    public class CommandStackItemList
    {
        private List<CommandStackItem> _items = new List<CommandStackItem>();

        public void Add(CommandStackItem item_)
        {
            _items.Add(item_);
        }

        public TimeSpan? ProcessItem(int unitNumber_)
        {
            TimeSpan? result = null;
            int index = -1;
            foreach (CommandStackItem item in _items)
            {
                ++index;
                if (item.ExpectedUnit == unitNumber_)
                {
                    if (null != item.Dispatched) // Bug #2825 - check that the DateTime is not null
                        result = DateTime.Now.Subtract((DateTime)item.Dispatched);
                    break;
                }
            }
            if (null != result)
                _items.RemoveAt(index);
            return result;
        }

        public int Count
        {
            get { return _items.Count; }
        }
    }
}
