﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fiesta;
using BNCS;
using Bncs.SysUtils;

namespace s3_fiesta
{
    /// <summary>
    /// Interaction logic for DeviceTypeExportSelectWindow.xaml
    /// </summary>
    public partial class ConfigurationEditor : Window
    {
        private event OnExportEventHandler _onExport = null;
        private bool _canClose = false;
        private int _devicesToInstantiate = 0;
        private int _infodriverNumber = 0;
        private ObservableCollection<DeviceTypeItem> _devices = new ObservableCollection<DeviceTypeItem>();

        public ConfigurationEditor(FiestaProtocol protocol_, int infodriverNumber_)
        {
            InitializeComponent();
            _infodriverNumber = infodriverNumber_;
            foreach (DeviceUnit unit in protocol_.Units)
                _devices.Add(new DeviceTypeItem(unit));
            lvDevices.ItemsSource = _devices;
        }

        private string SaveAs(DeviceUnit unit_)
        {
            string path = BNCSUtilities.GetBNCSFilePaths().DeviceTypesPath;
            if (!path.EndsWith("\\"))
                path += "\\";
            string filename = unit_.Name;
            filename = filename.Replace(' ', '_');
            filename = filename.Replace('/', '_');
            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
            if (System.IO.Directory.Exists(path))
                dialog.InitialDirectory = path;
            dialog.FileName = filename + ".xml";
            if (System.Windows.Forms.DialogResult.OK == dialog.ShowDialog())
                return dialog.FileName;
            return string.Empty;
        }

        private string SaveAs()
        {
            string path = BNCSUtilities.GetBNCSFilePaths().InstancesPath;
            if (!path.EndsWith("\\"))
                path += "\\";
            path += "merge\\";
            if (!DirFuncs.Exists(path))
            {
                if (!DirFuncs.MakeDirectory(path))
                {
                    MessageBox.Show("Error creating Merge directory at \"" + path + "\"", "System Error");
                    return string.Empty;
                }
            }
            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
            dialog.InitialDirectory = path;
            dialog.FileName = "instances_???.xml";
            if (System.Windows.Forms.DialogResult.OK == dialog.ShowDialog())
                return dialog.FileName;
            return string.Empty;
        }

        private void DoubleClick(object sender, MouseButtonEventArgs e)
        {
            ExportClick(null, null);
        }

        private void ExportClick(object sender, RoutedEventArgs e)
        {
            if (null != lvDevices.SelectedItem)
            {
                DeviceUnit unit = ((DeviceTypeItem)lvDevices.SelectedItem).Device;
                string filename = SaveAs(unit);
                if (filename.Length > 0 && unit.Number > 0)
                {
                    if (0 == unit.Parameters.Count)
                        InformationUpdate("Contacting Fiesta for parameter list, please wait...");
                    OnExportEventArgs args = new OnExportEventArgs();
                    args.Unit = new SelectedUnitForExport(filename, unit.Number, cbForceTypeName.IsChecked == true);
                    args.ForceFiestaRefresh = cbForceFiestaRefresh.IsChecked == true;
                    _onExport(this, args);
                }
            }
        }

        private void InstantiateClick(object sender, RoutedEventArgs e) 
        {
            List<string> deviceNames = new List<string>();
            foreach (DeviceTypeItem item in lvDevices.Items)
            {
                if (item.Instantiate)
                    deviceNames.Add(item.Device.Name);
            }

            if (deviceNames.Count > 0)
            {
                InstanceEditor editor = new InstanceEditor(deviceNames, _infodriverNumber);
                editor.ShowDialog();
                if (null != editor.Data)
                {
                    string instancesTarget = SaveAs();
                    if (instancesTarget.Length > 0)
                    {
                        if (!instancesTarget.ToLower().Equals(DirFuncs.IncludeTrailingBackslash(BNCSUtilities.GetBNCSFilePaths().InstancesPath.ToLower()).ToLower() + "instances.xml"))
                        {
                            if (BNCSSaveInstances.Save(instancesTarget, editor.Data))
                                MessageBox.Show("The selected instances have been written to:\n\n" + instancesTarget, "Information");
                            else
                                MessageBox.Show("Failed to save the selected instances to:\n\n" + instancesTarget, "Error");
                        }
                        else
                        {
                            string insert = "selected instance";
                            if (editor.Data.Count > 1)
                                insert += "s";
                            MessageBox.Show("You may not edit the live Instances.xml from this driver\n\nPlease save the " + insert + " to a different file, typically in the \"Merge\" directory. Open the Instances.xml file in a text or XML editor.", "Error");
                        }
                    }
                }
            }
        }

        private void CheckChange(object sender, RoutedEventArgs e)
        {
            bool value = true == ((CheckBox)sender).IsChecked;
            DeviceUnit unit = (DeviceUnit)((CheckBox)sender).Tag;
            foreach (DeviceTypeItem item in _devices)
            {
                if (item.Device.Equals(unit))
                {
                    item.Instantiate = value;
                    break;
                }
            }
 
            if (value)
                ++_devicesToInstantiate;
            else
                --_devicesToInstantiate;
            bnExportInstances.IsEnabled = _devicesToInstantiate > 0;
        }

        private void EditorClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !_canClose;
            if (e.Cancel)
                Hide();
        }

        private void CloseClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void UpdateOK(object sender, SelectionChangedEventArgs e)
        {
            bnExport.IsEnabled = null != lvDevices.SelectedItem && null != _onExport;
        }

        public void CloseEditor()
        {
            _canClose = true;
            Close();
        }

        public void InformationUpdate(string information_)
        {
            tbInfo.Text = information_;
            if (tbInfo.Foreground == Brushes.LightGreen)
                tbInfo.Foreground = Brushes.White;
            else
                tbInfo.Foreground = Brushes.LightGreen;
        }

        public event OnExportEventHandler OnExport
        {
            add { _onExport += value; }
            remove { _onExport -= value; }
        }
    }

    public class SelectedUnitForExport
    {
        private string _fileTarget = string.Empty;
        private int _unitNumber = -1;
        private bool _forceTypeNameToMatchFileName = false;

        public SelectedUnitForExport(string fileTarget_, int unitNumber_, bool forceTypeNameToMatchFileName_)
        {
            _fileTarget = fileTarget_;
            _unitNumber = unitNumber_;
            _forceTypeNameToMatchFileName = forceTypeNameToMatchFileName_;
        }

        public int UnitNumber
        {
            get { return _unitNumber; }
        }

        public string FileTarget
        {
            get { return _fileTarget; }
        }

        public bool ForceTypeNameToMatchFileName
        {
            get { return _forceTypeNameToMatchFileName; }
        }
    }

    public class OnExportEventArgs : EventArgs
    {
        public SelectedUnitForExport Unit;
        public bool ForceFiestaRefresh = false;
    }
    public delegate void OnExportEventHandler(object sender, OnExportEventArgs e);
}
